---
title: "Had a blast speaking at @brooklyn_js"
datePublished: 2016-03-18 20:45
published: false
excludeFromFeed: true
tags: note
---

Had a blast at @brooklyn_js! Thanks for having my everyone, you are awesome! 

Video: [https://youtu.be/CvorlYi4L_c](https://youtu.be/CvorlYi4L_c)
Slides: [https://drive.google.com/file/d/0B-RzdAWyftLQZGtoYVMzbDdESWs/view](https://drive.google.com/file/d/0B-RzdAWyftLQZGtoYVMzbDdESWs/view)

<img src="/media/2018/12/Hy_KV-BWV.jpg" alt="What do you do to get package delivered in NYC when your intercom is broken? @arturi built a robot for it 🙌">

![](/media/2018/12/HkZgHZSbV.jpg)

[https://twitter.com/brooklyn_js/status/710627522774491136](https://twitter.com/brooklyn_js/status/710627522774491136)
[https://twitter.com/arturi/status/710841320231837696](https://twitter.com/arturi/status/710841320231837696)