---
title: "Berlin"
description: "I moved to Berlin for a month of August to explore hidden beaches and a swimming pool on a river, tepee village, bike lanes, Back to the Future from a gentrification point of view, abandoned airport and a farm on a parking lot."
cover: "http://arturpaikin.com/media/2016/06/img_76121465924730087.jpg"
datePublished: 2016-06-17 11:55
published: true
---

<figure class="wide">
  <img src="/media/2016/06/img_76121465924730087.jpg">
</figure>

I moved to Berlin for the month of August to explore hidden beaches and a swimming pool on a river, tepee village, bike lanes, Back to the Future from a gentrification point of view, abandoned airport and a farm on a parking lot.

<!--more-->

---

This time, for the first time in history of my travels abroad, I’ve brought my [Strida bicycle](http://arturpaikin.com/ru/strida). Air Berlin was cool about it, I mean, it’s a tiny foldable bike and fits perfectly in a normal luggage, but some airlines might still want you to pay extra, which is crazy.

<figure>
  <img src="/media/2016/06/img_67461465925029266.jpg">
  <figcaption>Unpacking Strida and inflating its tires at the Tegel airport</figcaption>
</figure>

Bringing a bike turned out to be a great idea. Bike lanes in Berlin start almost right at the airport and last forever. Everywhere I went in the next few weeks — even Potsdam, about 40 km from Berlin — I did so by bike. Used the subway like twice.

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77641466007473801.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77611466007473788.jpg">
  </div>
</div>
<figcaption>The average speed inside the city is <a href="http://sootfreecities.eu/city/berlin">about 30 km/h</a>. It feels relaxed and safe.</figcaption>
</figure>

![](/media/2016/06/img_67781465925435111.jpg)

Lena managed to find a really good deal on a room in Neukölln — my favorite area in Berlin, full of great bars, parks and delicious falafel — it was around 300 euros for 3 weeks. If you find yourself looking for a place in Berlin, before Airbnb try here: [http://www.wg-gesucht.de](http://www.wg-gesucht.de) and [https://www.facebook.com/groups/roomsurfer](https://www.facebook.com/groups/roomsurfer).

<figure>
<div class="row wide collage">
  <div class="column-full">
    <img src="/media/2016/06/img_70581465926380903.jpg">
  </div>

  <div class="column-1-3 height-m">
    <img src="/media/2016/06/img_72291465926380917.jpg">
  </div>

  <div class="column-1-3 height-m">
    <img src="/media/2016/06/img_72231465926380911.jpg">
  </div>

  <div class="column-1-3 height-m">
    <img src="/media/2016/06/img_67531465926380891.jpg">
  </div>
</div>
</figure>

This is how you use a German washing machine if you don’t speak German:

<figure class="small-image">
<img src="/media/2016/06/img_76341466008607437.jpg">
</figure>

The city has some of the best coffee roasters in the world — Five Elephant and The Barn. The former was conveniently located 3 minutes by biсycle from my apartment.

![](/media/2016/06/img_70161465929156402.jpg)

There is a Berlin vibe going around Five Elephant — visitors, houses, the area itself. Coffee and sometimes their signature cheesecake have quickly become my morning ritual.

![](/media/2016/06/img_68721465929526014.jpg)

On a sunny day, before coffee, I’d go downstairs for fresh croissants at the French bakery. The building where we stayed had a beautiful inner yard with a table, which sadly nobody used. Lena and I made breakfast and brought it downstairs. I think that really impressed our German neighbours.

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77161465937891215.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77171465937891232.jpg">
  </div>
</div>
</figure>

![](/media/2016/06/img_76161465930828867.jpg)

![](/media/2016/06/img_72461465930953529.jpg)

Another favorite spot in the area is Görlitzer Park. Also, very Berlin — a bunch of people with dogs, bikes, barbecues and weed, which the park is mostly known for.

![](/media/2016/06/img_76891465931220821.jpg)

![](/media/2016/06/img_76991465931248531.jpg)

![](/media/2016/06/img_76961465931232889.jpg)

![](/media/2016/06/img_77041465931257844.jpg)

---

![](/media/2016/06/img_77191465937723173.jpg)

*Yes, I’m all about food and parks, and bikes, and food. Bare with me.*

Our roommate Jan is an artist and he invited us to visit his exhibition. One of the creations — a swimming pool full of trash, gives a perspective on how much plastic we waste.

![](/media/2016/06/img_77781465938783575.jpg)

Time for ice-cream at California Pops.

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77921465938928500.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77941465938928516.jpg">
  </div>
</div>
</figure>

<s>So Portland</s>, I mean, Berlin: a swimming pool right on the river. Queues on a warm summer day are something, but it’s worth a swim and chill at least once.

![](/media/2016/06/img_68961465939064184.jpg)

This used to be a public toilet under the overground train track, now it’s a popular burger spot (veg options available).

![](/media/2016/06/img_76431465939197397.jpg)

Famous queue to [Mustafas Gemüse Kebab](http://www.yelp.com/biz/mustafas-gem%C3%BCsed%C3%B6ner-berlin).

![](/media/2016/08/img_76201470589141588.jpg)

## Wow so Portland

The Tempelhof airport remains on the top of the list of my favorite places in Berlin.

![](/media/2016/06/img_77351465939664440.jpg)

It’s an abandoned airport that Berliners (the people, not the doughnut) voted to keep as a public space/park. The airport building served as an [emergency refugee shelter](http://www.telegraph.co.uk/news/worldnews/europe/eu/11863246/Refugee-crisis-EU-ministers-Germany-border-control-Austria-army-live.html) in 2015.

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77481465939378515.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77551465939686817.jpg">
  </div>
</div>
</figure>

People have nice little gardens.

![](/media/2016/06/img_76261465941236817.jpg)

![](/media/2016/06/img_76231465941268108.jpg)

![](/media/2016/06/img_76271465941279875.jpg)

The cross-walk is lit at night, thumbs up!

![](/media/2016/06/img_77581465939845959.jpg)

Wedding reception at a park.

![](/media/2016/06/img_77211465940094563.jpg)

Indian food is my favorite food. Especially “Paneer Tikka Masala” (not in the picture).

![](/media/2016/06/img_77071465940094550.jpg)

We found a weird play at a local theatre, mixing Back to the Future and neighbourhood gentrification. The theatre is like a brother to my favorite The Brick in Brooklyn.

![](/media/2016/06/img_77831465940249191.jpg)

[Knick-Knack To The Future | Ruckzuck in die Zukunft](http://www.etberlin.de/production/knick-knack-to-the-future-ruckzuck-in-die-zukunft-copy-waste/):

> The project focuses on the processes of urban development using the Back to the Future movie trilogy as its point of departure.
>
> In Back to the Future, Part II, the teenager Marty McFly travels to the future in a souped-up sports car and lands in 2015. There not only does Marty encounter holograms, hoverboards and his thirty-year older self; the city of Hill Valley is also wholly transformed.
> 
> The real 2015 may not look so futuristic, but cities have still changed immensely. Some neighborhoods have become impoverished while others have appreciated tremendously in value. And some impoverished neighborhoods are experiencing this appreciation right now.
> 
> First come the artists and studios, followed by the students and the cupcake cafés and then the well-to-do and boutiques. And this process takes place over increasingly shorter periods of time.
> 
> copy & waste intends to open a fictional store, a concept store: Ruckzuck – Cupcakes & Time Travel. Here customers can enjoy cupcakes and cappuccino in a Back to the Future ambiance – during the day. At night, the store also offers time travel. Like Marty McFly, the visitors can travel to the past or dash to the future to prevent certain things from happening – or from not happening.
> 
> The basic question in all of this is: what kind of city do we want to live in?

![](/media/2016/06/img_77871465940249207.jpg)

Before the play everybody bought beer in bottles or cans: drinking while enjoying a play or performance is a common thing to do. I’m gonna tell you a secret: beer is not great in Germany. I mean in a way I like it: with taste and character, you know, like wine. In Berlin, it’s customary to drink something very basic and cheap like Berliner Kindl by the canal. At first, I was skeptical, spoiled by the Russian “craft beer revolution” that introduced me to 30 beer flavors from all around the country and the world, but then it grew on me, I accepted Berliner Kindl by the canal, and began to enjoy it.

It is possible to find a few craft beer pubs though, if you look hard. 

Fun fact: drinking beer and wine is [legal in Germany from the age of 13](https://en.wikipedia.org/wiki/Alcohol_laws_in_Germany) when accompanied by an adult, and from 16 without an adult. In Berlin, everybody drinks beer everywhere, and it’s totally chill — I haven’t seen drunk people or conflict much, somehow drinking culture has matured here. It’s interesting compared to the US where people under 21 are not allowed to drink alcohol, which I believe is crazy.

## Hidden from muggles

Next up — weird bars! For the life of me, I couldn’t figure out what this place is called, but it’s super rad. Right on the bank of Spree, nevertheless some people seem to just pass by, not noticing.

*Update: it’s called [Holzmarkt](https://goo.gl/maps/K3roXEyFfYu)!*

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_76751466006205073.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_76731466006258512.jpg">
  </div>
</div>

<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77251466006292836.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_77221466006315587.jpg">
  </div>
</div>
</figure>

And if you look right across the river you’ll see the Teepeeland. As Faces of Berlin [puts it](http://facesofberlin.org/in-a-teepee-on-the-spree/):

> On the riverside of the Spree, right between Berlin’s famous nightclubs Kater Holzig and Magdalena, a small village has arisen. They call themselves ‘Teepeelanders’, as they’ve built teepees and Mongolian yurts to live in. In Teepeeland there are no rules, Roll explains. “We don’t like that word. But wait, actually, there is one rule”, he laughs, “no public masturbation.” 
> Welcome to Teepeeland!

![](/media/2016/06/img_76851466006998334.jpg)

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_76811466007033439.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_76841466007035458.jpg">
  </div>
</div>
</figure>

The village is free for anybody to visit and live at. It’s also in the city center and the government decided to let it be for a while. I actually think that’s how things should work — the world is free for everybody, come and live where you want, rent-free. Just bring your tent and a laptop. And grow your own food, share or trade with neighbours.

Speaking of food. [Prinzessinnengarten](http://prinzessinnengarten.net/about/) is a parking lot turned into a garden/farm with tomatoes, peas, carrots and other types of food growing. 

<figure>
<div class="row collage">
  <div class="column-full">
    <img src="/media/2016/06/img_76501466009239326.jpg">
  </div>
</div>

<div class="row collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_76531466008128014.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_76591466008132905.jpg">
  </div>
</div>

<div class="row collage">
  <div class="column-full">
    <img src="/media/2016/06/img_76571466008131117.jpg">
  </div>
</div>
</figure>

Also here is a volunteer bike repair spot. Friendly people hang around and teach you how to fix breaks, adjust something, repaint or whatever. 

<figure>
  <img src="/media/2016/06/img_76641466008226197.jpg">
  <figcaption>On the left Hans is helping someone with their bike, while on the right you can see a house in progress — also a DIY project where everyone interested may come and learn to build houses, while helping build this one.</figcaption>
</figure>

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_76621466008520132.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2016/06/img_76681466008520146.jpg">
  </div>
</div>
</figure>

![](/media/2016/06/img_76481466009794512.jpg)

## Further reading

* [Sometimes I think I’m cool](https://twitter.com/paulozoom/status/652532989210005504)
* Great [Berlin guide](https://34travel.by/post/berlin) (in Russian)
* [August in Berlin](http://blog.elenazaharova.com/post/august-in-berlin) — Elena’s version






