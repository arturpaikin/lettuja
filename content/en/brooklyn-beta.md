---
title: "Brooklyn Beta 2014"
datePublished: 2014-11-01 23:04
cover: "http://arturpaikin.com/media/brooklyn-beta/img_56701414699599729.jpg"
published: true
---

<figure class="wide">
  <img src="/media/brooklyn-beta/img_56701414699599729.jpg">
</figure>

[Brooklyn Beta](http://brooklynbeta.org) is a web conference and an annual gathering of friendly designers, developers, writers, artists, makers and the like from the US and all around the world. The main theme is “make something you love”. And, from my experience, that’s exactly what the creators and the team have done.

<!--more-->

I first heard of Brooklyn Beta a year or two ago from tweets and instagrams of the attendees. Later I [read about it some more](http://destroytoday.com/blog/brooklyn-beta-is-people/) and was convinced I had to attend, but missed out on the tickets.

Nevertheless, I planned my trip to New York so that I’d be there when Beta happens. My plan was to try and attend [some events](https://brooklynbeta.org/2014/#/schedule) around town and get a chance to meet those awesome people I follow on the Internet.

A few days before my flight, late at night as I was about to go to bed, I got an email from the Beta people (time difference). They were looking for volunteers, and the deal was fairly straightforward: we help out and in exchange get to attend the conference when we are not busy volunteering. I jumped at it and quickly replied with some information about how cool I am. The next morning I woke up and found out that I was in. Yay! I was so excited (still am).

## The Night Before

Ten hour flight from Moscow to JFK. A bit of Node.js coding (in Atom), some podcasts, a few Instapapered articles and a train ride later, I’ve finally arrived at [The Invisible Dog](http://theinvisibledog.org/).

Everything seemed surreal. The place and the people that only existed in my Instagram feed before have come to life.

There was a bucket full of beer and ice in the middle of the room, and while I was helping myself to some, [SwissMiss](http://swiss-miss.com) passed by, then [Cameron](https://twitter.com/FictiveCameron) asked how I was doing. Then a fellow designer walked up to me and we had a lovely conversation about bicycles in Brooklyn, Apple Events and working for yourself.

![image](/media/brooklyn-beta/img_55951414421658278.jpg)

The bicoastal event called “Portlyn/Brookland” has begun. We got to hear some great folks talk about their work, scaling it, the challenges we have to overcome, and whether or not there is a bird on everything in Portland.

This one thing that [Ben Pieratt](http://pieratt.com/) said has stuck with me: “If you are happy with your job, don‘t go for all that entrepreneurial stuff… Bloom where you are planted”. (That bloom thing is actually a quote from Mary Engelbreit.)

After the event I met Katie, the head of volunteering committee, and helped Jen, Jonnie and others clean up the place.

## The Grand Finale

The next morning I woke up to perfect sunlight coming through the window and left our friends’ studio in Greenpoint. I stopped for coffee and a bagel near Williamsburg Bridge (“It’s gonna be $4.30. Cash only, sweetie. Have a good day.”) and walked all the way up to the Brooklyn Navy Yard. I was planning to take a bus, but walking just felt right.

May I just say, I did not regret it.

![image](/media/brooklyn-beta/img_56331414535521471.jpg)

The road along Brooklyn Navy Yard was accompanied by a nice bike lane and picturesque scenery.

![image](/media/brooklyn-beta/img_56411414200626919.jpg)

### The Place

When I entered the Navy Yard, I was greeted by fellow volunteers, eager to help a friendly Internet hipster with a pocket map of the area. Totally not necessary, but a nice touch.

![image](/media/brooklyn-beta/img_56441414201744321.jpg)

I made my way further and arrived at the line to the Druggal Greenhouse while having a brief conversation with another fellow web developer (from Pennsylvania, if I remember correctly).

![image](/media/brooklyn-beta/img_56471414202208933.jpg)

The bicycle parking was right there as promised.

![image](/media/brooklyn-beta/img_56481414202299482.jpg)

Usually, at conferences, badges are sorted by last name. But here it was different, and for some reason I liked it better this way. I guess it goes all the way back to my school years when the teacher would call me by last name all the time. But I digress.

![image](/media/brooklyn-beta/img_56501414202591046.jpg)

A minute and another quick chat later (“Oh man, is that an ATP shirt? Is it the right one or the screwed up one? If you keep it for like ten more years, you might be able to sell it like for 10 grand!”) I finally got to see the place.

The place was stunning.

![image](/media/brooklyn-beta/img_56641414202774333.jpg)

![image](/media/brooklyn-beta/img_56511414421918631.jpg)

And so was the view. I remember walking back and forth and just feeling happy.

![image](/media/brooklyn-beta/img_56581414202732645.jpg)

There was coffee (from Brooklyn Roasting Company, a bit too dark for my taste), so I helped myself and enjoyed a cup on the terrace.

![image](/media/brooklyn-beta/img_56551414202709723.jpg)

<figure>
  <img src="/media/brooklyn-beta/img_56621414202926920.jpg">
  <figcaption><a href="http://yaronschoen.com">Yaron Schoen</a>, <a href="http://destroytoday.com">Jonnie Hallman</a> and somebody I did’t recognize (my appologies).</figcaption>
</figure>

Naturally, someone came with a dog. I was surprised there weren’t more.

![image](/media/brooklyn-beta/img_56751414202900978.jpg)

![image](/media/brooklyn-beta/img_56731414202951500.jpg)

![image](/media/brooklyn-beta/img_56761414202991166.jpg)

The view from the loft.

![image](/media/brooklyn-beta/img_56741414422211150.jpg)

Right next to the tables with free badges (“Make Something You Love” and “I’m from the Internet”) and stuff there was a kiosk with magazines, patches and t-shirts.

![image](/media/brooklyn-beta/img_30041414203075684.jpg)

![image](/media/brooklyn-beta/img_57051414203096467.jpg)

Unleash your inner artist — a giant canvas with the outline of the Beta logo on it, some brushes and buckets of paint.

![image](/media/brooklyn-beta/img_30211414203127438.jpg)

### The People

Then, out of the blue, I saw a guy I see on YouTube every other day. Sir [Jonathan Mann](http://jonathanmann.net/) himself! With his ukulele sticking out of the backpack. He said “hi”, then recognized my ATP shirt and we had a lovely chat. Then Jonathan took an obligatory selfie to [send a proof to Marco, Casey and John](https://twitter.com/songadaymann/status/520588283722862592).

![image](/media/brooklyn-beta/img_57261414446715149.jpg)

If you don’t know who Jonathan is, go watch some of my favorite songs that he made: [Come Live With Me In Brooklyn](http://www.youtube.com/watch?v=VZJqUyS5tvc), [Buy My Bed](http://www.youtube.com/watch?v=OtT1inUGqyk) and [The Craig Federighi Show](http://www.youtube.com/watch?v=yVSq2nAnJ6I&).

Laura and Meg from [Ghostly Ferns](http://ghostlyferns.com) are having a laugh. I said hi and thanked them for the wonderful [Scouted guide](http://scouted.in).

![image](/media/brooklyn-beta/img_56771414203501127.jpg)

### Feeling Like a Wizard

When it was time for the first talk, [Chris](http://shiflett.org) took the stage.

![image](/media/brooklyn-beta/img_56801414203666239.jpg)

He gave [opening remarks](http://shiflett.org/blog/2014/oct/brooklyn-beta-opening-remarks) on why we are here and the main goals of the event. Cameron joined Chris and told some very funny jokes.

*By the way, here is a [good interview with Chris & Cameron](https://thegreatdiscontent.com/interview/chris-shiflett-and-cameron-koczon), the creators of Brooklyn Beta.*

The first speaker was [Jane Ní Dhulchaointigh](https://thegreatdiscontent.com/interview/jane-ni-dhulchaointigh), the creator of [Sugru](http://sugru.com/), a self-setting rubber that can be formed by hand. People use it to enhance and fix cables, kitchen appliances, wheelchairs and basically anything else.

![image](/media/brooklyn-beta/img_56811414427230704.jpg)

Jane talked about how she started Sugru, how it was not at all an immediate success, and the important lessons learned on the way:

* You don’t need to be an expert. Learn it.
* Naivety is underestimated.
* Listen, but go with your gut.
* Start small and make it good.

### The Lunch

Straight after the first talk, there was lunch (what did I tell you, this place is the best) with plenty of options: vegan, gluten-free, meat — whatever.

![image](/media/brooklyn-beta/img_56941414443616778.jpg)

While eating a really huge and spicy sandwich on the terrace, I made some friends: a mobile designer from San Francisco and a married couple where the wife is a textile designer and husband is an architect. They live in Park Slope with their two children.

“When we first moved there I was freaked out. Everywhere I looked I saw the exact same people. Wife is a designer, husband is an architect and they have two kids: a boy and a girl. Everybody. That drove me crazy. But then I got used to it.”

![image](/media/brooklyn-beta/img_30261414539587220.jpg)

![image](/media/brooklyn-beta/img_29931414445370302.jpg)

And when came time for dessert, oh my. There were dozens of delicious pies, ice-cream (which I was [put in charge of](https://www.flickr.com/photos/99348135@N08/16061655367/in/set-72157649816906508)), cakes.

![image](/media/brooklyn-beta/img_56971414443902345.jpg)

![image](/media/brooklyn-beta/img_56991414443471025.jpg)

And whisky. Also, beer and wine.

![image](/media/brooklyn-beta/img_57031414443522097.jpg)

After lunch was my volunteer shift, so I only got a peek of the next few talks here and there. But I totally made up for it in conversations and having fun all around, cutting pies, pouring whisky and helping everybody with ice cream.

<figure>
  <img src="/media/brooklyn-beta/img_56981414444328709.jpg">
  <figcaption><a href="http://jenmussari.com">Jen</a> and Nate discuss the DJI Phantom Drone (presumably)
  </figcaption>
</figure>

<figure>
  <img src="/media/brooklyn-beta/img_57041414446198618.jpg">
  <figcaption>Ryan and Tina Essmaker, publishers of <a href="http://thegreatdiscontent.com/">The Great Discontent</a></figcaption>
</figure>

Jonathan Mann was supposed to write a song about the conference, but preferred to take a nap.

![image](/media/brooklyn-beta/img_56871414446320928.jpg)

### The End

The last talk knocked my socks off. It was by [Jason Scott](https://twitter.com/textfiles) from the [Internet Archive](http://web.archive.org) — a humble organization that just makes copies of Internet pages as well as books, magazines, video and audio content, and preserves them for us and future generations. No big deal.

<figure>
  <img src="/media/brooklyn-beta/img_57151414534934191.jpg">
  <figcaption>Jason is famous for rocking cool outfits during his talks. In case you can’t see from here, it’s a <a href="https://www.flickr.com/photos/99348135@N08/16247312195/in/set-72157650211822422">red suit with white hearts on it</a>.</figcaption>
</figure>

He is an astonishing and honest [speaker](http://ascii.textfiles.com/speaking). Watch one of his talks, I highly recommend this one: “[From COLO to YOLO: Confessions of the Angriest Archivist](http://vimeo.com/96634067)”. It is very close to the one I saw live at Brooklyn Beta. You won’t regret it.

Jason talks about how he hates when stuff from the Internet disappears forever. About all those services that lure you into trusting them with your digital life and then one day just shut down or sell out, leaving you without your precious data.

Internet Archive has downloaded the whole [GeoCities](http://en.wikipedia.org/wiki/GeoCities) catalog when they were about to shut down. And torrented it. How cool is that? You can now go to the Pirate Bay and download GeoCities. This opens up opportunities like digital archeology — people are [finding some really cool stuff](http://contemporary-home-computing.org/1tb/) we’ve already forgotten or never knew about.

A few quotes (as I remember them):

* “When company X announced they are shutting down, deleting everything, we downloaded from them so hard they had to postpone the shutdown. But we got it all!”

* “Always have an export function.”

* “I was a unix sysadmin for 15 years. Hated it. Made six figures. *Hated it.* Because every day was me doing the least amount of work, so I could do the thing I *wanted* to do. And on the side I would make movies, collect things, give speeches, and be funny, and go to places. Then I got fired and later got a job at the Internet Archive. It was like black & white and color. Every waking moment that I’m alive, I’m happy. This is where I wanna be. I love collecting things. I wake up and I’m like “what’s awesome today?”

    What I am saying is, I thought I had it made when I was making a $120,000 a year. I talked to Phill Torrone (one of the founders of Adafruit), and he said to me, “you know, what I discovered when I was making that much money was, that at night I was shopping online to convince myself I was free. Because when I could buy anything I want at night, the fact that I was owned for ten hours a day was irrelevant, cause I was “free”. And that’s just not true.

    Now everyday I am so happy. The letters that I get, and the joy… And I am making a third of what I used to make. But I am happy every single day. I wish all of you could feel that. However you get there, please do it.”

### The Song

After Jason, Cameron took the stage and talked about the importance of developers and gave himself a C+ for Brooklyn Beta, because he felt that it became too design focused and didn’t help bridge the gap between design and development. This was very honorable of him, but as impressed as I was by all the amazing things that happened on just that one day, I am really looking forward to what Chris and Cameron make next that is going to be an A.

Turns out, Jonathan Mann did’t just sleep, but also wrote a [song on the events of the day](http://www.youtube.com/watch?v=RJT0wX-JaSQ) (the video quality is on par with my pictures for this post, unfortunately, but watch it anyway, the song is awesome).

![image](/media/brooklyn-beta/img_57211414452120670.jpg)

<figure>
  <img src="/media/brooklyn-beta/img_44161414695513641.jpg">
  <figcaption>Me and <a href="http://elenazaharova.com">Elena</a> were actually on stage, waving pom-poms during the song (Elena is the first and I’m the second from the right). Photo by <a href="https://twitter.com/kskobac">Kevin Skobac</a>.</figcaption>
</figure>

***

Later I helped clean up and fold the chairs. We had some whisky with Cameron and the team. There was music and dancing.

And then I woke up.

![image](/media/brooklyn-beta/img_30421414451799277.jpg)


### Further Reading

* Great [pictures by Jan Paul Koudstaal](https://www.flickr.com/photos/jpk_spacemonkey/sets/72157648357004308)
* And some more [official pics by Sara Kerens and Steve McFarland](https://www.flickr.com/photos/99348135@N08/sets/72157649816906508/)
* [10 Lessons on design and making things I learned at Brooklyn Beta 2014](https://medium.com/@kskobac/10-lessons-on-design-and-creation-i-learned-at-brooklyn-beta-2014-312456dadc02) by Kevin Skobac
* [Farewell Brooklyn Beta](http://www.mangrove.com/en/journal/2014-10-30-farewell-brooklyn-beta/) and [Brooklyn Beta 2013](https://medium.com/@jpkoudstaal/brooklyn-beta-2013-bda8fec41ac) by Jan Paul Koudstaal
* A [writeup by Naz Hamid](https://www.storehouse.co/stories/o6shd-no-sleep-til-brooklyn)
* Jonnie Hallman, [Brooklyn Beta is People](http://destroytoday.com/blog/brooklyn-beta-is-people/)
* [Collection of posts and pics](https://gimmebar.com/collection/4e9b779b2e0aaa2720000125/brooklyn-beta) from all the years
