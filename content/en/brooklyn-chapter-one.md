---
title: "Brooklyn. Chapter One"
description: "In this chapter: Greenpoint and bagels, Jews in Williamsburg, Manhattan and the subway, food coops, community gardens and Brooklyn’s charming neighbourhoods: Carroll Gardens, Cobble Hill, Park Slope"
datePublished: 2015-09-28 18:39
cover: "http://arturpaikin.com/media/2015/09/img_56331443362218901.jpg"
category: "new-york"
published: true
---

<figure class="wide">
  <img src="/media/2015/09/img_56331443362218901.jpg">
</figure>

In this chapter: Greenpoint and bagels, Jews in Williamsburg, Manhattan and the subway, food coops, community gardens and Brooklyn’s charming neighbourhoods: Carroll Gardens, Cobble Hill, Park Slope.

<!--more-->

***

We’re lucky enough to live at a time when teleport has been already invented. We just don’t realize it. It’s called a plane, and is totally underappreciated. Think about it, in 10 hours you can be almost anywhere in the world! You get in on one planet (Bangkok) and exit on another (Copenhagen). Everything around you is kind of the same but totally different.

*I know I sound like [this guy](https://www.youtube.com/watch?v=akiVi1sR2rM&t=3m40s) but that’s my point. “You are sitting in a chair in the sky!”*

I am a bit worried each time I fly and all the check-ins and flight preparation stuff, among with constant announcements and jet lag make me dizzy. But once I get out of the teleport and wonder around the uncharted land, listening to people talk, looking at the way they dress, noticing smells and street signs, and — finally — that first slice of pizza or croissant — I know, once again, it was all worth it. And I am thankful to live today and be able to teleport myself anywhere in just a couple of hours.

So, grab your backpack and let’s go to Brooklyn.

<!--more-->

***

New York has a very prominent, vivid smell — the mix of laundromats and corner halal food stands, and it hits me right after I exit the subway from JFK.

I remember this one girl on the train. She is wearing a black hoodie “New York” and a tiny backpack, hugging a paper bag from Subway with one hand, a small coffee and a large bucket of coke with the other. Gently holding her sandwich she turns on the music on her iPhone in a pink case and starts singing to herself, “He-ey-yeah yeah. Never, never, oh yeah”.

I am walking down the street at Carroll Gardens, close to [The Invisible Dog](http://theinvisibledog.org) art centre, where Brooklyn Beta event is going to take place. People are passing by on bicycles, enjoying an evening stroll with their puffy dogs and babies, drinking coffee. People smile at me and it all feels like a happy dream.

![](/media/2015/09/img_62731443387847884.jpg)

*The next few hours and the following day were dedicated to Brooklyn Beta. It was totally epic and I’ve [written about it at length](http://arturpaikin.com/en/brooklyn-beta/), so I won’t repeat myself here.*

## Bagels in Greenpoint

Driggs Ave and the surrounding area, not far from my friends’ apartment where I am staying. Feels like home that I once lost and now found again.

![](/media/2015/04/img_56121428112767976.jpg)

![](/media/2015/04/img_56141428112854987.jpg)

A squirrel is crossing the street.

![](/media/2015/04/img_56031428113352357.jpg)

Dog park.

![](/media/2015/04/img_56201428113444055.jpg)

There is something for everyone in New York, even rolls for aliens. Sounds good, let’s make a stop.

![](/media/2015/04/img_56211427970186766.jpg)

Ashlea’s written about [8 things she loves that can only be found in the US](http://aglobewelltravelled.com/2015/09/16/8-things-i-love-that-you-can-only-find-in-usa/) and bagels are number one on that list. Couldn’t agree more! Bagels are great, everyone should have some. Crap, now I miss bagels again. I even bring a few bags of bagels on a plane back home with me.

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2015/09/img_56241442834847448.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2015/04/img_56221427970250247.jpg">
  </div>
</div>
<figcaption>Rainbow and breast cancer pink bagels. To paraphrase Tim Cook, only Brooklyn could do this.</figcaption>
</figure>

Well, yeah.

![](/media/2015/04/img_32401428114504127.jpg)

*On the sidewalk I notice a cart-board box from an old Mac Pro with some porridge on it. Times change.*

The jewish neighbourhood. I stumbled upon it unintentionally and was taken by surprise — everywhere I looked I saw jews ([hasidic](https://en.wikipedia.org/wiki/Hasidic_Judaism)) dressed in black, wearing their traditional hats (called Shtreimel) that remind me of my moms fur coat, carrying small plants in plastic bags. On every other corner I saw self-made wooden booths attached to houses. Turn out, it’s [Sukkot — the jewish harvest holiday](http://www.ibtimes.com/what-sukkot-5-quick-facts-you-should-know-about-2015-jewish-harvest-holiday-2114978).

![](/media/2015/04/img_56341427970577305.jpg)

![](/media/2015/09/img_56361442742618846.jpg)

The fire department is impressive.

![](/media/2015/04/img_56391427970888264.jpg)

## Manhattan, Pumpkins and Subway

The day after Brooklyn Beta I walked the Williamsburg bridge to Manhattan. Digging its bright pink and gray colors — time for some posing.

![](/media/2015/04/img_33041428183721968.jpg)

![](/media/2015/04/img_58271428183605981.jpg)

Magnolia Bakery in Greenwich Village.

![](/media/2015/04/img_58301427971282096.jpg)

Graffiti advertisement for Samsung.

![](/media/2015/04/img_58841427971351693.jpg)

October is pumpkin and Halloween season, so it’s not hard to find any kind of pumpkin imaginable. It’s hypnotising. If I could, I’d bring all of them home with me, but only a few small ones could fit in hand luggage.

![](/media/2015/04/img_30491427971414768.jpg)

Organic family farms come to New York every week so that locals can enjoy some fresh lettuce, corn, kale and whatever those other vegetables and fruits are, I haven’t even seen many of these in my life.

![](/media/2015/04/img_30591427971461472.jpg)

![](/media/2015/04/img_30621428184000749.jpg)

This particular market is [on Union Square](http://www.grownyc.org/greenmarket/manhattan-union-square-m), but there are more like it practically in every neighbourhood.

![](/media/2015/04/img_30701427971563400.jpg)

We got caught up in Halloween fever too and craved a pumpkin with friends, while listening to [Ghostbusters](http://www.youtube.com/watch?v=m9We2XsVZfc) and [Monster Man](http://www.youtube.com/watch?v=xqrxW-pEq3Q).

![](/media/2015/08/img_36321440621910878.jpg)

One of the best parts of the otherwise busy Manhattan — Lower East Side. Tribeca, SoHo, Greenwich Village, NoHo/Nolita, West Village (I tend to avoid Midtown, but depends on the mood).

![](/media/2015/04/img_59641427971743884.jpg)

Another one of my favourite spots — The High Line Park. I’ve [written about it before](http://arturpaikin.com/ru/new-york-and-san-francisco), but since then they’ve built a new part.

![](/media/2015/04/img_33321427971903876.jpg)

![](/media/2015/04/img_58361428158121354.jpg)

![](/media/2015/04/img_58401428158143942.jpg)

![](/media/2015/04/img_58321428158040531.jpg)

![](/media/2015/04/img_33271428158084520.jpg)

The Flash was about to air on TV, so the fastest man alive ads are all over town.

![](/media/2015/04/img_58411428158165094.jpg)

Ok, time to head back to Brooklyn, and let me take this moment to tell you about the subway in New York. It’s got a pretty good coverage of the city and works 24/7. Well, that is if you are lucky and your line or station is not undergoing a renovation, which happens quite often actually. But there is always a sign with an explanation of an alternative route and directions to the nearest station. Or even a live gentlemen who’ll happily point you in the right direction.

These are the funny looking machines you use to buy tickets.

![](/media/2015/04/img_31021428184099216.jpg)

When you make a purchase with a card on an automated machine in the US, you always have to enter your Zip Code. Even if you are not from the US — the machine doesn’t care. So I looked up one for New York City (10555) and just used it all the time. If my understanding is correct, the code is tied to a credit card and acts as a verification number. Still, it feels weird and ancient every time I enter it. Like mailing someone a check or something.

Oh, and if your card is issued outside of the US, you have to choose “credit card” instead of “debit”, even if you have the latter. Otherwise it doesn’t work.

Another gotcha is how fast you swipe the card — be it bank card or metro or something else. Swipe speed matters all the time. You may call yourself a real New Yorker once you can get on the train in just one MetroCard swipe. First week in NYC I have to try 3–4 times on the turnstile before I get the speed right. We even have a competition with Lena, as to who will get in the subway first.

Many stations look nice, especially in trendy neighbourhoods. Some are high overground and [recommended by locals](https://instagram.com/p/7QRFEWQ8TA/).

![](/media/2015/04/img_31121427972512364.jpg)

But some are in need of attention, with water leaking from the roof, cold wind blowing, rats and trash. There are stations where, if you get in on the wrong side of the platform (say Queens bound train instead of Brooklyn bound), you can’t change direction without having to exit the station and paying again, which is insane.

New York is very diverse. The crowd on the train will change every few stops, and you can usually tell if it’s Williamsburg (beards and glasses), Park Slope (families with beards and glasses), Bed-Stuy (guns, kidding, maybe), Bushwick or upper-Manhattan (suits) just by looking at people around you.

North of DeKalb Avenue station you can [see a moving cartoon](https://www.youtube.com/watch?v=PDJtemNsfrs) while passing by on a B train.

<figure>
  <img src="/media/2015/08/img_58171440533786167.jpg">
  <figcaption>You know what really grinds my gears? Seamless and similar food delivery startups are placing ads all over in an attempt to persuade you to never leave the house.</figcaption>
</figure>

On a late night L-train to my Airbnb room in Bed-Stuy, two guys sitting next to me are discussing everything they see around, including the breast enlargement ad:

“Oh, look! For 39 hundred dollars you can get a really weird pair of tits”.  
“You could travel around Europe for that money!”, I joined in.  
“But then you wouldn’t have that weird pair of tits!”

Last stop in Manhattan — [Georgetown Cupcakes](http://www.yelp.com/biz/georgetown-cupcake-new-york). Pink for the win.

![](/media/2015/08/img_33751440597809736.jpg)

## Carroll Gardens, Cobble Hill, Park Slope

If my understanding is correct, most people visiting New York never leave Manhattan. That makes me sad, because New York I love the most is in Brooklyn. It has [changed a lot](http://www.wired.com/2015/09/photos-brooklyn-hipsters) since Miranda from Sex and the City dreaded moving there (“even cabs don’t go to Brooklyn!”) with her family and worried her Manhattan friends won’t come visit. Things are very different today (and even Sarah Jessica Parker [admits that](http://www.dailymail.co.uk/travel/article-1214474/Leave-Manhattan-tourists-Brooklyn-New-Yorks-real-gem.html)).

Cobble Hill and Carroll Gardens are awesome. I love brownstones and quite family neighbourhoods.

![](/media/2015/08/img_33361440534825977.jpg)

Brooklyn Farmacy and Soda Fountain — my favorite place, [recommended](http://www.scouted.in/new-york-city/jonnie-hallman) by Jonnie Hallman.

![](/media/2015/08/img_59771440592854335.jpg)

Great retro atmosphere, friendly people.

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-l">
    <img src="/media/2015/08/img_57421440592893113.jpg">
  </div>

  <div class="column-1-2 height-l">
    <img src="/media/2015/08/img_59791440593124793.jpg">
  </div>
</div>
</figure>

Here I was introduced to a magical concept: pour some soda into a glass, then add an ice-cream ball to it. Probably never would have thought of it myself.

<figure>
<img src="/media/2015/08/img_57481440592936965.jpg">
<figcaption>Check out their <a href="https://instagram.com/brooklynfarmacy/">sweet Instagram</a> too</figcaption>
</figure>

I mentioned earlier it was Halloween time — every other house in New York was decorated with pumpkins, witches, crows, gravestones and skeletons.

![](/media/2015/08/img_59731440596252471.jpg)

![](/media/2015/08/img_59751440596252506.jpg)

![](/media/2015/08/img_59531440596252538.jpg)

This is roughly what streets in Cobble Hill, Park Slope, Fort Greene and Carroll Gardens look like. I could walk here forever and not get bored.

![](/media/2015/08/img_58631440596494698.jpg)

<figure>
  <img src="/media/2015/08/img_58541440596712811.jpg">
  <figcaption>Belive it or not, this cat tried to either attack or befriend me. I had to escape quickly.</figcaption>
</figure>

We continue our stroll down the street and reach a community garden (farm).

![](/media/2015/08/img_58531440596869843.jpg)

Neighbours come here to grow food like tomatoes, cucumbers and kale, make compost and spend some quality time away from their “busy” city. It is free for visitors to enter and look around (as long as there is someone inside and the door is open). Love the idea.

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2015/08/img_58481440596988845.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2015/08/img_58501440596988864.jpg">
  </div>
</div>
</figure>

There is even a [Museum of Reclaimed Urban Space](http://www.morusnyc.org) that does tours on community gardens such as this one.

In one of the gardens, neighbours planted a tree in memory of their friend who passed.

![](/media/2015/09/img_58791443276534400.jpg)

Some complain about Park Slope, this is what I heard from a woman who moved there two years ago with her family:

> Everyone here is the same: wife is a designer, husband is an architect. They’ve got two kids, a boy and a girl, both about two years old. Everybody. Drives me crazy!

Retro cinemas are a thing in the US, here is one from Brooklyn Heights and Cobble Hill:

![](/media/2015/08/img_59341440598016749.jpg)

![](/media/2015/08/img_59361440598016737.jpg)

Next stop — [Park Slope Food Coop](http://www.foodcoop.com/).

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2015/08/img_58581440598271627.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2015/08/img_58591440598658917.jpg">
  </div>
</div>

  <figcaption>The guy with a beard did a little tour of the store for us, he is a member and works at the coop a few hours on weekends</figcaption>
</figure>

One of the oldest food cooperatives in operation:

> The Park Slope Food Coop, located in the heart of the Park Slope section of Brooklyn, New York, was founded in 1973 by a small group of committed neighbors who wanted to make healthy, affordable food available to everyone who wanted it. PSFC has more than 15,500 members, most of whom work once every four weeks in exchange for a 20-40% savings on groceries. Only members may shop at the PSFC, and membership is open to all.

Brooklyn Public Library. Impressive on the outside, less so on the inside (compared to my favourite New York Public Library in Manhattan). But what’s really interesting on this picture is the jewish guy performing a traditional union act, with [four types of plants](https://twitter.com/ymrn03/status/647384810919436288/photo/1?ref_src=twsrc%5Etfw). There’s a little dance involved.

![](/media/2015/08/img_58621440598919265.jpg)

During my two-week stay I’ve witnessed this ritual a couple times around the city and even participated twice myself.

Great beer place — [Bierkraft](http://www.yelp.com/biz/bierkraft-brooklyn) (got permanently closed, according to Yelp, while I was writing this post — a lot of stuff happens while I’m writing posts).

![](/media/2015/08/img_58651440621674535.jpg)

*Here is a [Wi-Fi network joke](/media/2015/09/img_58671442747213190.jpg) you might or might not get.*

Support you gay child ad nearby the Prospect Park.

![](/media/2015/08/img_59521440622107077.jpg)

![](/media/2015/08/img_34551440621852100.jpg)

***

This concludes the first chapter, but the story continues with even more colors and authentic neighbourhoods in [Chapter Two](/en/brooklyn-chapter-two).
