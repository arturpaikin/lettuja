---
title: "Brooklyn. Chapter Two"
description: "In this chapter: Stunning views of Red Hook, bicycles, Dumbo and Studiomates. The Brick theatre and a robot actor. Surreal couchsurfing in Williamsburg. Gentrification or how neighbourhoods change: Bushwick, Bed-Stuy and Gowanus. Smorgasburg flea food market. And delicious doughnuts"
cover: "http://arturpaikin.com/media/2015/09/img_58851443362218892.jpg"
datePublished: 2015-09-29 15:40
category: "new-york"
published: true
---

<figure class="wide">
  <img src="/media/2015/09/img_58851443362218892.jpg">
</figure>

This is Chapter Two of my Brooklyn story (read the beginning in [Chapter One](/en/brooklyn-chapter-one)).

In this chapter: Stunning views of Red Hook, bicycles, Dumbo and Studiomates. The Brick theatre and a robot actor. Surreal couchsurfing in Williamsburg. Gentrification or how neighbourhoods change: Bushwick, Bed-Stuy and Gowanus. Smorgasburg flea food market. And delicious doughnuts.

<!--more-->

## Red Hook. And a piece of key lime pie

Turns out there is a whole neighbourhood in Brooklyn I’ve never heard about. It is called Red Hook and is known among New Yorkers because Ikea is there (spoiler: not the only thing there, despite the popular opinion). You can get to Red Hook on [Ikea Water Taxi](https://www.nywatertaxi.com/ikea) from Manhattan and the fare will be discounted from your later purchase at Ikea — smart.

But today we are not gonna go this way. We are walking through Cobble Hill, grabbing a cappuccino on the corner at Smith Canteen.

![](/media/2015/09/img_57511442662013239.jpg)

Then pass through an area filled with metal recycling, tire shops and storage spaces, and cross the bridge.

![](/media/2015/09/img_63831442662425442.jpg)

![](/media/2015/09/img_57531442663777812.jpg)

![](/media/2015/09/img_57551442663777807.jpg)

![](/media/2015/09/img_57581443285038363.jpg)

![](/media/2015/09/img_57621442663777803.jpg)

![](/media/2015/09/img_57571442663777796.jpg)

![](/media/2015/09/img_31391442664203548.jpg)

Red Hook reminds me of California at its best.

<figure>
<div class="row wide collage">
  <div class="column-full">
    <img src="/media/2015/09/img_57611442682775009.jpg">
  </div>
</div>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2015/09/img_57671442664203527.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2015/09/img_31481442664203554.jpg">
  </div>
</div>
</figure>

Fairway grocery store is located right in one of Red Hook’s signature warehouse buildings. I buy some fresh fruits and cheese there to enjoy, overlooking the ocean.

There is a common postulate that food in America is bad and unhealthy, which is hard to believe once you actually try it. I know it depends on a place, and a lot of people don’t know [how to cook proper meals](http://www.youtube.com/watch?v=go_QOzc79Uc), and in some areas you’ll have to wait for [your own food to grow](http://www.youtube.com/watch?v=EzZzZ_qpZ4w) before you’ll be able to eat something fresh. But where I’ve been, the variety and quality of food has amazed me.

Never before have I seen so much good looking potatoes, broccoli and mushrooms in my life:

<figure>
<div class="row wide collage">
  <div class="column-1-3 height-l">
    <img src="/media/2015/09/img_57701442664444248.jpg">
  </div>

  <div class="column-1-3 height-l">
    <img src="/media/2015/09/img_57711442664444262.jpg">
  </div>

  <div class="column-1-3 height-l">
    <img src="/media/2015/09/img_57721442664444270.jpg">
  </div>
</div>
</figure>

*This is a good time to remember how Russian soon-to-be president Elcin [visited an American supermarket in 1989](http://samsebeskazal.livejournal.com/348049.html). I can relate to that. I mean, it’s basically me at Whole Foods.*

And of course, where would we be without our dear friend, the pumpkin.

![](/media/2015/09/img_57681442675520027.jpg)

![](/media/2015/09/img_57751442676318828.jpg)

![](/media/2015/09/img_31571442676318839.jpg)

![](/media/2015/09/img_57781442676707771.jpg)

![](/media/2015/09/img_57561442675943823.jpg)

Is this GTA V or what? Waiting for [Trevor](http://gta.wikia.com/wiki/Trevor_Philips) to jump in the truck.

![](/media/2015/09/img_57891442678189708.jpg)

![](/media/2015/09/img_57591443290329976.jpg)

They know a thing or two about urban farming in Red Hook.

![](/media/2015/09/img_57771442697343593.jpg)

I promised you a piece of Key lime pie, so here we go to [Steve's Authentic Key Lime Pies](http://stevesauthentic.com/wpnew/).

![](/media/2015/09/img_57911442741605558.jpg)

![](/media/2015/09/img_57931442741657207.jpg)

It is delicious, even more so when paired with this view.

<figure>
<div class="row wide collage">
  <div class="column-full">
    <img src="/media/2015/09/img_63931443448995551.jpg">
  </div>
</div>
<div class="row wide collage">
  <div class="column-1-2 height-l">
    <img src="/media/2015/09/img_31961442741685447.jpg">
  </div>

  <div class="column-1-2 height-l">
    <img src="/media/2015/09/img_4889-21442744201581.jpg">
  </div>
</div>
</figure>

[Red Hook Winery](https://www.redhookwinery.com/) is very much worth a visit — I was lucky to take part in a “Doors Open Days” tour and got to see the backstage with all the barrels and even looked inside one. We were told many stories about the winery, including the one where mayor Bloomberg visited a few days before Sandy hurricane hit New York. He liked the wine so much that after the hurricane he called to ask if everything was ok (Red Hook suffered badly). And when he heard that the wine is going to be ruined without electricity in the next few days, he made sure they got power back in time.

<figure>
<div class="row wide collage">
  <div class="column-1-3 height-l">
    <img src="/media/2015/09/img_57841442741747134.jpg">
  </div>

  <div class="column-1-3 height-l">
    <img src="/media/2015/09/img_31821442741747128.jpg">
  </div>

  <div class="column-1-3 height-l">
    <img src="/media/2015/09/img_57851442741747117.jpg">
  </div>
</div>
</figure>

The wine is flavourful and exquisite.

But if you are into whiskey, today is your lucky day, too. Cacao for everyone!

![](/media/2015/09/img_32121442745254958.jpg)

![](/media/2015/09/img_57971442745543706.jpg)

This ship [can get you](http://www.travelweekly.com/Cruise/Princess-Cruises/Ruby-Princess/Schedule) to Alaska, Hawaii, US West Coast or Mexico.

![](/media/2015/09/img_57951442745841377.jpg)

The day ends with a concert in a giant old garage. Listeners park their bikes outside, sit on the floor, drinking beer, their kids playing nearby.

![](/media/2015/09/img_57991442745641013.jpg)

## Dumbo, Studio Mates and Bicycles

After Brooklyn Beta I was very eager to visit [Studio Mates](http://studiomates.com/) (currently [Friends Work Here](http://www.friendsworkhere.com)) — the coworking space started by Tina Roth-Eisenberg ([SwissMiss](http://www.swiss-miss.com/)), where a bunch of great people work every day.

To get to Dumbo from Manhattan I decided to give [Citi Bike](https://www.citibikenyc.com/) a try. It’s a bike sharing system, like [the one you’d find in Paris](http://arturpaikin.com/en/paris-amsterdam). It is pricy in New York though: $10/day and then $4/hour (as usual, the first 30 minutes are free, if you’ll manage to find the next station and return your bike in time). Overall I was charged $20 for the day, and this is probably what I would have payed if I got the bike from the old-school rental place. Only I would’t have to worry so much and constantly check the time in search for the next station. I didn’t have Internet on my phone, so I had to stop strangers and ask for help.

*On [the back of the receipt](/media/2015/09/img_61231443279377236.jpg) it says MasterCard is the “preferred payment provider” and lists a few basic cycling rules.*

![](/media/2015/09/img_59651443327458090.jpg)

In terms of cycling around the city, New York is no Copenhagen, but it is pretty decent. There are bike lanes (even dedicated on some bridges) and parkings,  drivers are mostly respectful and aware of cyclists and Google Maps are there to help you find the best route.

![](/media/2015/09/img_58921443197397122.jpg)

So I biked across the magnificent Brooklyn Bridge.

<figure class="wide">
  <img src="/media/2015/09/img_58931443361537290.jpg">
</figure>

And arrived at Jay street.

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2015/09/img_58071443221960636.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2015/09/img_58961443361928784.jpg">
  </div>
</div>
</figure>

Since this is America, there are rules.

<figure>
<img src="/media/2015/09/img_33931443197038715.jpg">
<figcaption>if (occupancy > 563) Disaster.begin()</figcaption>
</figure>

[Jen](http://jenmussari.com) was kind enough to give me a brief tour. The place rocks, you guys.

![](/media/2015/09/img_59251443196826275.jpg)

Friendly people, nice view, cool dogs.

![](/media/2015/09/img_58991443197254931.jpg)

![](/media/2015/09/img_59031443196989028.jpg)

Later I bought two slices of pizza from an Italian place across the street and enjoyed the sunset over Manhattan.

![](/media/2015/09/img_59111443222046864.jpg)

## Williamsburg and The Brick

Time to hurry up for the play at The Brick theatre in Williamsburg.

In Russia theatre is kind of a big deal. People of all ages go every now and then, some even more often than to the movies. Classical plays are coupled with modern, by [cool-looking directors](/media/2015/09/724651443477637063.jpg) who wear beards and dark blue square-shaped glasses. Plays mostly reflect on rough life and poor man’s struggles in the country, but from a hipster angle.

In New York it seems, theatre is conceived as this classical thing, like Broadway, more expensive and generally popular among tourists.

Although, there are quite a few pleasant exceptions. [Sleep no More](http://www.sleepnomore.com), where you are free to roam around various rooms and participate in activities during the “play”. [Division Avenue](http://www.divisionavenue.net/index.html) — a play about Hasidic culture, bicycles and Williamsburg. And then [The Brick](http://bricktheater.com) — small, casual, relaxing atmosphere, inexpensive (around $20).

![](/media/2015/09/img_59141443224686966.jpg)

There were three actors: a cleaning lady, a man and a robot. Robot was actually moving and talking. The play is about young man’s struggles, humans, technology and how it impacts our lives.

To my surprise, there were just a few people in the audience — the room could fit about 50 and 1/3 of the seats were empty — again, in Moscow there would be like 300 people easily. All in all, I highly recommend The Brick, I’ve been twice on different plays and will gladly go again.

It’s been a long day, time to head home for a nap. On this trip I split time between staying with friends, Couchsurfing and Airbnb. Lena found this nice couple from Williamsburg and here we are.

![](/media/2015/09/img_58181443262223799.jpg)

Our hosts are Anette, a friendly babysitter and her husband [Scott](http://www.imdb.com/name/nm0925441/), producer of the TV show [Gotham](https://en.wikipedia.org/wiki/Gotham_(TV_series)) (at the time ads for Gotham were on every other bus in New York, so I felt like I was either dreaming or high). Next thing we know, we are eating dinner and playing Mortal Combat with a couple from [German part of Italy](https://en.wikipedia.org/wiki/South_Tyrol) who traveled around the US and Canada in a van, and Silvija, a historian and author of [Public Domain City](http://publicdomaincity.tumblr.com/) project, from Lithuania. Everyone gets their own self-inflating bed.

The view out the window.

![](/media/2015/09/img_58201443262226587.jpg)

Staying with them was super nice and surreal, Couchsurfing is great. Next week Silvija invited us to her small birthday party at the High Line where we had a great time and made friends.

![](/media/2015/09/img_59541443267415561.jpg)

<figure>
  <img src="/media/2015/09/img_59311443280605168.jpg">
  <figcaption>Late evening on the Metropolitan Ave station in Williamsburg. The guy is singing “you are my sunshine, my only sunshine...” and then casually continues “postoi paravoz, ne stuchite kolyesa, konductor, nazhmi na to-rmo-zaa”</figcaption>
</figure>

What’s the best possible breakfast? A slice of pizza on Bedford.

![](/media/2015/09/img_58461443275319300.jpg)

## Bed-Stuy, Bushwick and Gowanus

New York is all about transformations — turning a boring unpleasant neighbourhood into a cool and attractive “the place to be”, while attempting to preserve its heritage and history.

My last home this time is an Airbnb room in Bed-Stuy for $50/night. Roommate has a playful pitbull, smokes hash, edits movies and is trying to become an art dealer. There is roof access, which is common in NYC.

As I explained before, the neighbourhood has a reputation (“Bed-Stuy — Do or Die” is the slogan after all), but is actually quite safe and vibrant today. Though you will sometimes see people arguing loudly and police patrols in neighbourhoods like this more often than in others.

Someone on Yelp [summs it well](http://www.yelp.com/list/bed-stuy-do-or-die-brooklyn-4): “no-longer-so-dangerous-but-thankfully-still-not-totally-gentrified Brooklyn neighborhood”.

* [This is Bed-Stuy](http://www.youtube.com/watch?v=gfaQP4TkHj8) — promotional video by a real estate agency that explains why Bed-Stuy is cool:
    <a href="http://www.youtube.com/watch?v=gfaQP4TkHj8" target="_blank"><img src="/media/2015/09/bed-stuy-promo1443426730262.jpg"></a>
* And then the news story on “[Longtime Bed-Stuy residents feeling alienated](http://pix11.com/2015/08/25/longtime-bed-stuy-residents-feel-alienated-after-promotional-video-highlights-gentrification/) after promotional video highlights gentrification”.
* [Bed-Stuy and Bushwick booming despite high crime](http://nypost.com/2014/09/15/bed-stuy-and-bushwick-booming-despite-high-crime/)
* [Man Shot in Leg on Clinton Hill/Bed-Stuy Border: NYPD](http://patch.com/new-york/bed-stuy/man-shot-leg-clinton-hillbed-stuy-border-nypd-0)

It’s a problem I’ve never really considered before: a neighbourhood becomes more attractive and gentrified, and the locals who’ve lived there for generations are driven away, because now they can’t afford the place or feel unwanted here.

[Dough](http://www.doughbrooklyn.com) are some real fine doughnuts fried in Bed-Stuy. Every morning there is a line for coffee (terrible) and doughnuts. I was just passing by, but people in line persuaded me it’s worth the wait (it’s New York, people talk to each other here).

![](/media/2015/09/img_59291443267561578.jpg)

“Wake up, honey!”, says the lady at the counter when I forget my coffee.

![](/media/2015/09/img_64411443268400123.jpg)

In Bed-Stuy you can like a church on Facebook.

![](/media/2015/09/img_58801443272147316.jpg)

Bushwick. Five years ago — warehouses, industrial buildings, crime. Today — streets are paradise, everywhere you look someone is painting something cool. Juice bars, coffee and bike stores pop up here and there.

![](/media/2015/09/img_35471443275714298.jpg)

![](/media/2015/09/img_59421443276029287.jpg)

[Bussiness Insider](http://www.businessinsider.com/photos-of-bushwick-gentrification-2013-11):

> As the rents in Williamsburg, Brooklyn have skyrocketed, young professionals have been begun to retreat deeper along the L train to Bushwick. For better or worse, the result has been rapid gentrification.The serious crime rate has dropped from 36% in 2000 to 24.4% as of 2011.
>
> Now everywhere you look there are juice bars, organic markets, craft beer stores, bike shops, and more things that hipsters love. Best of all, the rents are still relatively cheap.

But sometimes, as I’ve mentioned earlier, the change might have a negative impact on native residents. [What If It Were Me? One Writer’s Look at Gentrification in Bushwick](http://bushwickbuzz.com/blog/what-if-it-were-me-one-writers-look-at-gentrification-in-bushwick/):

> How would I feel if someone moved into my neighborhood, not knowing its history, or what it takes to be “loyal to the block”?
>
> The Census Bureau published in their 2009-2013 Census that “neighborhoods gentrifying since 2000 recorded population increases and became whiter”.
>
> I asked the owner of my block’s bodega what he thought of the changing neighbourhood, and he replied, “[I’ve] been here for 20 years. And every year the [rent goes] go up by $100. First, I pay $650, then $750, then $850, and up and up, ’til it [gets] to $1050. No tengo.”
>
> The fact that Bushwick offers an artsy, “keeping-it-real” vibe at a lower price than your costly apartment on the LES makes it a goldmine to an aspiring artist such as myself. But it doesn’t do the neighborhood folks much good. They’re slowly being pushed out due to higher costs of living, and an ever-changing landscape leaves them unable to sustain the traditional life to which they have grown so accustomed.

![](/media/2015/09/img_35521443275863205.jpg)

There is a street in Bushwick called “Brave Street” which is also the Wi-Fi password at a local hip coffee place. And one of the other networks is called “I was shot in Bushwick”.

![](/media/2015/09/img_35441443275928295.jpg)

Graffiti continues even on cars.

![](/media/2015/09/img_35531443276419689.jpg)

When in Bushwick, stop for lunch at their signature pizza place — [Roberta’s](http://www.yelp.com/biz/robertas-brooklyn-2).

View of Manhattan from the Whole Foods in Gowanus, over the solar-panel-covered parking.

![](/media/2015/09/img_58721443276758625.jpg)

Gowanus is yet another example of transformations. In 1636 — the first settlement of Dutch farmers in Brooklyn, later an industrial neighbourhood with mafia and a polluted canal, where they’d dump toxic waste and bodies:

* [9 Horrifying Things About Gowanus Canal](http://www.businessinsider.com/9-horrifying-things-about-brooklyns-gowanus-canal-2013-10)
* [What Would Happen If You Drank Water From The Gowanus Canal?](http://www.popsci.com/science/article/2013-08/fyi-what-would-happen-if-you-drank-water-gowanus-canal)

Today the canal has been [cleaned a little](https://en.wikipedia.org/wiki/Gowanus_Canal#Cleanup_efforts) (the cleanup should be completed by 2022) and you can appreciate a unique neighbourhood. Makers, designers and developers like [Anton & Irene](http://antonandirene.com/), [Ghostly Ferns](http://ghostlyferns.com/) and others are now moving to Gowanus.

Jonnie Hallman, creator of [Cushion](http://cushionapp.com/) and author of a great Creative Mornings talk [about his dad and model airplanes](http://destroytoday.com/blog/creativemornings/) posts a [picture of Gowanus](https://twitter.com/destroytoday/status/527882493576613888) on twitter and comments:
> And here I thought Studiomates was losing its beautiful view. Not too shabby, Gowanus.

While you are at it, have a piece of pecan pie at [Four & Twenty Blackbirds](http://www.birdsblack.com/).

![](/media/2015/09/img_58731443276937217.jpg)

Also worth visiting: [Ample Hills Creamery](http://www.yelp.com/biz/ample-hills-creamery-brooklyn-5) for their insanely good ice-cream and [Twig Terrariums](http://twigterrariums.com/) for, well, terrariums.

![](/media/2015/09/img_58701443280533344.jpg)

![](/media/2015/09/img_33471443280605176.jpg)

## Brooklyn Heights and more Manhattan

![](/media/2015/09/img_34701443277686669.jpg)

Phone charging station.

![](/media/2015/09/img_58101443285145768.jpg)

On Sunday [Smorgasburg](http://www.smorgasburg.com/) — the famous Brooklyn flea food market — is held at the [Brooklyn Bridge Park](http://www.brooklynbridgepark.org/) (locations change, refer to [the timetable](http://brooklynflea.com/)). From Mexican fried corn ($4) and French fries to grilled cheese and Blue Marble Ice Cream: [http://www.smorgasburg.com/vendors](http://www.smorgasburg.com/vendors).

![](/media/2015/09/img_34861443277770960.jpg)

![](/media/2015/09/img_32821443328108462.jpg)

Close by is a classic American roller skating rink. This is cool, never seen anywhere else — you rent a pair of skates on four wheels and ride in circles.

<figure>
  <img src="/media/2015/09/img_58141443328244982.jpg">
  <figcaption>In action it looks <a href="http://www.youtube.com/watch?v=_JYqO8JanJs">like this</a></figcaption>
</figure>

<figure class="wide">
  <img src="/media/2015/09/img_59571443277722640.jpg">
</figure>

Stopping for coffee at [Kaffe 1668](http://www.yelp.com/biz/kaffe-1668-new-york) with a weird semi-automated Aeropress-like coffee machine.

![](/media/2015/09/img_59601443278062717.jpg)

![](/media/2015/09/img_59681443330329847.jpg)

It starts raining.

![](/media/2015/09/img_36531443278492420.jpg)

Perfect timing to explore Fish Eddie’s — a paradise for your inner china-loving-self.

<figure>
<img src="/media/2015/09/img_59841443278739485.jpg">
<figcaption>Americans take a lot of pride in everything made in America</figcaption>
</figure>

![](/media/2015/09/img_59871443278813520.jpg)

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2015/09/img_36461443278813532.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2015/09/img_59861443278813543.jpg">
  </div>
</div>
</figure>

While admiring spoons I overheard a woman at the cashiers: “John, this is the founder of Dough Donuts”.

I’m hungry, so I stop near Moma (corners of 53rd st and 6th Avenue). There is a line of people moving quickly, and at first I thought its for an exhibition or something. But what it actually is is the [Halal Guys](http://thehalalguys.com/). They make delicious plates with chicken or falafel and vegetables for about $5. They are quite famous, [operating for over 25 years](https://en.wikipedia.org/wiki/The_Halal_Guys).

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2015/09/img_57351443284774572.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2015/09/img_57391443284726197.jpg">
  </div>
</div>
</figure>

It’s getting dark, time to wrap up our story.

![](/media/2015/09/img_36721443279817279.jpg)

![](/media/2015/09/img_59691443279331545.jpg)

But we have just a little bit of time left for a beer at [The Double Windsor](http://www.yelp.com/biz/the-double-windsor-brooklyn).

<figure>
  <img src="/media/2015/09/img_59901443279879204.jpg">
  <figcaption>By the way, if you want to blend in in Brooklyn, consider drinking your beer from a <a href="http://www.theatlantic.com/technology/archive/2015/09/mason-jar-history/403762/">mason jar</a>.</figcaption>
</figure>

And a ride on the East River ferry.

![](/media/2015/09/img_59971443284466067.jpg)

I pack my newly acquired things (mostly bagels and Marple syroup) in a [ridiculous 99 cents bag](/media/2015/09/img_60011443279949640.jpg) and head to JFK.

![](/media/2015/09/img_58761443280426980.jpg)

## The End

### Tips & tricks
* I manage to survive without a data plan on my phone for up to a month when travelling, but I admit it’d be easier if I had one. [T-mobile Walmart Exclusive plan](http://www.walmart.com/ip/T-Mobile-Complete-SIM-Kit/39081494) is the best for that, [I hear](https://twitter.com/m4rr/status/589870595950546944). About $30/month for 5GB data.
* Buy a [metro card for a week](http://web.mta.info/metrocard/mcgtreng.htm#7day) (or a month if you are staying that long), it works on subway and buses and will give you flexibility and peace of mind.

### Soundtrack
* [Macklemore and Ryan Lewis — The Heist](http://www.youtube.com/watch?v=lxZPNXRT2fQ&list=PLQ5OciHKz4J_f_vE4Skf-EXLJxgisFP3I)
* [Imagine Dragons — Night Visions](http://www.youtube.com/watch?v=Thbsg9i2mZ0&list=PLvklK-C2o3vxMbmDFvYAJjRcXR2UekEGL)
* [Suzanne Vega — Toms Diner](http://www.youtube.com/watch?v=FLP6QluMlrg)
* [Sting — English Man in New York](http://www.youtube.com/watch?v=d27gTrPPAyk)

### Links
* Scouted guides: [Jonnie Hallman](http://www.scouted.in/new-york-city/jonnie-hallman), [Meg Lewis](http://www.scouted.in/new-york-city/meg-lewis)
* [Hello New York](http://www.hello-newyork.com/) — great neighbourhood guide
* [On The Grid](http://www.onthegrid.city/new-york/) — awesome neighbourhood guide (not only for NYC, curators for each neighbourhood)
* Lena’s [post on New York](http://blog.elenazaharova.com/post/travel-in-new-york) (in Russian, check out the summary in the end too — lots of great places to visit) and cool [map of Brooklyn](http://blog.elenazaharova.com/post/my-brooklyn-map) (in English)
* [October in New York](https://medium.com/russian/-c28c06f07696) by Katerina Kai (in Russian)
* [Pictures of New York](http://blog.repponen.com/blog/?tag=New+York) by Anton Repponen and his [New York photo guide](https://readymag.com/repponen/NewYork-PhotoGuide/)
