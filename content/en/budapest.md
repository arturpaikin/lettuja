---
title: A Recipe for a Great Weekend in Budapest
datePublished: 2014-06-07 12:15
cover: http://arturpaikin.com/media/budapest/IMG_4868.jpg
published: true
---

![image](/media/budapest/IMG_4868.jpg)

I have spent three wonderful days in Budapest.

It was tasty, inexpensive and sunny, with fresh pastries, awesome bars, thermal baths, parks and bridges.

<!--more-->

***

*I got the tickets from Moscow from [Wizzair](http://wizzair.com/ru-RU/Search), for around 150$ both ways.*

During the flight I was engaged in my favorite pastime: secretly listening to podcasts without letting the flight attendant notice and bug me with the usual ‘Sir, I’m gonna ask you to turn off your electronic device’ crap (which has already [been canceled](http://www.reuters.com/article/2013/10/31/us-usa-aviation-devices-idUSBRE99U0R320131031) in the United States). The trick is to use  the left headphone, holding it to your ear, as if you were simply holding your head.  

Then we have arrived.

Subway carriages in Budapest are the same as in Moscow, only older.

![image](/media/budapest/IMG_4776.jpg)

Going to my Airbnb-rental through Liberty Bridge. Eating an onion soup out of plastic glass — a common thing here. Delicious.

![image](/media/budapest/IMG_0829.jpg)

![image](/media/budapest/IMG_4778.jpg)

The signature yellow Budapest tram is crossing the bridge. There should also be the longest tram in the world (50 meters) made by Siemens, but I haven’t seen it in three days.

![image](/media/budapest/IMG_4870.jpg)

![image](/media/budapest/IMG_4867.jpg)

I got to the apartment and took the key. The view of the school yard across the balcony.

![image](/media/budapest/IMG_4807.jpg)

This is the porch.

![image](/media/budapest/IMG_4808.jpg)

![image](/media/budapest/IMG_0674.jpg)

The door.

![image](/media/budapest/IMG_4875.jpg)

The apartment turned our to be cozy and inexpensive (30$ per night)

After a short nap, it’s time to discover the city. A lot of nice graffiti on the way.

![image](/media/budapest/IMG_0688.jpg)

![image](/media/budapest/IMG_4799.jpg)

![image](/media/budapest/IMG_0809.jpg)

If you are lucky, you can even swim in the fountain.

![image](/media/budapest/IMG_0538.jpg)

These huge banners in downtown suggest that beards, tattoos, mustaches and funny-looking kettles are not far.

![image](/media/budapest/IMG_0700.jpg)

And here we go. No beard, but the Kalita is right here.

![image](/media/budapest/IMG_4812.jpg)

And a cool futuristic cash register.

![image](/media/budapest/IMG_4813.jpg)

And look at this wall.

![image](/media/budapest/IMG_4811.jpg)

Coffee at [Tamp & Pull Espresso Bar](https://www.facebook.com/tamppull) is not bad, they order it from an English roaster and deliver by trucks, so that the change of pressure from an airplane won’t affect the beans.

After coffee I got a hankering for some fresh pastries, so heading to [Butter Brothers](https://www.facebook.com/pages/Butter-Brothers/1383017678583129).

![image](/media/budapest/IMG_0803.jpg)

Great place. Croissants and buns (especially with cheese and pear), different kind every day. And the fresh bread. And it’s cheep too, around 8$ for breakfast for two.

![](/media/budapest/IMG_4817.jpg)

*When we were unsure if we have enough cash, the guy at the register said: “no problem, just bring it tomorrow”. People know each other at this kind of places and croissant loans are quite natural.*

I also suggest that you visit the [Cake Shop](http://www.cakeshop.hu/) bakery, where everything is delicious and baked right in front of you.

![image](/media/budapest/IMG_4901.jpg)

When it comes to pillars, trees, flowerbeds, fenders and squares and such, everything is great on the streets of Budapest. It is a comfortable city for life and walks.

<figure>
  <img src="/media/budapest/IMG_4810.jpg">
  <figcaption>Plain black pillar, the essence of happiness</figcaption>
</figure>

![image](/media/budapest/IMG_4827.jpg)

![image](/media/budapest/IMG_4822.jpg)

![image](/media/budapest/IMG_0723.jpg)

Farmers market.

![image](/media/budapest/IMG_0741.jpg)

![image](/media/budapest/IMG_4825.jpg)

A park with a huge fountain.

![image](/media/budapest/IMG_4833.jpg)

![image](/media/budapest/IMG_4835.jpg)

![image](/media/budapest/IMG_0832.jpg)

Another nice cafe in Budapest is called [Blue Bird](https://www.facebook.com/bluebirdcafehungary). At first I thought it’s just a name, but then I heard a wild bawl from the first floor. I grabbed my bag and hurried downstairs to find this guy:

![Gosha, the Blue Bird](/media/budapest/IMG_0540.jpg)

<figure>
  <img src="/media/budapest/IMG_4792.jpg">
  <figcaption>— Fascinating!</figcaption>
</figure>

Gosha, the blue bird, quietly sneaked upon me and attacked the cheesecake. Later on he has been locked back in his cage for the crime. On the cafe’s page you can find some more pictures of him, like [Gosha-barista](https://www.facebook.com/bluebirdcafehungary/photos/a.212928035550270.1073741831.186018268241247/287057338137339/?type=1).

Pocket flora.

![image](/media/budapest/IMG_4787.jpg)

One of the points of interest in the city are its [ruin pubs](http://welovebudapest.com/clubs.and.nightlife.1/the.best.ruin.pubs.in.budapest). There is a whole neighborhood full of them in downtown. The first and most famous is [Szimpla Kert](http://www.szimpla.hu/).

The entrance to Szimpla is just an arch, there are no doors. Venue spaces in here expand outside the building, number of barriers is minimized.

![image](/media/budapest/IMG_4780.jpg)

Wine and beer costs like 2$.

![image](/media/budapest/IMG_4893.jpg)

![image](/media/budapest/IMG_0685.jpg)

Traditional long hungarian sandwiches.

*![image](/media/budapest/IMG_4786.jpg)*

As you know, I like to [sit in tubs](/spb-weekend).

![image](/media/budapest/IMG_4785.jpg)

At this cafe you can book a table right in the blue trailer at the front.

![image](/media/budapest/IMG_0687.jpg)

Besides ruin pubs, Budapest is famous for its thermal baths that have appeared in a great movie “[The Grand Budapest Hotel](http://www.imdb.com/title/tt2278388/)”.

The ones I’ve been to are called [Gellért Baths](http://www.gellertfurdo.hu/), but there are a few more. They are totally worth the visit.

![image](/media/budapest/IMG_4853.jpg)

![image](/media/budapest/IMG_4865.jpg)

![image](/media/budapest/IMG_0784.jpg)

*The ticket costs around 20$, including swimming suit rental, a locker and a white sheet that can be used as a towel. It’s free to just walk in and have a peek inside.*

There are some open pools for the good weather and closed for the bad one. In the open one there is an “artificial” wave every hour.

![image](/media/budapest/IMG_4843.jpg)

![image](/media/budapest/IMG_4850.jpg)

After a bath, heading to the bar at the abandoned bus station.

![image](/media/budapest/IMG_4899.jpg)

In order to try, wait for it: [wine with sparkling water](http://en.wikipedia.org/wiki/Spritzer) from the mini-shower. They even have proportions in the menu, like “3/4 wine & 1/4 sparkling water”.

![image](/media/budapest/IMG_4898.jpg)

![image](/media/budapest/IMG_0820.jpg)

As for dinner, here is what you can do. First, choosing the kind of pasta we want.

![image](/media/budapest/IMG_4830.jpg)

Then it’s cooked with olive oil, pesto and some cedar nuts.

![image](/media/budapest/IMG_4829.jpg)

And then we eat it at a park.

![image](/media/budapest/IMG_4831.jpg)

I advice you to hurry and visit Budapest, while the city hasn’t been completely occupied by tourists like Gorky Park on a summer weekend ;-)

![image](/media/budapest/IMG_0801.jpg)

![image](/media/budapest/IMG_0781.jpg)

### Related links
* Anton Repponen [on his trip to Budapest](http://blog.repponen.com/blog/2014/6/7/budapest-hungary)
