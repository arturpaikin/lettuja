---
title: "Speaking at Queens and Brooklyn JS"
description: "A place where everything happens, the New York City, has not one, but a handful of awesome JavaScript meetups. I’ve been following a few of them for a while from Russia: Brooklyn JS (the one that started it all), Queens JS, Mahnattan JS and Node Bots NYC (about controlling hardware with JavaScript, both beautiful and terrifying idea)."
cover: "http://arturpaikin.com/media/2016/10/20151217-viiy87941475617971291.jpg"
datePublished: 2016-10-06 12:00
published: false
---

![](/media/2017/05/r1w8i5vbb.png)

![image](/media/2016/10/20151217-viiy87941475617971291.jpg)

A place where everything happens, the New York City, has not one, but a handful of awesome JavaScript meetups. I’ve been following a few of them for a while: [Brooklyn JS](http://brooklynjs.com) (the one that started it all), [Queens JS](http://www.meetup.com/QueensJS/), [Mahnattan JS](http://manhattanjs.com), [Jersey Script](https://jerseyscript.github.io/) (not literally New York, but close) and [Node Bots](http://nodebots.nyc/) (on controlling hardware with JavaScript, a beautiful and terrifying idea).

Those are not your regular tech meetups — they are relaxed, fun, and very friendly to newcomers, both speakers and attendees. Read Jed’s awesome post about [how Brooklyn.js came to be](https://github.com/jed/building-brooklynjs).

During this whole winter I’ve maintained a near perfect attendance at most of these meetups, and even got to speak twice.

<!--more-->

---

## Queens.js

I was going to New York for Christmas and decided to pitch a talk about my Koti Home Automation project for Queens.js, and it got accepted! I’ve never given a conference or meetup talk before, so I was nervous and excited. Remember having a hard time falling asleep.

<figure>
  <img src="/media/2016/10/20160106-img_84911475619801116.jpg">
  <figcaption>Preparing for my talk at Milk & Pull in Ridgewood, Queens. Used Deckset for slides, it’s pretty cool!</figcaption>
</figure>

Preparing slides and content for the talk really helps structure your thoughts and summarise a project you’re talking about.

<figure>
  <img alt="The House Is Not On Fire" src="/media/2016/10/thehouseisnotonfire-2.jpg">
  <figcaption>Originally titled “Divide and conquer your house”</figcaption>
</figure>

I talked about my journey into hardware: from connecting a thermistor to an Arduino Uno, accessing it remotely through an Ethernet shield, adding BeagleBone and later Raspberry Pi for easier connectivity, cameras and Node.js support, to adding sensors, programming Telegram Chat Bot and Google’s voice recognition, and fixing up a control panel in React.

![image](/media/2016/10/20160106-img_85111475622483455.jpg)

The audience was very sympathetic, everybody clapped and laughed at my jokes, even when they were not that funny. Couldn’t have wished for a better place to give my first talk. Thanks, Sara, Nick, Suz and everyone who came that night. And Lena for supporting, helping me prep, and shooting pictures and the video.

![image](/media/2016/10/20160107-cyfnmtdwcaav0zv1475622670500.jpg)

Here are the [slides](http://bit.ly/notonfire) and [video](https://youtu.be/Z-NpPj6aKJE).

## Brooklyn.js

Later I came up with another project, an ESP8266 Internet-connected button that sends messages to me via Telegram Chat Bot. I talked about it at Brooklyn.js, which is a bit different format — just 10 minutes.

![image](/media/2016/10/20160318-img_89381475703655519.jpg)

[Video](https://youtu.be/CvorlYi4L_c) and [slides](http://bit.ly/mrrudolf).

![image](/media/2016/10/robot-talks-1.jpg)

Each meetup does something differently, Brooklyn JS, for instance, has musical guests that help soften the transition between talks, while the next speaker comes up and connects to the projector.

![image](/media/2016/10/20151217-img_82541475711720872.jpg)

![image](/media/2016/10/cdymuhow0aediqm1475703655536.jpg)

All day before the event there is Boroughgramming — coworking with kale, chips and nice crowd.

![image](/media/2016/10/20151217-img_823814757117208341475721069486.jpg)

![image](/media/2016/10/20151217-img_82631475711720878.jpg)

![image](/media/2016/10/20151217-piuy82281475711720882.jpg)

It’s hard to express the atmosphere and emotions that flow in the air at Brooklyn JS with words and pictures. I highly recommend that you [get a ticket](https://ti.to/BrooklynJS) and attend one day. Warning: tickets sell out in 5 minutes, set an alarm and hit the page at 10am sharp.

In the meantime, here’s an article: [How Brooklyn JavaScript Became the Heart of the Borough’s Tech Scene](http://www.bkmag.com/2016/06/10/brooklyn-javascript-became-heart-boroughs-tech-scene/).

<figure class="wide">
  <img src="/media/2016/10/20160121-brooklyn-js1475711720887.jpg">
</figure>

## Manhattan.js

![image](/media/2016/10/20160210-img_86741475712507734.jpg)

Manhattan.js features one passion talk in addition to the regular tech talks, and also has battle decks — random speakers come up and have to make up their talk on the spot, based on random slides that appear behind them.

![image](/media/2016/10/20160210-img_86791475712507774.jpg)

Each of these events also follows the [JSConf code of conduct](http://jsconf.com/codeofconduct.html), here is a quote from the quick version:

> JSConf is dedicated to providing a harassment-free conference experience for everyone, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion (or lack thereof), or technology choices. We do not tolerate harassment of conference participants in any form. Sexual language and imagery is not appropriate for any conference venue, including talks, workshops, parties, Twitter and other online media. Conference participants violating these rules may be sanctioned or expelled from the conference without a refund at the discretion of the conference organisers.

Organisers take this seriously and are patient enough to repeat the gist at the beginning of every meetup.

*Thanks Nick, Lena and Rick for the pictures of me talking. And thanks Jesse Ian Stein for the group photo!*