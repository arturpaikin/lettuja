---
title: "Eurotrip, The Movie: Part I"
description: "This is my movie about a two-month journey across Europe and Russia by hitchhiking, trains and cars. Part I: Paris, couchsurfing, French cheese talk, hitchhiking to Luxmbourg, exciting road stories, some more couchsurfing, then hitchhiking to Berlin."
cover: "http://arturpaikin.com/media/2017/10/BJCeyrg6b.jpg"
datePublished: 2017-10-14 9:53
published: true
---

My movie about a two-month summer journey across Europe and Russia by hitchhiking, trains and cars.

Part I: Paris, couchsurfing, French cheese talk, hitchhiking to Luxmbourg, exciting road stories, some more couchsurfing, hitchhiking to Berlin.

<figure>
<a href="https://www.youtube.com/watch?v=7CfdUFgLQog">
  <img src="/media/2017/10/BJCeyrg6b.jpg">
</a>
<figcaption><a href="https://youtu.be/7CfdUFgLQog">https://youtu.be/7CfdUFgLQog</a></figcaption>
</figure>

Subtitles available in English. Part II coming soon.

**Part I**
[Part II](/en/eurotrip-movie-part-two/)