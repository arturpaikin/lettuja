---
title: "Eurotrip, The Movie: Part II"
description: "My movie about the two-month journey across Europe continues. Part 2: Berlin, train to Zürich, boat ride and swimming in the lake, lookout tower, camping in the mountains, Moscow and some Saint-Petersburg"
cover: "http://arturpaikin.com/media/2017/12/ryK_irz-z.jpg"
datePublished: 2017-12-03 23:25
published: true
---

My movie about the two-month journey across Europe continues. 

Part 2: Berlin with friends, Transloadit team meetup, train to Zürich, boat ride and swimming in the lake, lookout tower, glazer and camping in the mountains, Moscow and some Saint-Petersburg.

<figure>
<a href="https://youtu.be/otF-BA5ZZ4Q">
  <div class="video-overlay">
    <img src="/media/2018/01/r1cOcWHVM.jpg">
  </div>
</a>
<figcaption><a href="https://youtu.be/otF-BA5ZZ4Q">https://youtu.be/otF-BA5ZZ4Q</a></figcaption>
</figure>

Subtitles available in English, don’t forget to turn them on. Thank you!

[Part I](/en/eurotrip-movie-part-one/)
**Part II**