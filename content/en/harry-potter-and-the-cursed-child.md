---
title: Harry Potter Play and a Meetup in New York
datePublished: 2019-03-18 21:44
cover: "http://arturpaikin.com/media/2019/03/B1k132owV.jpg"
description: "I’m a huge fan of Harry Potter since I was like 11, I grew up with the books. In school I even organized a Club of Harry Potter Fans, which later morphed into a book club “Magic”. Last year I went to see Harry Potter and the Cursed Child in New York."
published: true
---

![](/media/2019/03/B1k132owV.jpg)

I’m a huge fan of Harry Potter since I was like 11, I grew up with the books. In school I even organized a Club of Harry Potter Fans, which later morphed into a book club “Magic”.

Last year I went to see Harry Potter and the Cursed Child in New York. I’ve mentioned the book before on the blog, it left me with some mixed feelings. But the play puts everything in its place (which makes sense, because the book is just a published script for the play.

<!--more-->

Attention to details is astonishing, beginning with the entrance.

![](/media/2019/03/rk_563svV.jpg)

Lobby.

![](/media/2019/03/S1DQeTowV.jpg)

Cloakroom.

![](/media/2019/03/B1Uzlaiv4.jpg)

All of Lyric Theatre has been re-resigned for the play.

![](/media/2019/03/rJAEx6jPN.jpg)

![](/media/2019/03/rJJwepoPE.jpg)

There are two parts of the play, two hours each, which is a lot. There’s a break for about three hours — I went to work for a bit and had lunch. You can buy tickets just for one part, it’s best not to, but if you still do, pick the first one. I liked it better. Tickets were only $62, I got lucky, usually the are out of the cheap ones quickly.

![](/media/2019/03/Hy6OeTsw4.jpg)

<figure>
  <img src="/media/2019/03/BJAse6oD4.jpg">
  <figcaption>Pay attention to the phoenixes</figcaption>
</figure>

![](/media/2019/03/HyfibpsD4.jpg)

That carpet!

<img class="small-image" src="/media/2019/03/SJnobajD4.jpg">

Merchandise.

![](/media/2019/03/BJIgGTiPE.jpg)

Intersting detail and a light (very) spoiler — the merchindise was drastically changed for the second part.

![](/media/2019/03/SJ7mGaiwN.jpg)

![](/media/2019/03/S1ZDf6sP4.jpg)

After the first part you get the “keep the secret” badges, as to not ruin the fun for those yet to attend. So I’ll carefully state that I loved loved the effects — real magic on the stage, things happening  at the porter. And the cast is wonderful.

![](/media/2019/03/S1cxmasPN.jpg)

<img class="small-image" src="/media/2019/03/SyA47ajv4.jpg">

## The Group that Shall Not be Named

I was looking for some interesting meetups in New York to socialize, and stumbled upon [The Group that Shall Not be Named](https://www.meetup.com/TGTSNBN/). Next thing I know, I’m riding with them to the Hogwarts Halloween Field Trip to the Governors Island (which is highly recommended btw, 5 minutes on the ferry from Manhattan).

Все в костюмах, Лена мне тоже сшила Гриффиндорский.

<figure>
<img src="/media/2019/03/SJPg2psPE.jpg">
<figcaption>Hagrid is on the left, and the person next to me was cosplaying Princess Leia simultaniousely</figcaption>
</figure>

![](/media/2019/03/B1J4nTjPN.jpg)

I’ve come up with an image for Lena: she’s an elf with a S.P.E.W. badge — the Society for the Promotion of Elfish Welfare that Hermione ran.

<img src="/media/2019/03/ryev3ajD4.jpg">

<figure>
<img src="/media/2019/03/SkymppjwV.jpg">
<figcaption>Moaning Myrtle is in the center, and on the right — the group organizer, minister for magic</figcaption>
</figure>

Wizards resting.

![](/media/2019/03/ryaI6aiwN.jpg)

![](/media/2019/03/SkD90piD4.jpg)

![](/media/2019/03/HJ5uaasPV.jpg)
