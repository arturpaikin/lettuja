---
title: "Koti Home Automation And Mr. Rudolf, The Door Bell Bot"
cover: "http://arturpaikin.com/media/2016/10/kotihome.jpg"
datePublished: 2016-10-06 11:00
published: true
---

<figure>
  <img src="/media/2016/10/kotihome.jpg">
</figure>

Ever since my friends Anna and Sergey gave me an Arduino Starter Kit for my birthday, I’ve been experimenting with hardware: programming microcontrollers and connecting sensors to build a small home automation system. Prior to that I was a software-only person, with neither experience nor interest in hardware whatsoever.

I thought I’d try that kit maybe once, since I had it anyway. I connected a thermistor to the controller and saw my home temperature displayed in the console in Celsius, Fahrenheit and Kelvin! Seeing programs come to life, making things happen in real world and reacting to it got me excited for real. Turns out, those boring topics from school physics and math lessons have actual practical application. And I can be a part of it, with my own projects!

So here’s what I’ve been up to.

<!--more-->

---

## Koti Home Automation

A year of fiddling went by and I built myself a simple but extensible home automation system called Koti, which means “home” in Finnish.

On the picture above is the control panel: you can turn the lamp (or any electronic device) on and off, monitor temperature and light level in the house, see how long it’s been since someone last moved, and activate voice commands in the browser.

It’s all open source, [check out the code](https://github.com/arturi/kotihome). Built on top of Arduino Uno and Raspberry Pi, with Node.js on client and server. Control panel is built in React, to be cool and all. Plus there is a Telegram chat bot as an optional way for talking to the house on the go, cause that’s what I do now.

![image](/media/2016/10/koti-telegram1475685086660.jpg)

Here is how I see my home automation setup:

<figure class="bordered">
  <img src="/media/2016/10/kotihome-arch1475683648618.jpg">
</figure>

Here is what it actually looks like:

<figure>
  <img alt="The House Is Not On Fire" src="/media/2016/10/thehouseisnotonfire-51475634069536.jpg">
  <figcaption>The hardware lives in a cardboard box in Moscow and occasionally scares my subletters</figcaption>
</figure>

It’s not crazy advanced or anything, but I am a little proud. Had a lot of fun building it, and learned a ton too.

![image](/media/2016/10/thehouseisnotonfire-41475684545915.jpg)

![image](/media/2016/10/thehouseisnotonfire-31475639461056.jpg)

Many people are doing similar and more creative projects, my favorite is Leonid Kaganov’s home server, with fingerprint scanner to open the door, and speakers that shame prankers who ring the door bell for fun: [part 1](https://lleo.me/dnevnik/2010/04/27.html) and [part 2](http://lleo.me/dnevnik/2014/07/07.html) (in Russian, use Google Translate, there are pictures too). And there is an open source [Heimcontrol.js](https://ni-c.github.io/heimcontrol.js/) and [SidLee Dashboard](http://dashboard.sidlee.com), to name a few.

I gave a detailed talk about my project at an awesome New York JavaScript meetup, [Queens.js](http://www.meetup.com/QueensJS/). Check out the [slides](http://bit.ly/notonfire) and watch the [video](https://youtu.be/Z-NpPj6aKJE).

<figure>
  <a href="https://youtu.be/Z-NpPj6aKJE">
    <img alt="The House Is Not On Fire" src="/media/2016/10/thehouseisnotonfire-2.jpg">
  </a>
  <figcaption>Originally titled “Divide and conquer your house”</figcaption>
</figure>

## Mr. Rudolf

In winter of 2016 I moved to New York. 

![image](/media/2016/10/img_99491475695633866.jpg)

And I wanted to continue with my experimental projects on the go. But it made no sense to re-create the whole home automation experience at a temporary sublet, so I looked for something more portable. Around that time I discovered [ESP8266](https://www.sparkfun.com/products/13678) and, based on it, [NodeMCU board for $3 on Aliexpress](https://aliexpress.com/item/New-Wireless-module-CH340-NodeMcu-V3-Lua-WIFI-Internet-of-Things-development-board-based-ESP8266/32683073762.html). Also known as the more robust [Adafruit Feather Huzzah](https://www.adafruit.com/product/2821). It’s a small Arduino-compatable microcontroller with Wi-Fi and an ability to deep-sleep (thus consuming very little battery) and wake up either by timer or hardware interrupt, such as a button.

![image](/media/2016/10/esp82661475696948714.jpg)

So I began exploring and asking myself how could I put this awesome tech to test. At that time we didn’t have American sim-cards and the intercom in our building was broken, so we couldn’t get packages delivered. Which I always believed to be so easy and “all the cool kids in America are doing it”. Here is how it went for us: USPS would just leave a note at the door saying “we couldn’t deliver, get the package at your local post office”, which is all right. But UPS would drive around with my package for a few days, and then leave a note saying “well, we couldn’t deliver, so know you have to get it yourself at our location somewhere on the South Pole. Good luck!”. 

This madness had to stop. I went to [Tinkersphere](http://tinkersphere.com/), which is basically an Aliexpress website come to life, and bought a hot yellow button, a battery case and some wires. The result:

<figure>
  <img src="/media/2016/10/20160317-img_89141475695801913.jpg">
  <figcaption>On the left is my neighbourhood in Ridgewood, on the right is a cool macho picture, and a note from UPS saying “we couldn’t deliver your package”, and in the middle is the broken intercom and the button I added to it</figcaption>
</figure>

![image](/media/2016/10/20160317-img_89251475695801919.jpg)

So it’s a simple button, connected to ESP8266 that sleeps until pressed. Then it wakes up, connects to a Wi-Fi network (I borrowed the password from local barber shop), and sends a request to my VPS server, which in turn sends me a message via [Telegram Bot API](https://core.telegram.org/bots/).

<figure class="small-image">
  <img src="/media/2016/10/20160316-img_89111475696587770.jpg">
</figure>

All of the code I used for the server and ESP8266 is also [open source](https://github.com/arturi/mrrudolf).

And I gave a light and fun 10-minute talk about this project — after which I’m known as developer-comedian — at my favorite JS meetup in the universe, [Brooklyn JS](http://brooklynjs.com), with real cool demo of the button in action. Check out the [video](https://youtu.be/CvorlYi4L_c) and [slides](http://bit.ly/mrrudolf).

<a href="https://youtu.be/CvorlYi4L_c">
  <img src="/media/2016/10/robot-talks-1.jpg">
</a>
