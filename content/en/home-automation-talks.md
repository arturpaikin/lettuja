---
title: "Talks at Brooklyn.js and Queens.js"
datePublished: 2016-03-18 11:30
published: true
---

![](/media/2017/12/SJ6_ZK2zz.jpg)

Gave a fun 10-minute talk yesterday at Brooklyn.js about how I’ve fixed a broken intercom in my building by making a chat bot.

Video: [youtu.be/CvorlYi4L_c](https://youtu.be/CvorlYi4L_c)
Slides: [bit.ly/mrrudolf](http://bit.ly/mrrudolf)

![](/media/2016/10/robot-talks-1.jpg)

![](/media/2017/12/SJ66btnzz.jpg)

***

A few months prior I gave my first tech talk ever at Queens.js, always wanted to do that. It was about my home automation experiments. Couldn’t imagine a friendlier community.

Slides: [speakerdeck.com/arturi/koti-home-automation-robot](https://speakerdeck.com/arturi/koti-home-automation-robot)
Video: [youtu.be/Z-NpPj6aKJE](https://youtu.be/Z-NpPj6aKJE)

![](/media/2016/10/thehouseisnotonfire-2.jpg)

<img class="small-image" src="/media/2018/12/Hy_KV-BWV.jpg" alt="What do you do to get package delivered in NYC when your intercom is broken? @arturi built a robot for it 🙌">

<img class="small-image" src="/media/2018/12/HkZgHZSbV.jpg">