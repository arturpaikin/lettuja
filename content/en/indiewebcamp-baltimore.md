---
title: "Me and Indie Web Camp Baltimore"
description: "Personal indie web history and what I did this weekend"
datePublished: 2018-01-24 11:48
cover: "http://arturpaikin.com/media/2018/01/S1YS7KBHf.jpg"
published: true
---

<figure>
  <img src="/media/2018/01/ryz1kYHBM.jpg">
  <figcaption>Group photos — Aaron Parecki</figcaption>
</figure>

This weekend I attended [Indie Web Camp](https://indieweb.org/2018/Baltimore) in Baltimore. It’s a fun gathering of people who believe we should own our online identities — pictures, thoughts, short and long posts (and even check-ins in some cases). Post to your own domain, then syndicate to social networks, but remain in control. Core principles of the movement are listed on [indieweb.org](https://indieweb.org/).

A breif personal indie web history. I’ve been blogging and building websites and CMSes for as long as I can remember. When I was 12 living between Kursk, Russia and Imatra, Finland, I published an online zine called “Superfun”. I had some strong web design and writing skills:

<!--more-->

<figure>
<div class="row collage">
  <div class="column-1-2 height-l">
    <img src="/media/2018/01/ryKV0drHf.jpg">
  </div>
  <div class="column-1-2 height-l">
    <img src="/media/2018/01/BkpE0uHHG.jpg">
  </div>
</div>
</figure>

After the zine I was blogging in Russian, English and Finnish on my domain, switched to LiveJournal for a couple years:

<img class="bordered" src="/media/2018/01/S1_a0KBSz.png">

then coded a static site generator — first one in PHP, later replaced by a newer version in Node.js — and went back to publishing on my domain.

A good chunk of my work at [Baguette studio](http://unebaguette.com) was also dedicated to setting up, designing and coding independent blogs and websites for people and small businesses.

Naturally, I enjoyed the weekend meeting like-minded people and spending some quality time sharing ideas and working on my site and admin interface Tent ([2 min video](https://youtu.be/748UfVE28Ns?t=13m) of me presenting it). I learned more about microformats (last night I had a dream about adding microformats somewhere, not kidding), micropub, webmentions and other cool indie web technologies. More info can be found on the [camp’s wiki page](https://indieweb.org/2018/Baltimore) (and the [wiki itself](https://indieweb.org/principles) is a great resource on everything indie web).

<figure>
  <img class="bordered" src="/media/2018/01/r16WaFHHz.jpg">
  <figcaption>Tent interface</figcaption>
</figure>

Big thanks to [Marty](https://martymcgui.re), [Jonathan](https://jonathanprozzi.net/), [Amy](https://amyhurst.com/), [Aaron](https://aaronparecki.com), [Gregor](https://gregorlove.com) and others for all the fun!

![](/media/2018/01/S1YS7KBHf.jpg)

![](/media/2018/01/Hyv1DtSBG.jpg)

<figure>
  <img src="/media/2018/01/ryK0MtBBz.jpg">
  <figcaption>Slightly indieweb-themed improv comedy</figcaption>
</figure>

I didn’t get to spend much time exploring the city, but I got the important parts.

![](/media/2018/01/HkhK8FSBM.jpg)

Quality coffee at Ceremony:

![](/media/2018/01/HkZ28tSrG.jpg)

![](/media/2018/01/H19UXYSBM.jpg)