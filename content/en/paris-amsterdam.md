---
title: "Paris and Amsterdam"
datePublished: 2014-12-18 11:10
cover: "http://arturpaikin.com/media/paris-amsterdam/img_52591418847525968.jpg"
published: true
---

<figure class="wide">
  <img src="/media/paris-amsterdam/img_52591418847525968.jpg">
</figure>

For a few years I was convinced Paris is the best city in the world and nothing could ever compare. I would travel to other places, most of which I enjoyed and had fun at, but deep down I knew that Paris is the place to be. Its streets, parks, boulangeries, among with baguettes and croissants in the mornings, wine and cheese in the evenings with friends or alone, overlooking La Seine, made me come back at least once a year, sometimes twice.

From the very first Eurotrip I also really liked the Netherlands and Amsterdam — bicycles, canals and amazing architecture.

In fact, I loved Paris and Amsterdam so much, I even set my home Wi-Fi network name to “Paris” and used “Amsterdam” as a password. This is some serious stuff. Then I took a break for some time, during which I discovered other awesome places like New York (especially New York), Copenhagen and Berlin, but this summer I decided to return. Turns out, even though I’ve visited Paris half a dozen times, there still is a side of the city I completely missed out on. Same is true for Amsterdam, but let me start from the beginning.

<!--more-->

## Paris

Our Airbnb apartment was in a quite Parisian area, a few minutes from Arc de Triomphe. The area turned out to be rather dull, but we had an awesome view from the window and enjoyed breakfasts with juice, baguette and croissants, among with cheese and some wine in the evenings.

![image](/media/paris-amsterdam/img_51001416832270931.jpg)

![image](/media/paris-amsterdam/img_15831416832752215.jpg)

The Eiffel tower looks gorgeous day and night.

![image](/media/paris-amsterdam/img_51071416832886251.jpg)

“French picnic guy”, level 22.

![image](/media/paris-amsterdam/img_51111416832963216.jpg)

### Velib

Of course, I couldn’t not get a bike. Velib pass for a week for 7 euro — done. It was not the first time I tried it, but it was the first time I actually used it almost every day. I must say it’s pretty convenient, though sometimes I found it hard to locate an empty spot on the parking station, when it came time to return the bike. [An iPhone app](http://en.velib.paris.fr/Velib-on-smartphone) does help a lot, but naturally it won’t show you the number of available spots in offline mode, in which I was, because I don’t like to get a new SIM card if I’m in town only for a couple days — so not Velib’s fault.

![image](/media/paris-amsterdam/img_16841416833202975.jpg)

And traffic in Paris is rough. Great, if you compare it with Moscow, but a lot of work to be done if we are talking Amsterdam or Copenhagen. There are many bike lanes, but not on most streets, and I felt tense on the road all the time. Maybe it just takes some getting used to.

### Canal Saint-Martin

As I mentioned earlier, turns out I missed out a lot on my past trips. One of the places that has become hip is Canal Saint-Martin.

![image](/media/paris-amsterdam/img_51181412867243225.jpg)

<figure>
  <img src="/media/paris-amsterdam/img_51521412867986192.jpg">
  <figcaption>The drawing of bridges, Paris style. <a href="http://en.wikipedia.org/wiki/List_of_bridges_in_Saint_Petersburg">Hello, St. Petersburg</a>.</figcaption>
</figure>

The Canal is surrounded by small coffee shops, sandwich bars and authentic places like this boulangerie.

![image](/media/paris-amsterdam/img_51151416833648510.jpg)

Or this [Cowork Shop](http://www.coworkshop.fr/en/home-en/).

![image](/media/paris-amsterdam/img_15951416833690538.jpg)

People on the streets of Paris seem relaxed in general, nobody is rushing anywhere. Look at this place, [Holly Belly](http://holybel.ly/).

![image](/media/paris-amsterdam/img_15971416833973134.jpg)

![image](/media/paris-amsterdam/img_51511416833890618.jpg)

Short version: sorry, we worked hard and now we are taking a summer break, sipping beer somewhere in Mexico. Please don’t hate us, come back in September. *(By the way, if you are in Paris, do pay Holly Belly a visit.)*

It just seems very unusual to me, this kind of thing could never happen in Moscow where rent is higher than the Empire State Building. People do take shifts and go on vacations, but not like, you know, the whole crew at once. Don’t get me wrong, though — I love it. Socialism, people come first. And this is not the first time I’ve encountered somebody closing their place for the summer in Paris.

At the local bookstore I stumbled upon a book that briefly describes my life:

*![image](/media/paris-amsterdam/img_51201416834049790.jpg)*

It’s pretty great, actually, compares two cities using gorgeous minimalist style illustrations. [Check it out](http://society6.com/artist/parisvsnyc).

### Specialty Coffee

Paris’s got some cool coffee spots too. Like this one, [Coutume](http://www.yelp.com/biz/coutume-caf%C3%A9-paris-2).

![image](/media/paris-amsterdam/img_51301412869231857.jpg)

And the next one is my favourite, [Cafe Loustic](http://www.goodcoffeeinparis.com/2013/09/cafe-loustic.html).

![image](/media/paris-amsterdam/img_51491416835625154.jpg)

The owner is a great guy, it was a pleasure to have a chat with him.

![image](/media/paris-amsterdam/img_51481416835180199.jpg)

People watching is a great source of inspiration in here.

![image](/media/paris-amsterdam/img_51661412870306506.jpg)

And, if you are still not convinced , here is another one — [Ten Belles](http://www.yelp.com/biz/ten-belles-paris).

![image](/media/paris-amsterdam/img_51121412871513220.jpg)

Chewbacca and C-3P0 send their regards.

![image](/media/paris-amsterdam/img_51351416835674948.jpg)

An open fountain where parents leave their children, who then go nuts in the water — they slide, skate and jump and enjoy themselves tremendously, while their parents are enjoying a cup of coffee at a cafe on the corner. For a brief moment I felt an urge to jump in and join them.

![image](/media/paris-amsterdam/img_51581416835785983.jpg)

[Anti Cafe](http://anticafe.eu/en), a local version of Moscow’s [Ziferblat](http://pushkin.ziferblat.net/en/) (Clockface), where you pay for time, while snacks, coffee, tea, cereal and lemonade are free. And there is Wi-Fi.

![image](/media/paris-amsterdam/img_51361412871700999.jpg)

I pulled out my MacBook and had a few productive hours.

![image](/media/paris-amsterdam/img_51371416835927993.jpg)

The final coffee spot for today — [Telescope](http://www.yelp.com/biz/telescope-paris). Barista know their craft in here.

![image](/media/paris-amsterdam/img_52131416838275554.jpg)

### Parks, streets and La Seine

The streets of Paris are narrow, a lot of them have been built in medieval ages.

![image](/media/paris-amsterdam/img_16211416835020621.jpg)

![image](/media/paris-amsterdam/img_52051416838153260.jpg)

![image](/media/paris-amsterdam/img_17161416835996034.jpg)

In Summer the embankment along La Seine turns into a small beach.

![image](/media/paris-amsterdam/img_16661416836281315.jpg)

<figure>
  <img src="/media/paris-amsterdam/img_16381416836359718.jpg">
  <figcaption>This and the previous photo was taken by <a href="http://elenazaharova.com">Elena Zaharova</a>. We share travel photos every now and then, but I wanted to take a moment and credit Elena for these two.</figcaption>
</figure>

![image](/media/paris-amsterdam/img_51621416836081655.jpg)

And the parks are exactly what you’d expect — a great place to relax and throw a picnic.

![image](/media/paris-amsterdam/img_51341416836207645.jpg)

![image](/media/paris-amsterdam/img_51331416836233879.jpg)

Parks are everywhere, nice and friendly. Love it. The famous [Jardin du Luxembourg](http://en.wikipedia.org/wiki/Jardin_du_Luxembourg):

![image](/media/paris-amsterdam/img_16711416836623790.jpg)

Montmartre, the view from Sacré-Cœur.

![image](/media/paris-amsterdam/img_17431416836764480.jpg)

### 	Coulée verte

The Promenade plantée or the [Coulée verte](http://en.wikipedia.org/wiki/Promenade_plant%C3%A9e) is a Highline Park in Paris (please don’t email me), but a lot more French — cozy and cute, with a lot of flowers everywhere.

![image](/media/paris-amsterdam/img_51821416837027467.jpg)

![image](/media/paris-amsterdam/img_51711416837066422.jpg)

Must visit. Pro tip: grab some fresh fruits and pastries from the local boulangerie on your way here.

![image](/media/paris-amsterdam/img_51771416837160071.jpg)

![image](/media/paris-amsterdam/img_51811416837187993.jpg)

### There and back again

My birthday was coming up, and I decided that the best way to celebrate would be somewhere underground, where no man has gone before (sort of).

![image](/media/paris-amsterdam/img_51891416837524015.jpg)

So we took some flashlights and the Friendly Leader, who knew the secret, and submersed to the underside of Paris.

![image](/media/paris-amsterdam/img_18101416837551354.jpg)

Our journey was strenuous — we waded an underground river and had to crouch a lot. But in the end, we arrived at the hidden place and were rewarded by some pancakes, beer and wine. Everybody sang “bon anniversaire” to me, which was awesome. *Thanks, the friendly leader and another friend for making this mystery trip possible.*

![image](/media/paris-amsterdam/photo-21416837700837.jpg)

## Amsterdam

My first impression of Amsterdam is discrimination. Pedestrians are discriminated by cyclists ([bicycle rights!](http://www.youtube.com/watch?v=V3nMnr8ZirI)).

![image](/media/paris-amsterdam/img_52171416838999908.jpg)

Seriously, bikes and bike lanes are everywhere. It’s great.

Also, no cash — only credit cards. Now that’s my type of country.

<figure>
  <img src="/media/paris-amsterdam/no-cash1416839981967.jpg">
  <figcaption>Well, at least in some places</figcaption>
</figure>

Olive oil is sold in cans.

![image](/media/paris-amsterdam/img_52181416840105616.jpg)

I was tired, so after a quick breakfast I went to a park with a scary name — Sloterpark.

![image](/media/paris-amsterdam/img_52241416840155390.jpg)

<figure>
  <img src="/media/paris-amsterdam/img_52201416840423571.jpg">
  <figcaption>On this picture, believe it or not: a guy is spreading the picnic blanket, his dog attacks a goose. Goose escapes and jumps into the river. All in one brief moment.</figcaption>
</figure>

If you didn’t bring your own blanket, the park provides you with a disposable one. After you are finished — just wrap it up and take your trash out in it. Ingenious.

![image](/media/paris-amsterdam/img_52251416840709678.jpg)

After an hour-long nap (during which the police woke me up and advised not to sleep here, since someone might steal my stuff), it was time for a walk.

<figure>
  <img src="/media/paris-amsterdam/img_52271416841040831.jpg">
  <figcaption>Notice how the sidewalk is on the same level with the road. Barrier-free environment.</figcaption>
</figure>

Mini-rooftop party.

![image](/media/paris-amsterdam/img_52291416841128812.jpg)

And a [boat party](http://www.youtube.com/watch?v=R7yfISlGLNU). It’s actually a thing here: dinner on a boat or right outside the house — people bring out their tables and enjoy the weather.

![image](/media/paris-amsterdam/img_52301416841181186.jpg)

### Almere

No one from Couchsurfing could host me in Amsterdam this time, so I booked a nice room under the roof in Almere, a city located 20 minutes from Amsterdam by train. Why not in Amsterdam itself? Because ~~I saw two wonderful cats who lived in the house and could’t resist~~ it was cheaper and I wanted to feel real Dutch, commuting to Amsterdam from suburbs in the morning and back in the evening.

Almere is [the newest city in the Netherlands](http://en.wikipedia.org/wiki/Almere) and it’s a paradise: I could hardly spot any cars there. In fact the main road from the train station to our house is a wide bike lane.

![image](/media/paris-amsterdam/img_52331418844696796.jpg)

![image](/media/paris-amsterdam/img_52341418819737806.jpg)

Bought a pack of popsicles for 1 euro. Hard to find these in Russia for some reason (I blame bears).

![image](/media/paris-amsterdam/img_52361418820079843.jpg)

### Brouwerij 't IJ

Lena found a craft beer guide for Amsterdam and [Brouwerij 't IJ ](http://en.wikipedia.org/wiki/Brouwerij_'t_IJ)was at the top of the list, so we hoped on our bikes and went to check it out.

<figure>
  <img src="/media/paris-amsterdam/img_52411418846976535.jpg">
  <figcaption>Bike parking is tough in Amsterdam, especially by the hip craft beer place</figcaption>
</figure>

Beer is reasonably priced and served in small glasses, so you can easily try a few.

![image](/media/paris-amsterdam/img_52471418847219289.jpg)

![image](/media/paris-amsterdam/img_52551418847312546.jpg)

People around are friendly and easy-going. Sharing a table allows for easy conversations — we’ve made friends in no time.

![image](/media/paris-amsterdam/img_52561418847423568.jpg)

![image](/media/paris-amsterdam/img_52541418847345783.jpg)

So yeah, I’d say it’s a must in Amsterdam.

![image](/media/paris-amsterdam/img_52621418847543982.jpg)

### Creative Mornings

I am a frequent guest at [Creative Mornings](http://creativemornings.com) in Moscow, so why not check if there is one in Amsterdam. Sure enough there was one coming, so I had to wake up real early (Almere, remember?). My bike was parked at the Amsterdam Central railway station’s bicycle parking — free and convenient, especially when it costs around 6 euro to take the bike to Almere one way.

Creative Mornings was held in the Amsterdam’s coolest neighbourhood, [	Amsterdam-Noord](http://www.ilovenoord.com/), in  a co-working space called A Lab. To get there, you hop on a ferry (with your bike, naturally) and arrive in a few minutes.

![image](/media/paris-amsterdam/img_52651418848142854.jpg)

Feel free to enjoy a banana on the way.

![image](/media/paris-amsterdam/img_52661418848174027.jpg)

For breakfast there was coffee from The Coffee Virus, orange juice and croissants.

![image](/media/paris-amsterdam/img_52861418848270317.jpg)

And this is where the speaker, Radna Ramping, [gave her talk](http://creativemornings.com/talks/radna-rumping) about “the tension and possibilities of contemporary culture in a city with so much cultural heritage, Amsterdam”.

![image](/media/paris-amsterdam/img_52721418848309311.jpg)

### Amsterdam-Noord

The neighbourhood has a lot to explore, which is best done on your bike, of course.

How about an insanely cool vintage store, Waterloopleinmarkt?

![image](/media/paris-amsterdam/img_53011418848691003.jpg)

![image](/media/paris-amsterdam/img_52981418848655504.jpg)

A table for a 100 euro?

![image](/media/paris-amsterdam/img_53051418848799946.jpg)

![image](/media/paris-amsterdam/img_53151418848855336.jpg)

![image](/media/paris-amsterdam/img_53171418848874194.jpg)

There are nice cafes here too, this one reminded me of my favourite Fruits and Veggies in Moscow.

![image](/media/paris-amsterdam/img_53241418848916550.jpg)

![image](/media/paris-amsterdam/img_53211418848969504.jpg)

### Adventures in the night

![image](/media/paris-amsterdam/img_53271418849179687.jpg)

This particular photo brings back memories of how we’ve spent one night in Amsterdam.

We are sitting on a train, which is about to depart towards Almere. But they announce something in Dutch, and people tell us that somebody probably killed himself by jumping on the train track. So all the trains are canceled until the police investigates the matter.

At the same time, our Airbnb host from Almere takes a sit right in front of us — what a coincidence. And we are waiting together. Then we got out of the train and she starts calling all her friends to see if anybody can pick us up. I suggest riding a bike 30 kilometres, since, you know, we are in Netherlands. But everybody says it’s too dark and the lanes are not lit.

It’s Friday night, so all the friends are drunk. I went behind the railway station and started hitchhiking. No luck — nobody drives is Amsterdam, there are mostly taxis.

About 3 hours later they arranged a bus that finally took us to Almere.

### Sweet Cup

The next day was my last, so I went to check out local coffee places. This one is really good, [Sweet Cup](http://www.sweetcupcafe.com).

![image](/media/paris-amsterdam/img_53421418849402075.jpg)

It’s a small family place, they’ve got a cute dog and a roaster. The owners are very friendly, when they found out I know coffee, they treated me with some of their experimental cold brew.

![image](/media/paris-amsterdam/img_53341418849539758.jpg)

Corner of shame.

![image](/media/paris-amsterdam/img_53411418849780328.jpg)

### Airport Schiphol

Schiphol is hands down the best airport I’ve been to. It is nicely designed, with a lot of pleasant small details. Like this particular one — charge your phone by exercising:

![image](/media/paris-amsterdam/img_53461418849899932.jpg)

And a nice inner yard where you can relax, while waiting for your flight. By the way, the doors that lead into the yard produce energy when you push them.

![image](/media/paris-amsterdam/img_53511418849937475.jpg)

At Schiphol, you feel like a superman.

![image](/media/paris-amsterdam/img_53531418850053529.jpg)

![image](/media/paris-amsterdam/img_53501418850045712.jpg)

*As a bonus, watch this [KLM commercial about a dog](http://www.youtube.com/watch?v=PkBxPJuuJOU) who works at this airport and helps find people who’ve left their stuff behind on a plane (I’ve been told it’s not a true story, but the video is still good).*

##### Further reading
* [Paris and Amsterdam](http://blog.elenazaharova.com/post/paris-amsterdam) by Elena Zaharova (in Russian)
* [12 hours in Paris](http://www.12hrs.net/guides/12-hrs-in-paris)
* [Paris neighbourhood guide](http://trottermag.com/paris/neighborhood-guide)
* [10 secret places in Paris](http://www.the-village.ru/village/weekend/secretmap/172891-10-sekretnyh-mest-parizha) (in Russian)
* [Get Cultured: Amsterdam](http://www.thecultureist.com/2013/09/04/what-to-do-in-amsterdam-itinerary/)
