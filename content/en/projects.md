---
title: "Projeсts"
description: "Things I’ve done, organised, built or participated in"
datePublished: 2050-07-07 07:07
cover: "http://arturpaikin.com/assets/artur-bushwick.jpg"
type: page
published: true
---

Some things I’ve done, organised, built or participated in:

* Gave a [talk about Uppy](https://youtu.be/HTx8JH7j1G4) at Manhattan.js.
* Made a movie about traveling in Europe via hitchhiking and otherwise: [Part I](https://youtu.be/7CfdUFgLQog) and [Part II](https://youtu.be/otF-BA5ZZ4Q).
* Been working on [Uppy](https://uppy.io), the modular file uploader.
* Written a [thorough review of Strida folding bicycle](http://arturpaikin.com/ru/strida/) that I ride.
* Fun [10-minute Brooklyn.js talk](https://www.youtube.com/watch?v=CvorlYi4L_c) about how I’ve fixed a broken intercom in my building by making a chat bot.
* Gave a [talk at Queens.js](https://www.youtube.com/watch?v=Z-NpPj6aKJE) on Koti Home Automation.
* Organized [Web Development School](http://unebaguette.com/web-course/) — teaching HTML, CSS, JavaScript and some web design for beginners.
* [Koti Home](https://github.com/arturi/kotihome) automation system: uses Arduino + Raspberry Pi + Sensors + Node.js.
* [Lettuja](https://github.com/arturi/lettuja) static site generator, written in Node, built for speed, multilingual/environmental websites and fun. Powers my site.
* [Labelmaker](https://github.com/arturi/labelmaker) image annotation plugin.
* [Tipi Coffee](https://fb.com/thetipicoffee) pop-up fussy coffee & pastry cafe that I ran for a few years with my friend Lena.
* [One Month Across the USA](https://www.youtube.com/watch?v=Mm1gwVTi7SM) — 40-minute movie on traveling from New York to Portland, Oregon and everything in between.
* [Baguette](http://unebaguette.com) technology cooperative I started in 2013. Worked on personal blogs (support indie web), online stores, websites for conferences and more.
* [Travel stories](https://www.facebook.com/events/782856498477414/) meetups where friends gather to hear some exciting stories on recent travels & adventures around the world, drink beer or lemonade and have fun times.
* [Lectures on independent traveling](https://vk.com/arturpaikinvoyage).

## Archive

* Taught Finnish language for beginners at Gymnasium 5 in Belgorod.
* Written columns for “Big Break” and “Arguments and Facts” about Finish school, hitchhiking and the new iPhone.
* Organized Harry Potter fan club at Gymnasium 44 in Kursk. Later turned into a “Fantasy Books Fan Club”.