---
title: Subscribe
datePublished: 2019-02-24 13:57
published: false
type: page
---

Subscribe to my posts and notes:

* [RSS](/en/blog-feed.xml) — the best way. It was made specificaly for following websites. Choose any RSS service you like, [Feedly](https://feedly.com), for example, get an app for your computer and smartphone ([Reeder](http://reederapp.com/), for instance), [add my blog](http://feedly.com/i/subscription/feed/http://arturpaikin.com/ru/blog-feed.xml) — that’s it, your personal digest of interesting posts and articles, without ads, tracking, weird algorithms and other junk.
* [Twitter](https://twitter.com/arturi)
* [Instagram](https://instagram.com/arturi)
* [Facebook](https://fb.me/arturpaikin)
