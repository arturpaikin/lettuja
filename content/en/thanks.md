---
title: Thanks
datePublished: 2015-03-06 13:57
type: page
---

If you liked some of my work, articles, pictures or whatever, you can say thanks by sending me a little money for coffee, I’ll be grateful:

* [PayPal](https://paypal.me/iamarturpaykin);
* Yandex.Money: 41001898195842. [Сard payment or Yandex.Money transfer](https://money.yandex.ru/embed/shop.xml?account=41001898195842&quickpay=shop&payment-type-choice=on&writer=seller&targets=%D0%91%D0%BB%D0%B0%D0%B3%D0%BE%D0%B4%D0%B0%D1%80%D0%BD%D0%BE%D1%81%D1%82%D1%8C&targets-hint=&default-sum=100&button-text=01&successURL=).

![image](/media/2015/05/img_66851432231289707.jpg)
