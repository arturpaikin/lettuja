---
title: "Received a letter from Hogwarts 🇬🇧"
description: "About the Global Talent visa, British bureaucracy and a word on life in London"
cover: "http://arturpaikin.com/media/2020/03/H1pPlI-B8.jpg"
datePublished: 2020-03-20 12:25:20
published: false
---

<img src="/media/2020/03/H1pPlI-B8.jpg">

I received a letter in the mail in February. The letter contained a Residence Permit card with a British five-year visa. So I packed up my backpack, folded up my Strida and came to London.

<!--more-->

## About the visa

### What’s up

In August 2019 I was sitting in Moscow, in a Skuratov coffee shop on Paveletskaya. That's where I usually work from. That day somehow it wasn't working, and I thought: it's been a long time since I've gotten a strange visa. Then I opened the website [Tech Nation] (https://technation.io/visa/) and started reading the conditions for the visa "[Global Talent] (https://www.gov.uk/global-talent)". Everything came out that if I had faith in myself (that's not the case), nerve and perseverance (at least add up) - I may well succeed.

*I was actually inspired to get this visa by my friend Kirill, nothing would have worked without him. Kirill, thank you! And to everyone who helped with the letters, and morally. And of course, to the British government and Tech Nation for this opportunity, no kidding, I'm very grateful. And I've been thinking about the idea for a year and a half, monitoring the Brexit situation, “how we moved to London, but then returned” videos and all that.*

In a month and a half, I collected 10 pages of A4 in PDF (and that's the limit in Britain, [ahemhem, O-1] (http://arturpaikin.com/media/2017/08/H1v8t__O-.jpg)): [speeches] (😂) at meetings and conferences, mentoring (my web development courses, participation in events like [Global Diversity Day] (https://www.globaldiversitycfpday.com/)), [opensource](https://github.com/arturi/), letters of recommendation and all kinds of [interviews](http://arturpaikin.com/en/home-automation-talks/). Paid the fee, waited. It was, of course, stressful. But three weeks later the confirmation came - come, we will be glad to see 🎉.

### Why is the Global Talent visa good

Pretty much in every way:

* It is issued for 5 years, after 3-5 years you can get a permanent residence permit.
* You can work anywhere and in any way you want, just as long as in the area on which the visa.
* Possible to get without lawyers, mortgages and paid text messages - I was able to make everything myself, cost me a visa of about $1,000 (the rest - contributions to medicine NHS, 400 pounds per year, multiply by 5 years). By comparison, an American visa cost $8,000 for three years, and I wouldn't risk it without a lawyer.
* A partner can work full time wherever he wants (America and USCIS, I love you!).
* In Britain, the NHS is public medicine on a pretty good level (many people scold it, but nobody [is afraid to call an ambulance] (https://www.mcsweeneys.net/articles/welcome-to-our-modern-hospital-where-if-you-want-to-know-a-price-you-can-go-fuck-yourself) if necessary). And so, in the latest news: "Last March, announced that Mr. Castillejo, then identified only as the "London Patient," [had been cured of H.I.V. after receiving a bone-marrow transplant for his lymphoma] (https://www.nytimes.com/2020/03/09/health/hiv-aids-london-patient-castillejo.html?action=click&auth=login-email&login=email&module=Top%20Stories&pgtype=Homepage)".
* On entry, the border guard congratulated me on my visa 💃
* Brexit or Mexit - everyone’s still eating avocado sandwiches so far.
* You can't be an athlete and a dentist - that's where it hurt.

If you're interested in more details, please read these guides:

* [Tech Nation Visa Guide] (https://technation.io/visa-tech-nation-visa-guide/) - specifically for tech/IT.
* [Guidance on applications under Global Talent ](https://www.gov.uk/government/publications/guidance-on-applications-under-global-talent) - common to all areas: Art, Fashion, Science, Engineering, History - it’s not just tech ;-)

*There is a nuance. You are forced to decide for yourself whether you are still "Exceptional Promise" (i.e. give hope), or already "Exceptional Talent" (no hope). And the criteria are similar, to confuse you. And the difference is small: in the first case on a residence permit can be filed in 3 years, in the second - in 5, the details of life. Nevertheless, a lot of good people on the choice of this is easy. Stay calm, save and exhale: you can apply for Talent, and, if anything, they themselves will downgrade you to `Promise` (`catch` will work). I’m not sure, but it looks like it. If there are a few "proofs" coming together, and those are real-life projects, then I think you can apply for Talent. This is not legal advice, but a private opinion of a man on a triangular bike, who sometimes sleeps in a car in the Walmart parking lot.*

### Why don't you just get a job at Facebook or Booking, there’s relocation, stock options, visas, they [help move your cat](https://overreacted.io/my-decade-in-review/#2015)?

🤔

### What’s the plan?

I’d like to become a [solar-punk] (https://usesthis.com/interviews/devine.lu.linvega/) pirate.

## About bureaucracy

I have very little experience so far (two weeks, ha-ha), but some impressions are already there. I am still very happy with the way the bureaucracy works in Britain. I'll update my post over time.

### Residence Permit

Biometric Residence Permit card
