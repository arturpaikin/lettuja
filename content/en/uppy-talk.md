---
title: "Uppy or Building a File Uploader That Won’t Bark at the Mailman — my talk at Manhattan.js"
description: "My talk about Uppy, the file uploader I’ve been working on. It’s got sleek UI, fetches files from local disk, as well as remote Google Drives and Instagrams, or you can snap selfies with a camera, preview and edit metadata, and upload files to the final destination, optionally processing/encoding on the way."
cover: "http://arturpaikin.com/media/2018/01/S1chNGrEM.jpg"
datePublished: 2018-01-11 12:06
published: true
---

<figure>
<a href="https://youtu.be/HTx8JH7j1G4">
  <div class="video-overlay">
    <img src="/media/2018/01/S1chNGrEM.jpg">
  </div>
</a>
<figcaption><a href="https://youtu.be/HTx8JH7j1G4">youtu.be/HTx8JH7j1G4</a></figcaption>
</figure>

My talk about [Uppy](http://uppy.io), the file uploader I’ve been working on. It’s got sleek UI, fetches files from local disk, as well as remote Google Drives and Instagrams, or you can snap selfies with a camera, preview and edit metadata, and upload files to the final destination, optionally processing/encoding on the way.

<!--more-->

<figure>
  <a href="https://bit.ly/uppy-talk">
    <img src="/media/2018/01/HJotGMS4f.png">
  </a>
  <figcaption><a href="https://bit.ly/uppy-talk">bit.ly/uppy-talk</a></figcaption>
</figure>

* What it’s like to work in a remote team
* It’s scary to be a developer
* Looking for a view: Yo-Yo, Choo, Bel, template literals, Hyperapp and Preact
* Plugins within plugins
* Golden Retriever — how file uploads survive browser crashes
* Redux without Redux
* A word on accessibility and internationalization

![](/media/2018/01/SypvSfSNf.jpg)

![](/media/2018/01/HkWyYzBEM.jpg)