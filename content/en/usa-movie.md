---
title: "One Month Across the USA: the Movie"
datePublished: 2015-05-21 20:43
cover: http://arturpaikin.com/media/2015/05/usa-movie1432232239668.jpg
published: true
---

I’ve recently returned from a month-long trip across the United States. This time I decided to shoot a movie to go with my usual posts with pictures and stories. Here it is:

<figure>
<a href="https://www.youtube.com/watch?v=Mm1gwVTi7SM" target="_blank"><img src="/media/2015/05/usa-movie1432232239668.jpg"></a>
<figcaption><a href="https://youtu.be/Mm1gwVTi7SM">https://youtu.be/Mm1gwVTi7SM</a></figcaption>
</figure>

I’ve taken it seriously, maybe even too much so (as I often tend to do), so I’ve spent a lot of time editing and writing subtitles in both English and Russian. So please turn those on, unless you speak both languages and have a really loud sound.

Enjoy and let me know what you think! If you really like it,  [you can thank me](http://arturpaikin.com/en/thanks).
