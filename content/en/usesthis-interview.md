---
title: "Uses This interview"
datePublished: 2016-06-15 08:27
published: true
excludeFromFeed: true
tags: note, appearance
---

<figure>
<img src="/media/2018/01/HJLAWRUBM.jpg">
<figcaption>Photo by Afisha Magazine</figcaption>
</figure>

> Happy Tuesday, friends! Here’s Artur Paikin, founder of Baguette, designer and robot maker:

[https://usesthis.com/interviews/artur.paikin/](https://usesthis.com/interviews/artur.paikin/)