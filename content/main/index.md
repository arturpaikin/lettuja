---
title: "Artur Paikin"
description: "I blog in Russian and English, travel, grow food, brew coffee, ride a foldable bike, write code, play interactive fiction games and automate my home"
datePublished: 2050-07-07 07:07
cover: "http://arturpaikin.com/assets/berlin-art.jpg"
profilePic: "/assets/berlin-art.jpg"
template: index
published: true
---

<p class="about-lead">Hi, I’m <span class="p-name">Artur Paikin</span></p>

I blog about my life, travels and experiences in [English](/en/blog/) and [Russian](/ru/blog/).

Passionate about the open web, I make [websites and applications](http://unebaguette.com), program [static site generators](https://github.com/arturi/lettuja) and [DIY automate my home](https://github.com/arturi/kotihome). I enjoy [brewing coffee](https://www.instagram.com/p/0hp13RSSXX), [growing food](https://www.instagram.com/p/4_6LO8ySVL), and playing interactive fiction games. I bike around London, Berlin and Moscow on a [Strida foldable bike](https://www.instagram.com/p/BRJr6x2Dk3I/).

I post pictures on <a href="http://instagram.com/arturi" rel="me">Instagram</a>, and write short stories on <a href="http://www.facebook.com/arturpaikin" rel="me">Facebook</a> and <a href="http://twitter.com/arturi" rel="me">Twitter</a>. I’m also on <a href="https://micro.blog/arturi" rel="me">Micro.blog</a>, say hi! You can email me at <a class="u-email" href="mailto:artur@arturpaikin.com" rel="me">artur@arturpaikin.com</a>.

### Projects

* Fun [10-minute Brooklyn.js talk](https://www.youtube.com/watch?v=CvorlYi4L_c) about how I’ve fixed a broken intercom in my building by making a chat bot
* [Immigrantcast](http://radioimmigrant.com/) (in Russian) — I co-host a podcast where we talk about indie-web, mudlarking, photography and other topics. Every now and then we have guests from all over the world
* [Koti Home](https://github.com/arturi/kotihome) automation system ([talk](https://www.youtube.com/watch?v=Z-NpPj6aKJE) at Queens.js)
* [Lettuja](https://github.com/arturi/lettuja) static site generator that powers this site
* [Uppy](https://uppy.io) modular file uploader
* [Tipi Coffee](https://fb.com/thetipicoffee) pop-up fussy coffee shop
* [Labelmaker](https://github.com/arturi/labelmaker) image annotation plugin
* [Web Development Course](http://unebaguette.com/web-course/)
* [One Month Across the USA](https://www.youtube.com/watch?v=Mm1gwVTi7SM) movie
* [Travel stories](https://www.facebook.com/events/782856498477414/) meetups about traveling around the world

---
