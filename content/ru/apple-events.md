---
title: "Эппл эвенты"
datePublished: 2012-09-17 16:40
description: "На Стрелке, в Кофебине, Долькабаре и Новой Голландии"
cover: "https://lh5.googleusercontent.com/-kNI6fn0Sm9Y/UHGMi93ombI/AAAAAAAACtc/g-WtBc4aD14/s1000/UTK_4216.JPG"
published: true
---

Так случилось, что я иногда веду трансляции Эппл эвентов в Москве. Нахожу место, приглашаю людей, перевожу и комментирую в прямом эфире. Последний раз получилось очень здорово, в Долькабаре:

<figure><img src="https://lh5.googleusercontent.com/-kNI6fn0Sm9Y/UHGMi93ombI/AAAAAAAACtc/g-WtBc4aD14/s1000/UTK_4216.JPG"><figcaption>фото: <a href="http://alexanderutkin.com">Саша Уткин</a></figcaption></figure>

Пришло много людей, Сергей лично всех встречал, очень хорошая атмосфера была. Мне пришлось сидеть на ступеньках, потому что кабель от проектора дальше не доставал, но это пустяки.

<!--more-->

![](http://lh3.googleusercontent.com/-jMx6tvdG3vo/UHGMifbwLXI/AAAAAAAACtY/4UCHxuhjQ4E/s1000/UTK_4205.JPG)

![](http://lh6.googleusercontent.com/-cZcLHBJl0mM/UHGMiu0urBI/AAAAAAAACtg/dphFh74J9Wc/s1000/UTK_4212.JPG)

А раньше было вот как. В Кофе-бине, самая первая встреча.
![](http://lh6.googleusercontent.com/-uKu2E-8vTf4/UHGPIyRjYII/AAAAAAAACuU/boORp-H4i90/s1000/IMG_0273.JPG)

![](http://lh6.googleusercontent.com/-gj0KubCIRkU/UHGPHQHp9GI/AAAAAAAACuE/GEypIA24UFA/s1000/IMG_0263.JPG)

На Стрелке:
![](http://lh4.googleusercontent.com/-UU94YW-peVw/UHGPHBV8U_I/AAAAAAAACt8/Wm_erKCP-Qo/s1000/0_5f58f_a7bb41e9_XXXL.jpeg)

![](http://lh6.googleusercontent.com/-UQ19Tgz2xMY/UHGPHbpJO6I/AAAAAAAACuA/AkbvJPhRa1g/s1000/0_5f590_2972cefa_XXXL.jpeg)
