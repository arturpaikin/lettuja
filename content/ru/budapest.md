---
title: "Рецепт хороших выходных в Будапеште"
datePublished: 2014-06-07 12:15
cover: http://arturpaikin.com/media/budapest/IMG_4868.jpg
published: true
---

![image](/media/budapest/IMG_4868.jpg)

Я провел в Будапеште три прекрасных дня.

Там вкусно, недорого, солнечно, свежая выпечка, отличные бары, термальные ванны, парки и мост.

<!--more-->

***

*Билеты из Москвы купил на сайте [Wizzair](http://wizzair.com/ru-RU/Search), стоили около 4 000 рублей туда-обратно.*

В полете занимался любимым делом: слушал подкасты во время взлета и посадки так, чтобы стюардесса не заметила и не пристала со своим «Sir, I’m gonna ask you to turn off your electronic device» (в США эту [глупость уже отменили](http://www.reuters.com/article/2013/10/31/us-usa-aviation-devices-idUSBRE99U0R320131031)). Главный трюк — пользоваться только левым наушником, прижимая его рукой, как-будто подпираешь голову.

Прилетел.

В будапештском метро ходят такие же вагоны, как в Москве, только состаренные.

![image](/media/budapest/IMG_4776.jpg)

Иду в Эирбиэнби-квартиру через мост Свободы. По дороге ем суп из стакана — популярная тут штука. Очень вкусно.

![image](/media/budapest/IMG_0829.jpg)

![image](/media/budapest/IMG_4778.jpg)

По мосту идет [фирменный будапештский трамвай](http://zyalt.livejournal.com/630479.html). Желтый. Якобы в городе есть еще самый длинный в мире трамвай Сименс (50 метров), но за три дня он мне так и не попался.

![image](/media/budapest/IMG_4870.jpg)

![image](/media/budapest/IMG_4867.jpg)

Добрался до квартиры, забрал ключи. Вот такой вид открывается на внутренний двор школы напротив.

![image](/media/budapest/IMG_4807.jpg)

Это подъезд.

![image](/media/budapest/IMG_4808.jpg)

![image](/media/budapest/IMG_0674.jpg)

А это дверь.

![image](/media/budapest/IMG_4875.jpg)

Квартира оказалась очень уютной и недорогой (1 000 рублей в сутки).

После небольшого отдыха пора идти изучать город. Попадается много приятных граффити.

![image](/media/budapest/IMG_0688.jpg)

![image](/media/budapest/IMG_4799.jpg)

![image](/media/budapest/IMG_0809.jpg)

Местами можно поплавать в фонтане.

![image](/media/budapest/IMG_0538.jpg)

Большие баннеры в центре как бы намекают, что дух бородатых хипстеров в татуировках и чайников с носиком здесь жив.

![image](/media/budapest/IMG_0700.jpg)

И действительно. Хоть и без бороды, но Калита.

![image](/media/budapest/IMG_4812.jpg)

И космический кассовый аппарат.

![image](/media/budapest/IMG_4813.jpg)

И на стене вон что.

![image](/media/budapest/IMG_4811.jpg)

Кофе в [Tamp & Pull Espresso Bar](https://www.facebook.com/tamppull) неплохой, везут от обжарщиков из Англии фурами, чтобы не воздействовать на зерна переменой давления в самолете.

После кофе хочется свежей выпечки, поэтому направляемся в [Butter Brothers](https://www.facebook.com/pages/Butter-Brothers/1383017678583129).

![image](/media/budapest/IMG_0803.jpg)

Отличное место. Круассаны и булочки (особенно с сыром и грушей), каждый день разный набор. И свежий хлеб. И дешево, конечно, рублей на 250 вдвоем плотно позавтракали.

![](/media/budapest/IMG_4817.jpg)

*Когда мы засомневались, хватит ли у нас наличных денег, парень за стойкой сказал «да без проблем, занесете завтра». В таких местах все друг друга знают и булочки в кредит в порядке вещей.*

Еще порекомендую кондитерскую [Cake Shop](http://www.cakeshop.hu/), где все вкусно и печется прямо при вас.

![image](/media/budapest/IMG_4901.jpg)

На улицах Будапешта хорошо со столбами, деревьями, клумбами, коваными решетками и небольшими скверами. Приятный для жизни и прогулок город.

<figure><img src="/media/budapest/IMG_4810.jpg"><figcaption>Просто черный столбик, какое счастье</figcaption></figure>

![image](/media/budapest/IMG_4827.jpg)

![image](/media/budapest/IMG_4822.jpg)

![image](/media/budapest/IMG_0723.jpg)

Рынок.

![image](/media/budapest/IMG_0741.jpg)

![image](/media/budapest/IMG_4825.jpg)

Парк с большим фонтаном.

![image](/media/budapest/IMG_4833.jpg)

![image](/media/budapest/IMG_4835.jpg)

![image](/media/budapest/IMG_0832.jpg)

Ещё одно приятное кафе в Будапеште называется [Blue Bird](https://www.facebook.com/bluebirdcafehungary). Сначала я думал, что это просто название, а потом услышал дикие крики с первого этажа. Схватил сумку и поспешил вниз по винтовой лестнице. А там вот кто:

![Gosha, the Blue Bird](/media/budapest/IMG_0540.jpg)

<figure><img src="/media/budapest/IMG_4792.jpg"><figcaption>— Fascinating!</figcaption></figure>

Синяя птица Гоша бочком-бочком подкрался ко мне и напал на чизкейк, за что в наказание был посажен хозяевами обратно в просторную клетку. На странице кафе есть еще фотографии Гоши, например, [Гоша-бариста](https://www.facebook.com/bluebirdcafehungary/photos/a.212928035550270.1073741831.186018268241247/287057338137339/?type=1).

Карманная растительность.

![image](/media/budapest/IMG_4787.jpg)

Одна из достопримечательностей города — руинные пабы. В центре ими заполнен целый квартал, район VII, [Эржебетварош](http://www.the-village.ru/village/situation/parts/141419-erzhibetvarosh-kvartal-ruinnyh-pabov). Самый первый из них, [Szimpla Kert](http://www.szimpla.hu/).

Вход в Симплу — это просто арка во двор, без всяких дверей. Пространства заведений здесь зачастую распространяются на улицу, количество преград стараются минимизировать.

![image](/media/budapest/IMG_4780.jpg)

В Симпле хорошо. Вино и пиво по 60 рублей.

![image](/media/budapest/IMG_4893.jpg)

![image](/media/budapest/IMG_0685.jpg)

Традиционные длинные венгерские сендвичи.

*![image](/media/budapest/IMG_4786.jpg)*

Как известно, я люблю [сидеть в ванных](/spb-weekend).

![image](/media/budapest/IMG_4785.jpg)

А в этом кафе можно заказать столик прямо в голубом автомобиле напротив.

![image](/media/budapest/IMG_0687.jpg)

Помимо руинных пабов, Будапешт известен термальными ванными, которые сняты в прекрасном фильме «[Гранд отель Будапешт](http://www.imdb.com/title/tt2278388/)».

Те, в которых побывал я, называются [Gellért Baths](http://www.gellertfurdo.hu/), но есть еще несколько. Там очень красиво.

![image](/media/budapest/IMG_4853.jpg)

![image](/media/budapest/IMG_4865.jpg)

![image](/media/budapest/IMG_0784.jpg)

*Стоит 700 рублей вместе с прокатом плавательного костюма, шкафчика и белой простыни. А зайти внутрь поглазеть можно бесплатно.*

Есть открытые бассейны на улице для хорошей погоды и закрытые для плохой. В большом открытом раз в час пускают искусственную волну.

![image](/media/budapest/IMG_4843.jpg)

![image](/media/budapest/IMG_4850.jpg)

После ванн проследуем в бар на заброшеной автобусной станции.

![image](/media/budapest/IMG_4899.jpg)

Чтобы отведать, внимание: [вино с газировкой](http://en.wikipedia.org/wiki/Spritzer) из мини-душа. Там даже пропорции в меню есть, вроде «3/4 вина и 1/4 газировки».

![image](/media/budapest/IMG_4898.jpg)

![image](/media/budapest/IMG_0820.jpg)

Поужинать можно так. Выбираем пасту.

![image](/media/budapest/IMG_4830.jpg)

Специально для нас ее готовят на оливковом масле, с соусом песто и кедровыми орешками.

![image](/media/budapest/IMG_4829.jpg)

Потом едим в парке.

![image](/media/budapest/IMG_4831.jpg)

Советую поспешить в Будапешт, пока город не захватили туристы и он не превратился в Парк Горького в летние выходные ;-)

![image](/media/budapest/IMG_0801.jpg)

![image](/media/budapest/IMG_0781.jpg)

##### Ссылки по теме
* Саша Зайцев [про Будапешт](http://nqst.net/blog/budapest/)
* Илья Варламов рассказывает о своей поездке: [первая часть](http://zyalt.livejournal.com/996867.html) и [вторая часть](http://zyalt.livejournal.com/997644.html)
* Антон Реппонен [выкладывает красивые фото города](http://blog.repponen.com/blog/2014/6/7/budapest-hungary)
* [Как отмечают столетие домов в Будапеште](http://www.the-village.ru/village/situation/abroad/143437-na-vse-sto-kak-v-budapeshte-otmechayut-stoletie-domov)
* [Эржебетварош, квартал руинных пабов](http://www.the-village.ru/village/situation/parts/141419-erzhibetvarosh-kvartal-ruinnyh-pabov)
* Что еще [делать в Будапеште](http://www.the-village.ru/village/all-village/puteshestviya/105049-otdam-darom-buda-pesht)
