---
title: Копенгаген и Берлин
datePublished: 2014-05-13 19:12
cover: http://arturpaikin.com/media/copenhagen-berlin/IMG_4167.jpg
published: true
---

![image](/media/copenhagen-berlin/IMG_4167.jpg)

Две недели в Копенгагене и Берлине. Велосипеды и всë, что на них перевозят. Мосты, парки и кофейни. Велосипеды. Уютное кладбище и королевский дворец. Христиания — деревня хипстеров в центре Копенгагена. Каппинг в Кофе коллектив и The Barn. Трехэтажные завтраки и заброшенный аэропорт. Прогулка под парусом.

<!--more-->

---

## Копенгаген

В Копенгагене красиво, удобно и велосипедно.

Настолько велосипедно, что случаются небольшие велосипедные пробки.

![image](/media/copenhagen-berlin/IMG_4162.jpg)

![image](/media/copenhagen-berlin/IMG_4163.jpg)

<figure>
  <img src="/media/copenhagen-berlin/IMG_9103.jpg">
  <figcaption><a href="https://vimeo.com/141516172">Таймлапс</a> велодвижения в городе</figcaption>
</figure>

Всегда можно припарковать велосипед и полюбоваться набережной по дороге домой.

![image](/media/copenhagen-berlin/IMG_4165.jpg)

Знаменитый хипстерский мост, на котором все время сидят хипстеры.

![image](/media/copenhagen-berlin/IMG_9148.jpg)

Проходим мост и идем на фермерский [рынок Torvehallerne](http://torvehallernekbh.dk/english) — там, среди прекрасных тыкв и сельдереев, в углу разместилась кофейня Coffee Collective. Первый раз попробовал Калиту. Понравилось, [купили для Типи кофе](http://instagram.com/p/l60UVUySbH/).

![image](/media/copenhagen-berlin/IMG_4168.jpg)

![image](/media/copenhagen-berlin/IMG_9078.jpg)

Ричард Троваттен, менеджер из Копенгагена, который [живет в Москве](http://gorod.afisha.ru/archive/datskij-menedzher/) и работает в Dream Industries:
>Я почти не скучаю по Дании: там все работает так хорошо, что тебе даже хочется что-нибудь сломать. Люди довольны своей работой, у каждого есть свое четкое представление о будущем, но именно поэтому там сложно что-то поменять.

И действительно.

![image](/media/copenhagen-berlin/IMG_4178.jpg)

В кафе [Grød](http://www.yelp.com/biz/gr%C3%B8d-copenhagen) двадцать видов каши. Вкусно и красиво. И дорого. В Копенгагене все дорого.

![image](/media/copenhagen-berlin/IMG_4176.jpg)

В Дании совсем не умеют делать плохие вывески. Хочется прямо взять и [научить](/media/copenhagen-berlin/bad-sign.jpg).

![image](/media/copenhagen-berlin/IMG_4174.jpg)

*На этой же улице, кстати, отличная пекарня и напротив еще одна кофейня. И вообще, [район Nørrebro](https://www.google.com/maps/place/Gr%C3%B8d/@55.6840346,12.5705349,17z/data=!4m2!3m1!1s0x0:0x976e483f87b966b) прекрасный.*

Неподалеку обнаружился парк.

![image](/media/copenhagen-berlin/IMG_4180.jpg)

Который оказался — вы не поверите — кладбищем. А это могила:

![image](/media/copenhagen-berlin/IMG_4182.jpg)

Общество настолько разложилось, что на кладбище, оказывается, можно гулять с детьми, устраивать пикники и ездить на велосипеде.

![image](/media/copenhagen-berlin/IMG_4185.jpg)

![image](/media/copenhagen-berlin/IMG_4184.jpg)

Могила пианиста Нестора.

![image](/media/copenhagen-berlin/IMG_4186.jpg)

![image](/media/copenhagen-berlin/IMG_4187.jpg)

Женщина гуляет с собачкой. Собачка сходила по делам, женщина идет с пакетом убирать.

![image](/media/copenhagen-berlin/IMG_4189.jpg)

Неподалеку церковь, ребята сели пообедать.

![image](/media/copenhagen-berlin/IMG_4190.jpg)

Школа.

![image](/media/copenhagen-berlin/IMG_4197.jpg)

Дети после уроков идут на велопарковку, берут свой велосипед и едут домой.

![image](/media/copenhagen-berlin/IMG_4191.jpg)

![image](/media/copenhagen-berlin/IMG_4194.jpg)

На перемене можно попрыгать.

![image](/media/copenhagen-berlin/IMG_4196.jpg)

У машин в городе есть специальный циферблат на лобовом стекле — на нем стрелкой отмечается, во сколько машину припарковали. В центре во многих местах лимит — два часа.

![image](/media/copenhagen-berlin/IMG_4198.jpg)

Это первая и последняя табличка «не ходите по траве», которую я встретил за время поездки. С объяснением, почему нельзя.

![image](/media/copenhagen-berlin/IMG_4254.jpg)

Сотрудник королевского дворца возвращается домой после работы и остановился поболтать с охранником.

![image](/media/copenhagen-berlin/IMG_4256.jpg)

Дворец.

![image](/media/copenhagen-berlin/IMG_9177.jpg)

А это мы с нашим хостом, программистом в Issuu, надуваем матрас без фена и насоса.

![image](/media/copenhagen-berlin/IMG_9069.jpg)

![image](/media/copenhagen-berlin/IMG_9014.jpg)

Электрический столб преукрасили.

*![image](/media/copenhagen-berlin/IMG_4169.jpg)*

Вот девушки купили синтезатор и везут его домой на велосипеде.

![image](/media/copenhagen-berlin/IMG_4173.jpg)

А вот таксист помогает девушке перевезти куда-то ее велосипед.

![image](/media/copenhagen-berlin/IMG_4172.jpg)

Кстати, на велосипеде можно возить велосипед.

![image](/media/copenhagen-berlin/IMG_4259.jpg)

На велосипеде все.

![image](/media/copenhagen-berlin/IMG_4306.jpg)

![image](/media/copenhagen-berlin/IMG_4358.jpg)

Мы не удержались и тоже взяли на прокат (800 рублей в день).

![image](/media/copenhagen-berlin/IMG_4282.jpg)

Сеть велодорог покрывает весь город, и в большинстве случаев они как-то отделены от проезжей части. Для велосипедистов свои светофоры, где зеленый свет загорается на три секунды раньше, чем у автомобилей.

![image](/media/copenhagen-berlin/IMG_4245.jpg)

По такой дороге ехать в радость — удобно и безопасно.

![image](/media/copenhagen-berlin/IMG_4272.jpg)

У каждого веломагазина на улице бесплатный насос.

![image](/media/copenhagen-berlin/IMG_4247.jpg)

А в электричках есть велосипедный вагон.

![image](/media/copenhagen-berlin/IMG_4248.jpg)

Магазин цветов и растений.

![image](/media/copenhagen-berlin/IMG_4249.jpg)

А это открыточный вид со знаменитого канала.

![image](/media/copenhagen-berlin/IMG_4252.jpg)

Парк.

![image](/media/copenhagen-berlin/IMG_4243.jpg)

![image](/media/copenhagen-berlin/IMG_4160.jpg)

### Христиания

![image](/media/copenhagen-berlin/IMG_9025.jpg)

Христиания ([Freetown Christiania](http://en.wikipedia.org/wiki/Freetown_Christiania)) — это район Копенгагена, жители которого решили, что они будут отдельным государством внутри государства. В нем живет около 1 000 человек, которые выкупили землю у города, разрешили сами себе легкие наркотики и запретили тяжелые (оружие и насилие тоже запретили).

Большая часть людей знает Христианию как место, где дуют. Это заблуждение: дуют на одной улице (вполне мирно и ненавязчиво, фотографировать там нельзя), а за ней начинается самое интересное — деревня хипстеров.

![image](/media/copenhagen-berlin/IMG_4209.jpg)

![image](/media/copenhagen-berlin/IMG_4206.jpg)

![image](/media/copenhagen-berlin/IMG_4207.jpg)

![image](/media/copenhagen-berlin/IMG_4212.jpg)

Можно вкусно пообедать, но дороговато: на двоих — 1 300 рублей.

![image](/media/copenhagen-berlin/IMG_4208.jpg)

Вывеска этого [кафе](http://www.morgenstedet.dk/) в латиноамериканском стиле напомнила мне Far Cry 3 и Сан-Франциско одновременно.

![image](/media/copenhagen-berlin/IMG_4235.jpg)

В Христиании проходит негласное соревнование — кто более чудной дом построит.

![image](/media/copenhagen-berlin/IMG_4214.jpg)

![image](/media/copenhagen-berlin/IMG_4221.jpg)

![image](/media/copenhagen-berlin/IMG_9046.jpg)

Куда ни глянь — кадр из «Королевства полной луны».

![image](/media/copenhagen-berlin/IMG_9068.jpg)

Хочется заинстаграмить все, большого труда стоит спрятать айфон и просто наслаждаться.

![image](/media/copenhagen-berlin/IMG_9044.jpg)

Так не бывает.

![image](/media/copenhagen-berlin/IMG_9052.jpg)

![image](/media/copenhagen-berlin/IMG_4343.jpg)

![image](/media/copenhagen-berlin/IMG_4344.jpg)

![image](/media/copenhagen-berlin/IMG_4341.jpg)

Вот мужчина просто взял и поставил перед домом Бутыль. Побольше бы таких мужчин.

![image](/media/copenhagen-berlin/IMG_4227.jpg)

![image](/media/copenhagen-berlin/IMG_4225.jpg)

Пятачок.

![image](/media/copenhagen-berlin/IMG_4228.jpg)

Гуляю, значит, никого не трогаю. И тут — девочка причесывает пони. Представляете? Девочка живет в Христиании и после школы причесывает собственного пони. Шок фото.

![image](/media/copenhagen-berlin/IMG_4230.jpg)

Здесь был Уолтер Вайт.

![image](/media/copenhagen-berlin/IMG_4224.jpg)

![image](/media/copenhagen-berlin/IMG_4223.jpg)

Жители Христиании производят очень популярные в Дании, Голландии, Германии и других странах грузовые велосипеды. Называются Christiania Bikes. На их сайте есть увлекательная страница о том, [что можно перевозить на велоспеде](http://christianiabikes.com/en/category-product/everything-else/) (кроме прочего: диван, собаку, контрабас, огромное бревно). Посмотрите обязательно.

![image](/media/copenhagen-berlin/IMG_4234.jpg)

Велосипеды дорогие — порядка 100–150 тысяч рублей, но они очень качественные и долговечные. Многие покупают такие вместо машины. Я бы тоже купил — ездить в Икею, Оби и за продуктами.

А это бывшие военные казармы, в которых теперь живут семьи. Мимо домов и во дворы можно спокойно пройти-посмотреть, никто не обращает внимания, иногда здороваются.

![image](/media/copenhagen-berlin/IMG_4219.jpg)

![image](/media/copenhagen-berlin/IMG_4220.jpg)

![image](/media/copenhagen-berlin/IMG_4217.jpg)

![image](/media/copenhagen-berlin/IMG_4340.jpg)

[Free-range chicken](http://www.youtube.com/watch?v=ErRHJlE4PGI).

![image](/media/copenhagen-berlin/IMG_4350.jpg)

Кому посылку.

![image](/media/copenhagen-berlin/IMG_4346.jpg)

![image](/media/copenhagen-berlin/IMG_4345.jpg)

А может, присядем?

![image](/media/copenhagen-berlin/IMG_4348.jpg)

![image](/media/copenhagen-berlin/IMG_9238.jpg)

![image](/media/copenhagen-berlin/IMG_9266.jpg)

— Сколько нужно хипстеров, чтобы заготовить дрова?
— Два: один будет рубить, второй красиво укладывать.

![image](/media/copenhagen-berlin/IMG_9232.jpg)

На выходе.

![image](/media/copenhagen-berlin/IMG_4203.jpg)

### Кастеллет и Кофе коллектив

В Копенгагене сохранилась крепость 17-го века в виде звезды, [Kastellet](http://en.wikipedia.org/wiki/Kastellet,_Copenhagen).

![image](/media/copenhagen-berlin/IMG_4286.jpg)

Там тоже везде красиво и открыточно.

![image](/media/copenhagen-berlin/IMG_4300.jpg)

![image](/media/copenhagen-berlin/IMG_4299.jpg)

Вдали виднеются ветряные электростанции. Особенно хорошо их видно из самолета при посадке.

![image](/media/copenhagen-berlin/IMG_4293.jpg)

Русалочка Андресона.

![image](/media/copenhagen-berlin/IMG_4301.jpg)

А вот так делают съезды для колясок и велосипедов. Столб, правда, разложился немного — здесь вам не Германия.

![image](/media/copenhagen-berlin/IMG_4302.jpg)

С погодой повезло.

![image](/media/copenhagen-berlin/IMG_4295.jpg)

Поехали пить кофе в Сифоне у [Кента](http://www.kentkaffelaboratorium.com/). Там же можно и посидеть с вай-фаем.

![image](/media/copenhagen-berlin/IMG_4269.jpg)

Прекрасные [Coffee Collective](http://coffeecollective.dk/), узнав про [нашу кофейню](http://coffee.the-tipi.com), пригласили к себе на каппинг.

Это парковка для детских колясок у входа.

![image](/media/copenhagen-berlin/IMG_4334.jpg)

А это внутри.

![image](/media/copenhagen-berlin/IMG_4333.jpg)

Засыпаем и отмеряем. Бариста поясняет, что у весов должны быть десятые доли грамма, потому что 15,1 — не то же самое, что 15,9.

![image](/media/copenhagen-berlin/IMG_4321.jpg)

Пробуем. Бариста вежлив и подробно все объясняет. Это был второй каппинг в моей жизни, очень интересно.

![image](/media/copenhagen-berlin/IMG_4335.jpg)

Пора ехать дальше — в Берлин. Нашли каршеринг, немецкий парень едет из Швеции через Берлин к себе домой. Попросил встретить его в Мальмо — шведском городе через пролив от Копенгагена.

Поехали по длинному мосту на воде.

![image](/media/copenhagen-berlin/IMG_4359.jpg)

## Берлин

Контейнеры для ненужной одежды.

![image](/media/copenhagen-berlin/IMG_4363.jpg)

Стильные поезда метро.

![image](/media/copenhagen-berlin/IMG_4390.jpg)

![image](/media/copenhagen-berlin/IMG_4375.jpg)

![image](/media/copenhagen-berlin/IMG_4372.jpg)

![image](/media/copenhagen-berlin/IMG_4376.jpg)

В электрички можно с велосипедами.

![image](/media/copenhagen-berlin/IMG_4443.jpg)

Поехали на завтрак. В Германии лучшие в мире трехэтажные завтраки.

![image](/media/copenhagen-berlin/IMG_9281.jpg)

![image](/media/copenhagen-berlin/IMG_9503.jpg)

Небольшой парк, полный светофоров и знаков — видимо, для обучения детей правилам дорожного движения.

![image](/media/copenhagen-berlin/IMG_4377.jpg)

Общественный огород.

![image](/media/copenhagen-berlin/IMG_4382.jpg)

Огромный канцелярский магазин Modulor, где есть, например, буквы на любой вкус.

![image](/media/copenhagen-berlin/IMG_4386.jpg)

3Д-принтер в аренду.

![image](/media/copenhagen-berlin/IMG_4384.jpg)

И даже швейный уголок.

![image](/media/copenhagen-berlin/IMG_4387.jpg)

После Христиании берлинские сквоты выглядят довольно печально и больше напоминают свалку.

<figure><img src="/media/copenhagen-berlin/berlin-squat.jpg"><figcaption>«It used to be a famous pissing corner here» — поясняет мужик, возделывающий грядку на месте бывшего pissing corner</figcaption></figure>

А этот продавец хотдогов — самое интересное явление на Александерплац.

![image](/media/copenhagen-berlin/IMG_4502.jpg)

В супермаркетах покупатели не стесняются думать об экологии и не набирать лишних пластиковых пакетов. Кассир все спокойно взвешивает и пересыпает в многоразовую сумку.

![image](/media/copenhagen-berlin/IMG_4401.jpg)

По-немецки точное расписание автобусов. Первый уже прибыл, следующий будет ровно через 11 минут.

![image](/media/copenhagen-berlin/IMG_4403.jpg)

Журнальный магазин «Do you read me». Здесь есть все, от [Кинфолка](http://sergeykorol.ru/blog/kinfolk/) до [Offscreen](http://offscreenmag.com) и Hello Mr.

![image](/media/copenhagen-berlin/IMG_4407.jpg)

![image](/media/copenhagen-berlin/IMG_9428.jpg)

Детей привезли домой.

![image](/media/copenhagen-berlin/IMG_4415.jpg)

Вообще, Берлин похож на Москву в том, что ему досталась сложная архитектура и много страшных советских домов. И Берлин доказал, что дома можно отремонтировать и перекрасить, рядом посадить деревья и цветы, поставить лавочки, убрать баннеры — тогда скучный серый *район* превратится в приятный для прогулок *квартал*, обрастет ~~бородой~~ уютными кофенями и магазинчиками, и вскоре о нем уже напишут в путеводителях.

Буккроссинг: прочитал — дай другому, а сам что-нибудь еще возьми.

![image](/media/copenhagen-berlin/IMG_4418.jpg)

Стена.

![image](/media/copenhagen-berlin/IMG_4432.jpg)

Большой веганский магазин. Сыры, соевое мясо, мука, несколько видов молока и шоколада — все без животных продуктов.

![image](/media/copenhagen-berlin/vegan.jpg)

![image](/media/copenhagen-berlin/IMG_4439.jpg)

### Аэропорт Темпельхоф

[Темпельхоф](http://www.tempelhoferfreiheit.de/en/visit/) — это заброшенный аэропорт, из которого сделали парк. Уникальное место, где можно покататься на велосипеде или просто погулять по взлетно-посадочной полосе.

![image](/media/copenhagen-berlin/IMG_4454.jpg)

![image](/media/copenhagen-berlin/IMG_4487.jpg)

![image](/media/copenhagen-berlin/IMG_4462.jpg)

Устроить пикник.

![image](/media/copenhagen-berlin/IMG_9510.jpg)

![image](/media/copenhagen-berlin/IMG_4445.jpg)

Прополоть грядку на своем маленьком огороде и лечь отдохнуть.

![image](/media/copenhagen-berlin/IMG_4469.jpg)

![image](/media/copenhagen-berlin/IMG_9505.jpg)

Пасека.

![image](/media/copenhagen-berlin/IMG_9508.jpg)

Грядки.

![image](/media/copenhagen-berlin/IMG_4476.jpg)

![image](/media/copenhagen-berlin/IMG_4477.jpg)

А если вы счастливый владелец, скажем, гигантского паруса, то не найти вам в мире лучше места для прогулки с ним.

![image](/media/copenhagen-berlin/IMG_4463.jpg)

![image](/media/copenhagen-berlin/IMG_4457.jpg)

Закаты в Темпельхофе тоже отличные.

![image](/media/copenhagen-berlin/IMG_4479.jpg)

![image](/media/copenhagen-berlin/IMG_4483.jpg)

![image](/media/copenhagen-berlin/IMG_4488.jpg)

Еще гулял около [заброшенного парка аттракционов](http://www.spiegel.de/international/germany/spreepark-the-story-behind-defunct-berlin-amusement-park-a-922117.html). Пугающе. Парк, кстати, [продается на ебее](http://kleinanzeigen.ebay.de/anzeigen/s-anzeige/erbbaurecht,-vergnuegungsund-freizeitpark-spreepark-/180150666-277-3481?ref=search&clk_rvr_id=590917078583&clk_rvr_id=632504964047). Его владелец сначала вывез несколько аттракционов в Перу, а потом [пытался провезти](http://en.wikipedia.org/wiki/Spreepark) обратно в Германию 180 кило кокаина в аттракционе «летающий ковер».

![image](/media/copenhagen-berlin/IMG_4491.jpg)

### Рынок Markthalle Neun и кофейня The Barn

В этом индийском кафе лучшая индийская еда в Берлине (в других не был). О, этот нан. Я помню о тебе.

![image](/media/copenhagen-berlin/IMG_4371.jpg)

В приятном турецком квартале Берлина, Кройцберг, есть рынок [Markthalle Neun](http://www.markthalleneun.de/).

![image](/media/copenhagen-berlin/IMG_4498.jpg)

Помимо фруктов и овощей, в нем готовят вкусные обеды. Кушают вот за такими столами:

![image](/media/copenhagen-berlin/IMG_9597.jpg)

Это очень вкусно.

![image](/media/copenhagen-berlin/IMG_9587.jpg)

В кофейне [The Barn Roastery](http://barn.bigcartel.com/) отличный интерьер, вежливый сервис и вкусный кофе всеми хипстерскими способами.

![image](/media/copenhagen-berlin/IMG_9427.jpg)

![image](/media/copenhagen-berlin/IMG_4524.jpg)

![image](/media/copenhagen-berlin/IMG_4522.jpg)

![image](/media/copenhagen-berlin/IMG_4520.jpg)

![image](/media/copenhagen-berlin/IMG_4525.jpg)

У посетителя кофейни из рюкзака во все стороны торчал огород с дайконом и прочим сельдереем.  

![image](/media/copenhagen-berlin/IMG_4518.jpg)

Ампельман передает привет.

![image](/media/copenhagen-berlin/IMG_4412.jpg)

[Checkpoint Charlie](http://en.wikipedia.org/wiki/Checkpoint_Charlie) — мемориальный пропускной пункт на границе восточного и западного Берлина.

![image](/media/copenhagen-berlin/IMG_4506.jpg)

Парень в метро старается быстро зарисовать наши велосипеды.

![image](/media/copenhagen-berlin/IMG_4517.jpg)

##### Ссылки по теме
* Христиания, [квартал со своей валютой и законами](http://www.the-village.ru/village/situation/parts/139861-hristianiya-kvartal-skvotterov-i-hudozhnikov-so-svoey-valyutoy-i-zakonami)
* Главный архитектор Копенгагена — [о том, как поднять настроение горожанам](http://www.the-village.ru/village/city/public-space/135187-pryamaya-rech-tina-saabyu-ob-arhitekture-kotoraya-delaet-nas-blizhe)
* Лена [про Копенгаген и Берлин](http://blog.elenazaharova.com/post/82371454088/copenhagen-berlin). Очень много полезной информации: районы, кафе, кофейни, парки и интересные места.
* Эрестад, [город-конструктор в Копенгагене](http://www.the-village.ru/village/situation/parts/144145-erestad-daniya)
