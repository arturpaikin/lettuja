---
title: "Дайджест #12"
datePublished: 2015-08-16 15:44
cover: http://arturpaikin.com/media/2015/08/img_33781439746348045.jpg
published: true
---

![image](/media/2015/08/img_33781439746348045.jpg)

* Подкаст WTF с Обамой
* Переезд в Берлин, США и на Кипр
* WWDC 2015 и diversity
* Инди игры: Lifeline и Sunset
* Образование в Финляндии
* Постоянное пространство XOXO в Портленде
* Интервью с автором The Martian
* Remote Moscow
* Гики в России
* SPA для блогов и журналов
* Рейкьявик и Исландия

<!--more-->

### 001
Барак Обама пришел в гости в подкаст WTF к Марку Мэрону в его гараж:

<figure>
<img src="/media/2015/07/p061915ps-01941435746326596.jpg"></a>
<figcaption>Фото: Pete Souza</figcaption>
</figure>

Обама хорошо говорит, одно удовольствие послушать: [Episode 613 — President Barack Obama](http://potus.wtfpod.com/podcast/episodes/episode_613_-_president_barack_obama/). И посмотреть  [фотографии](http://marcmeetsobama.com/index).

### 002
Дизайнер автомобилей о том, [как он променял Москву на Берлин](http://gorod.afisha.ru/people/dizayner-avtomobiley-o-tom-kak-on-promenyal-moskvu-na-berlin/):

> В целом в московской жизни меня все устраивало: у меня была работа в центре и абсолютно свободный график. Моя жена тогда училась в Лондоне, и я спокойно мог позвонить в офис и сказать: «Ребята, я сегодня не приду, у меня через два часа самолет в Лондон». У меня была свобода, пофигизм, вид на парк Горького, рядом с работой институтская столовая с обедами за 140 рублей. К тому же в Москве почти все мои друзья и мои родители.
>
> Сейчас, когда я приезжаю в Москву, у меня ощущение, будто меня присоединили к выхлопной трубе. Берлин очень зеленый, и это чувствуется сразу. Мои родители, которые прилетали в гости, сказали, что здесь воздух лучше, чем на даче.
>
> Когда я рассказываю немцам, что в Москве абсолютно нормально заплатить 6 евро за стакан пива в баре, — они пучат глаза и говорят, что этого не может быть. В Берлине в хорошем заведении пиво стоит 3–4 евро. Когда я рассказываю, что мы снимали у знакомой двушку на «Бабушкинской» за тысячу, мне никто не верит. Здесь за эти деньги можно снять трешку недалеко от центра. Хотя Берлин по сравнению с Москвой небольшой, тут все недалеко.

### 003
Сегодня немного эмигрантский дайджест ;-) Коля Сулима [о переезде из Минска в США](http://gorod.afisha.ru/people/rezhim-posle-iznasilovaniya-kolumnist-kolya-sulima-o-pereezde-iz-minska-v-ssha/).

И Павел Мунтян — [о переезде на Кипр всей компанией](http://www.the-village.ru/village/business/interview/216157-pavel-muntyan).

### 004
После конференции Apple WWDC Джон Грубер традиционно устраивает лайв-версию своего подкаста The Talk Show. На этот раз его гостем стал Фил Шиллер, вице-президент Эппл по маркетингу. Это было приятным сюрпризом — обычно сотрудники компании общаются с журналистами через пиар-службу и ведут себя достаточно закрыто.

<figure>
<img src="/media/2015/07/tts2015-schiller61435746986207.jpg">
<figcaption>Фото: Марко Армент</figcaption>
</figure>

Получилось здорово, посмотрите: [Live From WWDC 2015, With Special Guest Phil Schiller](http://daringfireball.net/thetalkshow/2015/06/09/ep-123). А Марко написал по этому поводу [пост с фотографиями](http://www.marco.org/2015/06/11/live-with-phil).

### 005
Этот год можно назвать годом равноправия и борьбы с дискриминацией (несмотря на усиливающиеся проблемы). В тех-прессе и подкастах все чаще стали обсуждать участие женщин в индустрии, создавать специальные организации и фестивали, вроде [App Camp For Girls](http://appcamp4girls.com/).

* В ATP целый эпизод обсуждали, как им привлечь больше женской аудитории: «[116: Women Aren’t a Minority](http://atp.fm/episodes/116)». И нашли решение: выпустили эпизод, где гостем стала [Кристина Уорен](http://atp.fm/episodes/119), а Джон Сиракьюза тем временем отправился в другой подкаст — [Rocket](http://www.relay.fm/rocket/20).

* На презентации WWDC, кажется, впервые в истории продукты представляли сразу две женщины по очереди.

* Не первый год о проблеме говорит фестиваль XOXO: [Diversity and Subsidized Passes](http://blog.xoxofest.com/post/121224460200/diversity-and-subsidized-passes):
    > We firmly believe in inclusivity and want everyone to participate, feel welcome, and know that they belong.
    > 
    > XOXO isn’t a tech conference, but as it spread through word-of-mouth, it inherited the homogeneity of tech culture: a dominantly male, dominantly white group of attendees. In its first two years, XOXO was about 80% men and almost entirely white, which doesn’t represent the community we want to foster.

### 006
Ghostly Ferns о [своей работе](http://blog.ghostlyferns.com/agency-of-the-year-nomination/):
> No one is employed by Ghostly Ferns. Rather, we’re a group of freelancers who work on projects together. No set hours. No bosses.
> 
> Ideally, we only work for “happy companies”. What’s a happy company? We define these as any company who is working toward making the world a happier, healthier place. We’re happy and genuinely excited! We don’t work with a company unless we stand behind their mission.

Вдохновляет, в своей работе стараюсь руководствоваться такими же принципами.

### 007
Саша Зайцев съездил в Будапешт и написал прекрасный большой пост [про все виды транспорта в нем](http://nqst.net/blog/transport-in-budapest/). 

А Марина Зайцева о том, [как путешествовать с полугодовалым ребенком](http://marin-k-a.com/blog/all/pervaya-semeynaya-poezdka-s-rebenkom/).

### 008
[Стенограмма выступления](https://www.facebook.com/permalink.php?story_fbid=1576204212630741&id=100007235320406) Евгения Чичваркина в Киеве о бизнесе.

### 009
Я уже советовал книгу [The Martian](http://www.amazon.com/Martian-Andy-Weir-ebook/dp/B00FAXJHCY/), так вот осенью по ней выйдет фильм, а ее автор тем временем дал [хорошее интервью Look At Me](http://www.lookatme.ru/mag/people/experience/211547-andy-weir-interview).

### 010
[Кто и зачем делает игру об уборщице-иммигрантке](http://www.lookatme.ru/mag/live/industry-research/214124-sunset-tale-of-tales). Про инди-игры с необычным сюжетом.

### 011
[I’ve been texting with an astronaut](https://itunes.apple.com/us/app/lifeline.../id982354972?mt=8) — об iOS-игре, где в главной роли астронавт, потерпевший крушение на далекой планете, и сумевший связаться с вами. Это текстовая игра: астронавт пишет вам о том, что с ним происходит и просит советов, а вы выбираете варианты действий, в зависимости от которых развивается сюжет.

*![](/media/2015/07/lifeline_for_ios_11435757410237.jpg)*

Игра идет в реальном времени: если астронавт ушел спать, то ответит вам только утром, через несколько часов.

[Lifeline](https://itunes.apple.com/us/app/lifeline.../id982354972?mt=8) — 3$ в Аппсторе.

### 012
Новая волна мессенджеров на смарфонах набирает все большую популярность (я за [Telegram](https://telegram.org/)), а в некоторых местах на базе них образуются целые сообщества: [In the Siberian province of Yakutia, WhatsApp is basically the internet](http://qz.com/390475/in-the-siberian-province-of-yakutia-whatsapp-is-basically-the-internet/):

> Banks, shops and restaurants maintain channels on WhatsApp; social organizations provide information through its chat groups. For local media outlets, WhatsApp is a substitute for Twitter. News of fires and accidents often appears first on the messaging service. For instance, a police official sends the “journalists’ chat” a message whenever something happens in the city.

### 013
Марко публикует [подробную статистику доходов и расходов](http://www.marco.org/2015/01/15/overcast-sales-numbers) своего подкаст-клиента Overcast. Очень интересно.

> After the self-employment penalties in taxes and benefits, I’m
probably coming in under what I could get at a good full-time job in the city, but I don’t have to actually work for someone else on something I don’t care about. I can work in my nice home office, drink my fussy coffee, take a nap after lunch if I want to, and be present for my family as my kid grows up. That’s my definition of success.

### 014
![image](/media/2015/08/1-phdufpcikyr2hu4klon4ma1439726773897.jpeg)

Дэн Пэрри о том, как он сделал не-конференцию: [Creating a conference: The launch of Epicurrence](https://medium.com/@DannPetty/creating-a-conference-the-launch-of-epicurrence-be925c312b15).

> Real conversations were what I was after, not lectures. I wanted no rockstars, just humans.

> We live in a world today, especially here in the Bay Area, where it’s all about profit and next quarter’s revenue. Making money is not my agenda. I’ve got other projects for that. When my career is over and I’m all washed up, I don’t want to be thinking about how much money I made. I’d rather be thinking of the great experiences and relationships I’ve gained, the places I’ve seen, the people’s lives that were hopefully touched for the better, or my life being better by the people that reached out to me.

### 015
Отличная колонка Алисы Таёжной: «[Как я перестала бояться и научилась жить по средствам](http://www.the-village.ru/village/blogs/city-blog/175261-bednye-lyudi-kolonka-taezhnoy)».

> Даже в самые дорогие театры можно купить билет, отслеживая афишу за два-три месяца. С авиабилетами та же история: каждый новогодний отпуск я всегда продумывала с марта и винила себя за то, какая я скучная, но каждый раз уезжала самым дешёвым способом в место мечты. Вместо отдельной квартиры в этот раз — комната с хозяевами или каучсёрфинг: если путешествовать для вас — приоритет, можно понизить запросы и всё-таки уехать туда, куда действительно хочется.

### 016
Наши каучсерфинг-гости Дагна и Гжегош [про Москву](http://dagnaandgreg.blogspot.ru/2014/06/mockba.html):
> Ku naszemu szczęściu naszym hostem okazała się dwójka rewelacyjnych hipsterów.

Также интересно [о дороге до Иркутска в российском поезде](http://dagnaandgreg.blogspot.ru/2014/07/irkutsk.html).

### 017
Новый подход к образованию в Финляндии:
* Goodbye, math and history: [Finland wants to abandon teaching subjects at school](http://qz.com/367487/goodbye-math-and-history-finland-wants-to-abandon-teaching-subjects-at-school/). 
* И еще: [Загадки финской школы](http://terve.su/zagadki-finskoy-shkoly-menshe-uchi): меньше учишься — больше знаешь?

### 018
XOXO открывает в Портленде [постоянное пространство](http://xoxopdx.com):

*![image](/media/2015/08/hero1438767116116.jpg)*

[A New Experiment](http://blog.xoxofest.com/post/120703410510/a-new-experiment):
> So, this fall, we’re taking the ideas behind XOXO and opening a permanent, year-round space: a 13,000 square foot industrial building in Portland’s Central Eastside.

### 019
По вебу идет волна React, Angular и других модных JS-фреймворков, которые упрощают разработку веб-приложений. Данные в них подгружаются джаваскриптом, а общий подход называется SPA (Single Page Application).

Проблема в том, что технику SPA стали использовать в разработке контентных сайтов — блогов и журналов, а не приложений. На медленном интернете, в мобильном браузере вроде Оперы мини и некоторых сервисах отложенного чтения, пользователь вместо сайта с текстом видит пустую страницу. К тому же, SPA плохо индексируются и архивируются. Пути решения есть: не применять SPA для контентных сайтов и использовать технику [изоморфных приложений](http://bensmithett.github.io/going-isomorphic-with-react/#/44), где первоначальный рендеринг страницы происходит на сервере.

Рассуждения на тему доступного веба:

* [js;dr = JavaScript required; Didn’t Read](http://tantek.com/2015/069/t1/js-dr-javascript-required-dead).

    > Pages that are empty without JS: dead to history (archive-org), unreliable for search results (despite any search engine claims of JS support, check it yourself), and thus ignorable. No need to waste time reading or responding.

    > Because in 10 years nothing you built today that depends on JS for the content will be available, visible, or archived anywhere on the web.

* [Let links be links](http://alistapart.com/article/let-links-be-links).

    > The web doesn’t work that way. Websites aren’t viewed solely through web browsers. People consume websites through apps like Pocket or Instapaper, which try to use the structured information of a web page to extract its relevant content. A browser on a smartwatch might ignore your layout and present your information in a way that’s more suitable for a one-inch screen. Or—who knows?—your website might be used through some future device that will transform the information into thoughts beamed directly into a user’s brain. Even web screen readers don’t work like VoiceOver does on an iPhone, reading out the text in the order it’s laid out under a user’s finger. Web screen readers read through the whole document, ignoring layout, and infer meaning from the standardized semantic definitions of HTML tags. A simple example of when semantics like this matter is the recently introduced main element, used to define the main part of a document. To a sighted user viewing your website through Google Chrome, whether you use ```<main>``` or ```<div id="main">``` makes no difference. To someone using another web client, though, such as a screen reader or Instapaper, the meaning implied by the main element is very important to their software in helping them navigate your document.

* [An Alphabet of Accessibility Issues](https://the-pastry-box-project.net/anne-gibson/2014-july-31).

### 020
Веб-разработчик о том, как использовать новые технологии и не стоять на месте, но при этом не сойти с ума: [On being overwhelmed with our fast paced industry](http://wesbos.com/overwhelmed-with-web-development/).

### 021
Новый герой: [Как Фрэнк Кимеро стал примером для подражания](http://www.lookatme.ru/mag/people/hero/209193-new-hero-chimero).

Фрэнк интересный дизайнер и иллюстратор, я давно слежу за его работой.

### 022
Иранский блогер вышел из тюрьмы через пять лет и не узнал интернет: [The Web We Have to Save](https://medium.com/matter/the-web-we-have-to-save-2eb1fe15a426):
> The rich, diverse, free web that I loved — and spent years in an Iranian jail for — is dying. Why is nobody stopping it?

### 023
Инженер-программист из Петербурга рассказал, [как он стал фермером и начал поддерживать экотуризм](https://tjournal.ru/p/hostel-senoval). Это продолжение истории ребят, которые [уехали из Петербурга жить в деревню](http://land.umonkey.net), построили дом и завели хозяйство.

### 024
Антон Мухатаев ведет на Look at Me блог, где увлекательно и понятно рассказывает о [программировании текстовой игры LAM-40 на Python](http://www.lookatme.ru/mag/blogs/anton-mukhataev/215085-python-classes):
> Начнём с простейшего гейм-дизайна. Игра, которую мы будем писать, рассказывает о борьбе простого человека с бюрократией. Задача игрока — получить до конца дня справку под названием LAM-40 на самом высоком, 40-м этаже огромного государственного учреждения. В честь этой справки я игру и назову.

### 025
Советы от живущего в путешествии дизайнера интерфейсов о том, что стоит, и чего не стоит брать с собой в дорогу: [Stop packing so much: The minimalist packing list](https://medium.com/digital-nomad-stories/a-small-bag-on-a-big-adventure-5e4851ac7801).

### 026
[Гики в России: Как стать свободным  в стране, которой  ты не нужен](http://www.lookatme.ru/mag/live/experience-reports/208453-russian-geeks). Интересно про комиксы, гиков, косплей, видеоигры и мир без границ.

### 027
Хорошие мысли про дела и задачи:

* Илья Бирман: [Главное дело дня](http://ilyabirman.ru/meanwhile/all/pursuit-of-the-day/).
* Leo Babauta: [Purpose Your Day: Most Important Task](http://zenhabits.net/purpose-your-day-most-important-task/).
* Константин Осинцев об управлении задачами: [Как управлять задачами, чтобы сбылось будущее](http://osintsev.me/posts/4).

    > Со временем я понял, что задачи нужно разделить на оперативные и стратегические.
    >
    > Оперативные задачи — это то, что нужно делать каждый день по текущим проектам. Это ваша работа, она держит вас на плаву и дает деньги. Но решая эти задачи, вы стоите на месте. Список оперативных задач почти никогда не убывает.
    >
    > Стратегические задачи ведут вас вперед и создают будущее. Вы их придумываете себе самостоятельно. Цепочка стратегических задач — это путь в будущее, к вашей цели. Если у вас нет таких задач, то вы живете в фильме «День сурка» — каждый день одинаковый.

### 028
Сходил на интерактивный спектакль [Remote Moscow](http://www.remote-moscow.ru/) и остался очень доволен. Дает возможность посмотреть на город, себя и жизнь вообще с другой стороны. Спасибо, [Лена](http://elenazaharova.com), за подарок!

*![image](/media/2015/08/img_75501439729451543.jpg)*

Обязательно сходите. Вот [интервью с создателем](http://www.interviewrussia.ru/art/shtefan-kegi-nikto-ne-smozhet-svesti-remote-moscow-k-odnoy-edinstvennoy-idee).

### 029
На одном дыхании посмотрел первый сезон [Humans](http://www.imdb.com/title/tt4122068/) (особенно начало). Обзор на Лукэтми: [Как устроен лучший научно-фантастический сериал года](http://www.lookatme.ru/mag/live/opinion/215597-humans-tv-show).

> Для рекламы сериала «Люди» канал Channel 4 придумал оригинальную маркетинговую кампанию: во-первых, они завели поддельный сайт для компании Persona Synthetics, проивзодящей андроидов в сериале. Во-вторых, запустили рекламный ролик об андроидах, будто бы их можно купить уже сейчас. Наконец, Channel 4 завели два магазина для Persona Synthetics: один на eBay, где проходили аукционы с продажей андроидов, другой — на улице Риджент-стрит в Лондоне. Это была футуристичная витрина (внутрь зайти было нельзя), в которой использовалась технология Microsoft Kinect, а прохожие могли посмотреть, каких андроидов можно купить в магазине.

### 030
Сергей Капличный ведет интересный блог, и в том числе [дневник  исполнения 100 желаний](http://blog.skaplichniy.ru/all/lifelist/). Мне особенно понравился пост про Рейкьявик, куда я мечтаю съездить:

[#65 Уехать в незнакомый город на весь день](http://blog.skaplichniy.ru/all/65-uehat-v-neznakomy-gorod-na-ves-den/).

### 031
Интервью с Йоном Гнаром, прекрасным мэром Рейкьявика:

* Больше панка, меньше ада — [как анархисты и комики вывели из кризиса исландскую столицу](http://style.rbc.ru/person/2014/07/15/18861/).

* [О прямой демократии и пешеходном городе](http://www.the-village.ru/village/situation/situation/111205-interview-jon-gnarr).

Бонус: раз заговорили про Рейкьявик, Мег Льюис из Ghostly Ferns интересно пишет о проездке в Исландию: [Iceland Travel Tips](http://blog.darngood.co/blog/2014/9/11/iceland-travel-tips).
