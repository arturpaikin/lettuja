---
title: "Дайджест #13"
datePublished: 2016-02-29 12:40
cover: http://arturpaikin.com/media/2015/12/img_56401451341995348.jpg
published: true
---

![image](/media/2015/12/img_56401451341995348.jpg)

* О митапе Brooklyn.js
* Подборка статей для разработчиков
* Instagram Husband
* Internet of cats
* Про Портленд
* Сколько зарабатывает врач, актер и дворник в Москве
* Технологический кооператив Feel Train и базовый доход
* Hello New York
* Website Obesity Crisis
* Карта истории Белоруссии

<!--more-->

### 001
![image](/media/2016/02/brooklyn-js1456715202531.jpg)

Джед Шмит рассказывает о том, [как создавался](https://github.com/jed/building-brooklynjs) один из самых крутых митапов для разработчиков — [Brooklyn.js](http://brooklynjs.com/).

<figure class="small-image">
<img src="/media/2016/01/screen-shot-2016-01-25-at-00-27-061453699670407.jpg">
</figure>


### 002: О разработке
* [Подборка хороших плагинов](http://www.threechords.org/blog/my-atom-setup/) для редактора кода [Atom](http://atom.io). Я перешел на него почти полностью, и вам советую попробовать. Он все еще медленнее Саблайма, но это не критично, зато к нему много хороших плагинов (и можно легко написать свой на JS). И он развивается, а Саблайм не очень.
* [How to Switch from the Imperative Mindset](http://www.lispcast.com/imperative-mindset).
* [Migrating From Bootstrap to Susy](http://www.zell-weekeat.com/migrating-from-bootstrap-to-susy/).
* [Nesting in Sass and Less](http://markdotto.com/2015/07/20/css-nesting/): “Bottom line? If you’re building pretty simple sites, nest to your heart’s content. However, if you’re building large apps or sites—Twitter, Bootstrap, GitHub, NY Times, etc—avoid it and keep your CSS simple, performant, and easy to parse.”
* Не спешите выкидывать jQuery: [jQuery’s Relevancy – There and Back Again](http://developer.telerik.com/featured/jquerys-relevancy-there-and-back-again/). Я и не спешу, использую. Внутри еще отличное видео.
* Небольшая [коллекция советов по CSS](https://github.com/AllThingsSmitty/css-protips).
* Хорошие [ES2015 (ES6) tips, tricks, best practices](https://github.com/DrkSephy/es6-cheatsheet#arrow-functions) и [JavaScript Tips](https://github.com/loverajoel/jstips).
* [Why I Left Gulp and Grunt for npm Scripts](https://medium.com/@housecor/why-i-left-gulp-and-grunt-for-npm-scripts-3d6853dd22b8). Тоже был на Gulp’е, а сейчас на npm-скрипты подсел и рад.
* [Why NPM Scripts](https://css-tricks.com/why-npm-scripts/) — хорошая инструкция на CSS-Tricks и отличный [шаблон системы сборки](https://github.com/damonbauer/npm-build-boilerplate) на Гитхабе.

### 003
Ролик [Instagram Husband](https://www.youtube.com/watch?v=fFzKi-o4rHw) — посвящается всем, кто тоже иногда чувствует, что он “basically a human selfie stick”.

![image](/media/2016/01/screen-shot-2016-01-24-at-23-59-051453697962708.jpg)

### 004
Энди Байо интересно пишет о том, [как его сервер взломали](http://waxy.org/2015/12/the_joy_of_getting_hacked/) (похоже, украинский хакер), и чему это его научило.

### 005
Рейчел запустила опенсорс-проект [Robokitty](https://github.com/rachelnicole/robokitty) — это как мой умный дом, только для котов (Internet of Cats).

### 006
![image](/media/2016/02/img_69251456715141370.jpg)

Немного Портленда:
* [So You’re Moving To Portland](https://al3x.net/2015/11/11/portland.html).
* [A Heartfelt Letter Convinced One Family to Sell Their Home. Now It’s An Airbnb](http://www.portlandmercury.com/portland/a-heartfelt-letter-convinced-one-family-to-sell-their-home-now-its-an-airbnb/Content?oid=17039154). Семья очень хотела продать свой дом в Портленде в хорошие руки, другая семья написала им трогательное письмо, и что в итоге получилось. Драма портленд-стайл.
* [Moving Back to Portland](https://medium.com/portland-oregon/moving-back-to-portland-aa8448642a75#.4zwza234q). 

Ну вдруг вы тоже фанаты, и сможете прочитать столько же постов про Портленд.

### 007
[One Hour Sex Rule](http://www.telegraph.co.uk/women/sex/11964022/Sex-Happy-marriage-means-having-sex-within-an-hour-of-getting-home.html): “I have sex with my husband as soon as he's through the front door”.

И еще про отношения: “[Non-monogamy showed me what it really means to be with someone](http://www.theguardian.com/commentisfree/2015/dec/04/non-monogamy-showed-me-what-it-really-means-to-be-with-someone)”.

### 008
There’s An Entire [Conference Dedicated to Geocities-Style Websites](http://motherboard.vice.com/read/theres-an-entire-conference-dedicated-to-geocities-style-websites). 

Без шуток — [конференция](http://websiteconf.neocities.org), посвещенная олдскул-статичным сайтам:
> A conference celebrating the creative, original, static (no backend) web sites, both old and new, that stand the test of time. It's time to rediscover and bring back the lost art of web site creation.

Угадайте, в каком городе проходит.

А еще есть [конференция по генератору статичных сайтов Jekyll](http://jekyllconf.com/), проходит онлайн.

### 009
Вилладж выяснил, сколько зарабатывает (15 000 рублей) и [на что тратит](http://www.the-village.ru/village/business/schet/226747-dvornik) дворник, живущий в Москве. И еще [актер театра](http://www.the-village.ru/village/business/schet/231555-akter) (30 000 рублей +). 

Раз уж мы здесь, [врач](http://www.the-village.ru/village/business/schet/223299-vrach). Но это все Москва, да.

### 010
<figure class="small-image"><img src="/media/2016/02/screen-shot-2016-02-28-at-22-20-131456716060427.jpg"></figure>

Дариус Каземи недавно переехал в Портленд и создал там вместе с подругой Кортни технологический кооператив [Feel Train](http://feeltrain.com). Собираются делать интернет-ботов и другие интересные штуки. Vice [пишет](http://motherboard.vice.com/read/the-creator-of-ethical-ad-blocker-is-trying-to-build-an-ethical-tech-business):

> It seems impossible to think that our current system will drastically change to include things like a universal income, but small steps may be a unique alternative. Kazemi and his wife Courtney Stanton are making changes in their own way, starting with the founding of Feel Train out of their home in Portland, Oregon. Feel Train is a technology cooperative with a unique business model that is capturing the attention of creatives.
> 
> People seem to be responding to the idea of a small place that isn’t trying to extort value.
> 
> Instead of pertaining to typical hierarchal structures seen in most corporations, the collective makes sure everyone in the company has an equal share. It is “worker-owned,” with all employees having the same amount of power, with the ability to hold each other accountable, and get paid the same salary. The company has a strict cap of eight employees. Anything larger would ruin the structure and integrity of the cooperative.

Еще о кооперативе Feel Train [в их блоге](http://feeltrain.com/blog/hello-feel-train/).

Дариус интересно рассказывает о том, как сделал [бота — сортировочную шляпу из Хогвартса](http://tinysubversions.com/notes/sorting-bot/) для твиттера — программирование случайных рифм.

### 011
Все чаще заходят разговоры о [базовом доходе](https://ru.wikipedia.org/wiki/%D0%91%D0%B5%D0%B7%D1%83%D1%81%D0%BB%D0%BE%D0%B2%D0%BD%D1%8B%D0%B9_%D0%BE%D1%81%D0%BD%D0%BE%D0%B2%D0%BD%D0%BE%D0%B9_%D0%B4%D0%BE%D1%85%D0%BE%D0%B4) — идея в том, чтобы каждый человек получал необходимый для жизни минимум денег (например, $1000 в месяц). Просто за то, что человек. Это позволило бы не сильно переживать о еде и жилье, и дало возможность экспериментировать и заниматься тем, что интересно. Вот, например: «[26 немцам в течение года платили по тысяче евро в месяц, чтобы проверить, будут ли они при этом работать](https://tjournal.ru/p/1000-a-month)».

[Из твиттера Hannah Nicklin](https://twitter.com/hannahnicklin/status/654779034136477697):

> Ideal adblock service:
> * There is a global basic income
> * People only make things for the internet that they want to
> * No ads are needed

Известный «стартап инкубатор» Y Combinator запускает [исследование базового дохода](http://blog.ycombinator.com/basic-income).

### 012
[Hello New York](http://www.book-by-its-cover.com/fineart/hello-ny) — иллюстрированные истории о прекрасном городе. Спасибо, Катя, за наводку. Подарил Лене на Рождество, классная книга.

<figure class="small-image"><img src="/media/2016/02/screen-shot-2016-02-28-at-22-16-091456715963101.jpg"></figure>

### 013
Парень решил приготовить сендвич с нуля: вырастить зелень, добыть соль из океана, подоить корову, сварить сыр: [How to Make a $1500 Sandwich in Only 6 Months](http://www.youtube.com/watch?v=URvWSsAgtJE). Шикарно, мой кумир.

### 014
Engadget пишет, с фотографиями, о новом фестивале XOXO (без которого, кажется, не обходится ни один мой Дайджест): [How an independent art and technology festival captured my heart](http://www.engadget.com/2015/09/19/xoxofest-2015/).

И еще [зарисовки-комиксы](https://medium.com/xoxo/xoxo-livesketches-from-lucy-aacbfa7b9878#.9hd2qgu4w) с выступлений.

### 015
<figure class="small-image"><img src="/media/2016/01/screen-shot-2016-01-25-at-00-03-401453698239887.jpg"></figure>

Internet Archive выложил [большую коллекцию ретро-DOS игр](https://archive.org/details/softwarelibrary_msdos_games): от Prince of Persia до Prehistoric 2 и SimCity. Но самое интересное, что они работают в браузере. На Джаваскрипте, Карл!

### 016
Maciej Ceglowski, хороший человек и автор сервиса закладок Pinboard, рассказывает о том, как сайты стали неподъемными, а хостинг дорогим, и что с этим делать — The Website Obesity Crisis. Приводит остроумные сравнения размера веб-страницы с произведениями русской классической литературы. 

Доступно в двух версиях: [видео выступления](https://vimeo.com/147806338) и [пост](http://idlewords.com/talks/website_obesity.htm), если читать удобнее.

Особенно разработчикам стоит посмотреть, но вообще всем. Я вижу актуальность сказанного ежедневно. Сайты про парки, почту или кино стало «немодно» делать без Реакта — JavaScript на них весит около 1 мегабайта, они тормозят и параллаксятся так, что читать невозможно. Мне приходилось отказываться от интересных проектов, потому что клиенты настаивали на параллаксе, страницах-листалках, блокирующих скролл и прочем мусоре — так теперь модно.

### 017
На острове с мужчиной [живет ручной пеликан](https://www.facebook.com/for.polyakova/posts/10205376073146770).

### 018: Пётр
* Нат и Алена пишут о том, [как можно переехать в США](https://vc.ru/p/californicate): про работу, дом, визы, доставку еды. Сами еще пока не до конца переехали — одной ногой.
* Елена Базулина о переезде и жизни в Нью-Йорке: [первая часть](http://blog.elenabazu.com/nyc/), [вторая часть](http://blog.elenabazu.com/relocate-nyc/).
* Meg Lewis: [12 Reasons Why I’m Leaving NYC](https://the-pastry-box-project.net/meg-lewis/2015-October-15).
* «[Нью-Йорк — это сказка](http://gorod.afisha.ru/people/nyuyork-eto-skazka-bartender-o-begstve-iz-rossii-v-ssha/)».

### 019
Закрылся сервис для разработчиков Parse. Вот что Марко [говорит по этому поводу](https://marco.org/2016/01/30/mjtsai-sunsetting-parse):

> People often ask why I don’t use high-level, proprietary hosting platforms for my apps’ backing services.
> 
> The short answer is that I can’t afford to — for my business models to work, I need to keep costs very low, a discipline I’ve built over time as one of my most important professional skills.
> 
> The long answer is that when I talk about minimizing costs, I’m not just talking about money.
> 
> For whatever it’s worth, running your own Linux servers today with boring old databases and stable languages is neither difficult nor expensive. This isn’t to say “I told you so” — rather, if you haven’t tried before, “[You can do this](https://marco.org/2014/03/27/web-hosting-for-app-developers)”.

### 020
Алексей Черенкевич сделал классную [карту истории Белоруссии](http://map.letapis.by/ru/), и в подробностях [рассказывает о процессе](http://cherenkevich.com/blog/mapa-process/).

### 021
[Пять любимых вещей](http://bestin.ua/people/person/12390/) Антона Шнайдера. Антона всегда интересно послушать или почитать.

### 022
Польский градостроитель Куба Снопек — [о пяти годах жизни в Москве](http://www.the-village.ru/village/city/experience/231165-kuba).

### 023
Максим Кац про [умение делать бизнес и преувеличенность необходимости высшего образования](http://maxkatz.livejournal.com/469069.html) для всех.

### 024
![image](/media/2016/01/hi1453697752662.jpg)

Когда я вышел из кинотеатра после фильма Марсианин ([прочитайте](http://www.amazon.com/The-Martian-Novel-Andy-Weir-ebook/dp/B00EMXBDMA) и [посмотрите](http://www.imdb.com/title/tt3659388/)), обсуждал с Леной, что было бы здорово пройти игру по мотивам этой истории. Но не обязательно с тем же сюжетом, просто что-то про освоение Марса и других далеких планет. Пришел домой, залез в душ, оказалось — мою мечту скоро реализуют — [Astroneer](http://astroneer.space/).

### 025
Путешественнику Антону Кротову исполнилось 40 лет, он [оглядывается на свою жизнь](http://a-krotov.livejournal.com/1011172.html), рассуждает про постоянство намерений и важность занятия своим делом.

### 026
<p>
    <a href="http://comediansincarsgettingcoffee.com/president-barack-obama-just-tell-him-you-re-the-president">
<img src="/media/2016/02/comediansincars-obama1456764965276.jpg"></a>
</p>

Эпизод [Comedians in cars getting coffee с Бараком Обамой](http://comediansincarsgettingcoffee.com/president-barack-obama-just-tell-him-you-re-the-president). Очень приятно посмотреть и послушать живого человека.

### 027
Эрон Гилмор кратко про [плюсы и минусы удаленной работы](http://aarongilmore.com/working-remotely/). У меня очень схожие впечатление и трудности.

### 028
Mathias Buus о том, [как начать программировать](http://mafintosh.com/learning-javascript.html) (или вообще что-то новое изучать):

> Find a project that motivates you.
> 
> Writing bad code is better than not writing any code. Learn from your mistakes.
> 
> Read other people's code.
> 
> Don't listen too much to other devs / books / blogs. Do things your own way. Again, learn from your mistakes.
> 
> Don't be afraid to reinvent the wheel. You'll learn a lot doing so.
> 
> Always publish your code to Github. It doesn't matter if you think it's bad. It gives you a notion of finishing things.

### 029
Василий Зоркий, автор колонок о неизвестном горожанине, о том, [каково быть неизвестным горожанином](http://www.the-village.ru/village/city/experience/231068-zorkiy-coming-out), своей жизни и спектаклях по колонками.

### 030

Илья Бирман [про важность сна](http://ilyabirman.ru/meanwhile/all/sleep/) и о том, что [нужно брать предоплату 100%](http://ilyabirman.ru/meanwhile/all/prepayment/). Оба мнения разделяю, и стараюсь соблюдать.

### 031
Промо-сайт [новой велодороги в Берлине](http://www.radbahn.berlin/#intro).

<figure><img src="/media/2016/02/151118_radbahn_press-image-oberbaum1456716900823.jpg"></figure>

 
