---
title: "Дайджест #14"
datePublished: 2016-11-14 16:40
cover: http://arturpaikin.com/media/2016/11/img_03921479085236594.jpg
published: true
---

![image](/media/2016/11/img_03921479085236594.jpg)

* Harry Potter and the Cursed Child
* Decline of independent blogging
* Бэкапы, Future-safe archives и digital legacy
* Интервью с психотерапевтом, о депрессии
* JavaScript в 2016 году, фреймворк choo, библиотека yo-yo
* Советы о фрилансе и про ИП
* Как совмещать работу и путешествия
* Несколько любимых мной игр-квестов

<!--more-->

### 001
Прочитал Harry Potter and the Cursed Child. Ходил даже на ночную вечеринку в Нью-Йорке, в книжный Barnes and Noble. Доволен, что книгу дали скачать epub файлом, а не просто добавили в библиотеку очередного сервиса. Отвлекся.

<figure class="small-image">
  <img src="/media/2016/11/harry_potter_and_the_cursed_child1479080075579.jpg">
</figure>

Читал с интересом, но обуревали смешанные чувства. Во-первых, это не книга, а сценарий спектакля (впрочем, как и обещалось). Поэтому текст кажется слишком прямым, без деталей и эмоций — они добавляются атмосферой на сцене и актерами. Во-вторых, в новой части цитируются события из прошлых книг, но детали и диалоги слегка меняются. Это переворачивает мою картину мира: я воспринимаю события как давно совершившийся факт, а тут на моих глазах историю переписывают. Похожие ощущения вызывают фильмы после книг, но тут эффект сильнее, потому что вместо фильма — текст, и читатель оказывается не готов.

### 002
[Redesigning Waxy, 2016 edition](http://waxy.org/2016/11/redesigning-waxy-2016-edition/) — Энди Байо (один из создателей XOXO) о том, почему он продолжает писать в свой блог, а не уходит на какую-нибудь модную платформу.

> More people than ever before are able to express themselves on Twitter, Facebook, Instagram, Tumblr, Medium, YouTube, Pinterest, and countless other social platforms. All of that is great.
> 
> But there a few reasons why I’m sad about the decline of independent blogging, and why I think they’re still worth fighting for.
> 
> Ultimately, it comes down to two things: ownership and control.
> 
> Last week, Twitter announced they’re shutting down Vine. Twitter, itself, may be acquired and changed in some terrible way. It’s not hard to imagine a post-Verizon Yahoo selling off Tumblr. Medium keeps pivoting, trying to find a successful revenue model. There’s no guarantee any of these platforms will be around in their current state in a year, let alone ten years from now.

Я считаю, что каждому активному в интернете человеку стоит подумать о том, чтобы завести свой домен и не доверять творчество и мысли только соцсетям и Медиуму. 

С этим [согласен Леонид Каганов](http://lleo.me/dnevnik/2016/10/21.html), хотя я не разделяю сильную нелюбовь к Фэйсбуку:

> И да, я совершенно не понимаю людей, которые вкладывают в Фейсбук собственное время, силы, тексты и фотографии. Ваш труд не найти уже через месяц, потому что Фейсбук не индексируется поисковиками. А в один прекрасный день ваш контент просто убьют — в наказание, по жалобе. Заводите собственный сайт, либо ведите блог на Бинонике или Вордпрессе или в том же полудохлом ЖЖ, а в Фейсбук (и остальные соцсети) настройте зеркалирование. Что тем более полезно, что вы можете писать что угодно: роботы Фейсбука не умеют банить пост, если он «перепост».

И Дэйв Вайнер, [How future-safe are your ideas?](http://scripting.com/2015/07/30/howFuturesafeAreYourIdeas.html)

> Will the Big Think piece you just posted to Medium be there in 2035? That may sound like it's very far off in the future, and who could possibly care, but if there's any value to your writing, you should care. Having good records is how knowledge builds. If we're constantly starting over how can we pretend to be accomplishing anything other than self-promotion? Is that enough? Don't we need more value in our thinking?

[Future-safe archives](http://scripting.com/stories/2007/12/10/futuresafeArchives.html):

> People are humble, no one wants to come out and say their work has any value that's worth preserving past their death, but come on, we know that's not true. If Shakespeare were alive today, he'd be writing on the web. As would Hemingway or Faulkner, Vonnegut or Mailer, John Lennon or Dylan Thomas, Carl Sandberg or Robert Frost. Mozart, Bach and Beethoven. You think there isn't any great literature out there on the web? I wouldn't be so sure about that. What if there is? And what if a baby born today becomes a great creative force? Or what if there's a social disaster like the Holocaust? Did you know that there are preserved diaries from pre-revolutionary America? Writings of ordinary people can be of enormous help to historians. And if we believe in citizen journalism (I do) why not citizen historians? Shouldn't we be thinking out into the future? We should!

Мэтт Гемел о том, что он хочет оставить [digital legacy](http://mattgemmell.com/permanence/):

> I’d like to create a permanent digital record of my work, that will outlive me, and ensure my work is always available to others. It’s a simple enough idea.

### 003
Раз мы здесь, скажу про бэкапы. Делайте бэкапы своих фотографий, проектов и всего на жестком диске.

Самый простой способ бэкапить все и не думать об этом — [Backblaze](http://www.backblaze.com/partner/af6033). $5 в месяц и файлы с компьютера автоматически сохраняются в облако, в фоновом режиме. Украли ноутбук, пролили кофе на клавиатуру, сгорел жесткий диск, случился потоп — данные можно скачать с сервера или заказать жесткий диск домой. Посмотрите [видео](https://www.youtube.com/watch?v=lTT-v7bwJsI) от Джонатана Мена.

Лайт версия: просто складывать фотографии, тексты и все, что важно, в Дропбокс или Гугл драйв (с локальной копией).

Мэтт Гемел [про свою бэкап-стратегию](http://mattgemmell.com/backups/) (она слишком advanced, но круто).

### 004
Отличное [интервью с Василием Эсмановым](https://birdinflight.com/ru/media-2/20161101-vasily-esmanov-42-is-yet-the-best-answer-we-got.html):

> *Думаешь, модель «Слона», «Ведомостей», «Дождя» — более правильная?*
>
> Однозначно правильная. Люди должны платить за контент. Потому что если они не платят, то их «продают», и клиентами медиа становятся те, кто пытается читателям что-нибудь продать.
>
> ...
>
> Что справедливо для здешнего общества — и для Европы, вообще западного мира – люди здесь закрытые. Отчужденные друг от друга. Это непривычно. В России, кажется, есть возможность строить какие-то прочные связи практически из воздуха. Здесь твои связи формируются в колледже или в университете. В качественный вуз стремятся попасть не для того, чтобы получить образование, а ради набора людей, нетворка. В России на самом деле так же — смотришь, где люди друг друга находят: с кем-то в спортзале тренировался, с кем-то в институт ходил, с кем-то служил. И ты тащишь этих людей за собой, потому что хотя бы отдаленно им доверяешь. Самая большая проблема в отъезде — то, что у тебя этих связей нет, их нужно обретать заново, а это занимает время. Нужно с кем-то поговорить, выпить, у вас должны быть какие-то общие истории — это невозможно сделать за неделю или на мероприятии, нужно здесь какое-то время прожить. Я почти два с половиной года здесь прожил, и у меня только недавно появилось ощущение «Я кого-то здесь знаю» — в отличие от Москвы, где я знаю феноменальное количество потрясающих людей, с которыми я могу что-то начать делать. Вот главная разница. А все остальное — бытовые вещи. Переживаемо это все, терпимо.
>
> ...
>
> *Думаешь, приходят времена, когда главный вклад человека в экономику будет скорее в том, чтобы быть беззаботным, ходить по магазинам и есть митболы, нежели в том, чтобы работать?*
> 
> Отсутствие концепции работы не означает безделья. Нет, это возможность заниматься тем, что сложно измерить. Мы из-за этой западной модели немножко помешались на успехе, а он должен быть в чем-то измерим: в деньгах, внимании, известности. Но есть много областей, где сложно что-то мерить: та же культура, образование, воспитание детей. Все это не совсем работа. Вообще, концепции работы не очень много лет — может, двести. Раньше это была просто жизнь. Микроскопическая прослойка людей имела возможность заниматься чем-то еще — у них были на это время и деньги. Сейчас у всех есть возможность заниматься чем-то еще, но есть еще и работа. Она по большей части будет автоматизирована, десятки миллионов людей через пять лет останутся без работы — водители, курьеры.

### 005
Евгения Филатова побывала на фестивале Burning Man и [делится впечатлениями](http://www.wonderzine.com/wonderzine/life/travel/221019-burning-man):

> Одно из правил Burning Man — носить с собой все, что тебе может понадобиться (воду, теплую одежду, очки или маску от пыли) и ни на кого не надо рассчитывать.

### 006
Психотерапевт Дмитрий Ковпак — [о том, как понять, что вашу фобию пора лечить](http://www.the-village.ru/village/people/city-news/214337-fear). Очень интересно про психологические проблемы, которые мы часто списываем как неважные, психотерапевтов и таблетки от депрессии.

И еще: [Что делать, если депрессия возвращается](http://www.wonderzine.com/wonderzine/life/life/216807-when-it-s-back).

### 007
[How it feels to learn JavaScript in 2016](https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f#.7lge5cqf8) — смешно и честно о том, что представляет из себя фронтенд-разработка в 2016 году. Тема “JavaScript fatigue” и “analysis paralysis loop” вообще популярна — новые практики, подходы и фреймворки появляются каждый месяц. Двадцать способов решить одну и ту же проблему и спор о том, какой правильнее, и кто здесь настоящий разработчик, мешают постепенно становиться лучше в своем деле.

Стараюсь найти баланс: тратить 20% времени на изучение нового и споры, что круче, React или Choo, а 80% на собственно работу над проектом.

О том, как решать проблему:

* [How to learn web frameworks](https://medium.com/shopify-ux/how-to-learn-web-frameworks-9d447cb71e68#.r41rvpvmu).
    > If you are that junior developer and you know you need to learn React, or MEAN stack, or whatever, I suggest you take a step back. It’s unrealistic for you to expect yourself to learn a framework that solves a problem you’ve never experienced.
* [How to become a successful developer](https://medium.com/@yoshuawuyts/how-to-become-a-successful-developer-6058723583ef#.kyobj7frh).
    
    > Reinvent the wheel. Others will tell you not to, but they’ve never attempted to build wheels before. They probably don’t understand wheels. Maybe they’re the types that buy new trucks when all they need is a new wheel. Learn how to make wheels.
    >
    > Publish everything you do. No matter how small, send it out into the world. If it’s bad: people don’t care. If it’s useful, people will be grateful. It doesn’t matter if you plan on maintaining it / polishing it. Release it now, or risk never releasing it at all.
* [A Study Plan To Cure JavaScript Fatigue](https://medium.freecodecamp.com/a-study-plan-to-cure-javascript-fatigue-8ad3a54f2eb1#.1fk0denzl).
    > But today, I want to go one step further. Instead of simply complaining about the state of things, I’m going to give you a concrete, step-by-step study plan to conquering the JavaScript ecosystem.
* Прекрасные короткие видео funfunfunction: [Composition Over Inheretance](https://www.youtube.com/watch?v=wfMtDGfHWpA) и [Factory Function](https://www.youtube.com/watch?v=ImwrezYhw4w).

### 008
[Руководство астронавта по жизни на Земле](https://daily.afisha.ru/archive/vozduh/books/chto-my-uznali-iz-knigi-rukovodstvo-astronavta-po-zhizni-na-zemle/).

И по теме: NASA [выпустила крутые постеры в ретро стиле](http://mars.nasa.gov/multimedia/resources/mars-posters-explorers-wanted/) «стань космонавтом».

<figure class="small-image">
  <img src="/media/2016/11/p03-farmers-wanted-nasa-recruitment-poster-600x1479012442241.jpg">
</figure>

### 009
[Микрожилье для макролюдей](http://knowrealty.ru/mikrozhil-e-dlya-makrolyudej/).
> Наш человек в Калифорнии Коля Сулима о том, как микроскопические квартиры помогают сосредоточиться на поисках себя.

И [RV-жизнь](http://knowrealty.ru/rv-zhizn/).

### 010
Yoshua Wuyts о том, как и почему появился его приятный мини-инди JavaScript фреймворк [choo](https://github.com/yoshuawuyts/choo) — [A better frontend experience](https://medium.com/@yoshuawuyts/a-better-frontend-experience-7b0498c85658#.89x579p85).

Choo весит всего около 5kb (mingz), в основе [yo-yo](https://github.com/maxogden/yo-yo) — простой и понятный. Мне нравится, что в JS-фреймворках тоже есть свое небольшое Портленд-стайл сообщество. Принято использовать Browserify (и не спорить слишком много о конфигах Вебпака); отдавать предпочтение понятным и полезным фичам ES6: [const, arrow functions и template strings/literals](https://github.com/yoshuawuyts/es2020); выкладывать много простых и понятных модулей в NPM, комбинировать их с друг другом; использовать [Tape](https://github.com/substack/tape) для тестирования и [Stndard JS](http://standardjs.com/) для правил.

<figure class="small-image">
  <img src="/media/2016/11/yoyojs1479157416014.png">
</figure>

Это такой хиппи-подход к фронтенду, не мейнстрим. Зимой я был на мастерклассе Джеймса Холлидея (Substack, автор Browserify), и тогда первый раз постиг смысл Реакта и Редакса (хотя уже использовал до), на [простом примере](https://github.com/substack/training/blob/3041b1e4e3908d4df1b26cf578c34cd4df8fe9b7/web-dev-whirlwind/example/arch/bus/main.js) комбинации virtual-dom + EventEmitter + main-loop + hyperx. Если интересно, [изучите примеры](https://github.com/substack/training/tree/3041b1e4e3908d4df1b26cf578c34cd4df8fe9b7/web-dev-whirlwind/example/dom).

### 011
Хорошее [интервью с путешественником Антоном Кротовым](http://traveler62rus.livejournal.com/70649.html).

### 012
[McDonald’s Theory](https://medium.com/@ienjoy/mcdonalds-theory-9216e1c9da7d#.9ee0bf6bg):

> I use a trick with co-workers when we’re trying to decide where to eat for lunch and no one has any ideas. I recommend McDonald’s.
> 
> An interesting thing happens. Everyone unanimously agrees that we can’t possibly go to McDonald’s, and better lunch suggestions emerge. Magic!

### 013
Саша Зайцев [улучшает детскую железную дорогу](http://zaytsev.io/blog/childrens-railway/):

> В центре Днепра есть детская железная дорога. Когда мы с дочкой поехали на ней кататься, я заметил, что некоторые вещи можно сделать лучше. Глаза загорелись, включился режим повышенной наблюдательности, и я начал собирать идеи. Дополнительным источником вдохновения был грядущий <nobr>80-й</nobr> день рождения железной дороги.
> 
> За месяц работы удалось придумать идеи, предложить их сотрудникам детской железной дороги, получить добро, довести до ума прототипы и воплотить их в жизнь.

Еще Саша недавно начал [учить венгерский язык просто так](http://zaytsev.io/blog/hungarian/).

### 014
Мне нравятся видео Джона Оливера, он очень смешно и остро рассуждает на тему актуальных вопросов. Например, [про Brexit](https://www.youtube.com/watch?v=iAgKHSNqxa8) (выход Великобритании из Евросоюза). В конце песня — не пропустите.

### 015
Людвиг о том, как он начал [медитировать понемногу](http://grosslarnakh.livejournal.com/100373.html). Почему это просто и полезно.

> Это умение — обнаруживать будоражащие сюжетные линии и не отыгрывать их мысленно — одно из самых ценных, приобретенных мною за последние годы. Меньше драматических переживаний = меньше страхов и сомнений = больше психологической устойчивости и уверенности. Просто учишься быть спокойнее. Воображение больше не ускакивает вдоль придуманных сцен, я не даю фантазии захватить внимание и внутренний зритель все реже переживает вместе с внутренним теликом сильнейшие эмоциональные сцены на пустом месте.

### 016
Несколько хороших советов о фрилансе:

* Фрилансер хочет работать официально, но [не может встречаться с клиентами для заключения договора](http://artgorbunov.ru/bb/soviet/20130119/).
* [Как шрифтовику продавать шрифты законно](http://artgorbunov.ru/bb/soviet/20140426/), без посредников, внутри страны и за рубеж.
* Я веду блог и собираю пожертвования. [Должен ли я регистрировать ИП и платить налоги?](http://artgorbunov.ru/bb/soviet/20140503/)
* [Как брать больше денег](http://artgorbunov.ru/bb/soviet/20151101/).
* Алексей Черенкевич о том, [как он был ИП](http://cherenkevich.com/blog/ip/) и работал официально с разными клиентами.
* И Сергей Король [про свой опыт фриланса c ИП](http://sergeykorol.ru/blog/ip/).

### 017
[Профессиональная корзина яиц](http://ksoftware.livejournal.com/302089.html):
> Разложите их в разные корзины: занимайтесь личными проектами, работайте с другими компаниями, пробуйте, экспериментируйте. Как успевать делать свои проекты и работать? «Платите» себе первому. То есть отдавайте предпочтение своим проектам, а не рабочим.

### 018
[Digital nomad: Как я совмещаю работу и путешествия](http://www.the-village.ru/village/business/opyt/239963-digital-nomad).

И про жизнь в вэне, [A Young Man Quits His Old Life and Goes West](http://www.nytimes.com/2016/06/03/fashion/mens-style/van-life-nomad.html).

> Zach Both left his office job last summer, customized a
Chevy van and hit the road. He hasn’t looked back since.

И на тему переездов, генератор историй “Почему я уезжаю из ...”: [Leaving Everywhere](http://tinysubversions.com/leavingEverywhere/).

### 019
[Writing-first Design](https://signalvnoise.com/posts/3801-writing-first-design) — о том, что дизайн сайта начинается с текста. С примерами.

### 020
Макс Огден рассказывает, [как из Raspberry Pi и Kindle сделать компьютер](https://maxogden.com/kindleberry-wireless.html).

### 021
[Интервью с Марко Арментом](https://www.raywenderlich.com/129219/making-overcast-instapaper-tumblr-top-dev-interview-marco-arment), создателем Overcast и Instapaper, разработчиком Tumblr.

> I work in erratic bursts. I’ll accomplish relatively little for maybe a week, then have an incredible marathon in which I accomplish a week’s worth of work in a few hours. In reality, this is how I’ve always worked, even when employed full-time in regular jobs — I just tried to hide it, and my bosses seemed happy enough with the results of the productive bursts to overlook my useless periods.

### 022
История жизни Романа Мазуренко: [Speak, memory](http://www.theverge.com/a/luka-artificial-intelligence-memorial-roman-mazurenko-bot). И о том, как чтобы сохранить о нем память, друзья и команда Luka сделала бота-аватара. 

Роман делал в Москве вечеринки Idle Conversation, а еще проект для создания онлайн-журналов [Stampsy](https://stampsy.com/about).

### 023
Одной из самых любимых мною игр, Secret of the Monkey Island исполнилось 25 лет (год назад). Создатель рассказывает о том, как делали и выпускали игру на дискетах: [Happy Birthday Monkey Island](http://grumpygamer.com/monkey25).

<figure class="small-image">
  <img src="/media/2016/11/96023-the_secret_of_monkey_island_-cd_dos_vga-51479082293125.jpg">
</figure>

<figure class="small-image">
  <img src="/media/2016/11/mi_title_ega1479081682471.jpg">
</figure>

Если любите интересные квесты с хорошим юмором, поиграйте в [Secret of the Monkey Island](https://en.wikipedia.org/wiki/The_Secret_of_Monkey_Island) и вторую часть. А еще горячо рекомендую [Day of the Tentacle](http://store.steampowered.com/app/388210/), который недавно вышел в remastered-версии с улучшенной графикой и звуком.

### 024
Есть еще две игры, которые я давно хочу посоветовать:

1\. [Gone Home](http://gonehome.game/). В нее рекомендуется играть с вином и другом/подругой, проходится за два часа, как хороший фильм.

> You arrive home after a year abroad. You expect your family to greet you, but the house is empty. Something's not right. Where is everyone? And what's happened here?
> 
> Gone home is an interactive exploration simulator. Interrogate every detail of a seemingly normal house to discover the story of the people who live there. Open any drawer and door. Pick up objects and examine them to discover clues. Uncover the events of one family's lives by investigating what they've left behind.

![image](/media/2016/11/maxresdefault1479082162741.jpg)

[Meet the game that shows us the future of storytelling](https://medium.com/message/meet-the-game-that-shows-us-the-future-of-storytelling-320140f1d216#.xtznjqwgk):

> This tab is for people who don’t play video games. People who watch movies and sophisticated TV dramas instead. People who read fiction.
> 
> Hi.
> 
> You don’t play video games, but I’m going to try to convince you to play one particular game in one particular way.

2\. [Firewatch](http://www.firewatchgame.com/):

> Firewatch is a mystery set in the Wyoming wilderness, where your only emotional lifeline is the person on the other end of a handheld radio.

Вы — смотритель в национальном парке (заповеднике), у вас есть уютная смотровая башня и рация для связи. Посмотрите [трейлер](https://www.youtube.com/watch?v=cXWlgP5hZzc).

![image](/media/2016/11/firewatch0122161280jpg-0d7b84_1280w1479082804253.jpg)

Для тех, кто интересуется деталями, рассказ дизайнера о том, [как создавалась атмосфера в Firewatch](https://www.youtube.com/watch?v=ZYnS3kKTcGg&index=17&list=WL). А вот как выглядела презентация игры — разработчики [собрали настоящую комнату из смотровой башни](http://dewith.com/2015/photos-firewatch-at-gdc/).




