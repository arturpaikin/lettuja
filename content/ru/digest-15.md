---
title: "Дайджест #15"
datePublished: 2017-09-25 14:23
cover: "http://arturpaikin.com/media/2017/09/BJfCepUo-.jpg"
description: "Базовый доход. На товарных поездах через Америку. Про путешествия. О децентрализованном распределенном интернете. Мои впечатления от Nintendo Switch. Игра RimWorld, как создавался Prince of Persia. Учеба и прокраскинация, gap year. Об опенсорс-программировании, жизни и доходах. Приложения будущего — PWA. Заметки о Нью-Йорке"
published: true
---

![](/media/2017/09/BJfCepUo-.jpg)

* Базовый доход
* На товарных поездах через Америку. Про путешествия
* О децентрализованном распределенном интернете
* Мои впечатления от Nintendo Switch
* Игра RimWorld, как создавался Prince of Persia
* Учеба и прокраскинация, gap year
* Об опенсорс-программировании, жизни и доходах
* Приложения будущего — PWA
* Заметки о Нью-Йорке

<!--more-->

### 001

[My own private basic income](https://www.opendemocracy.net/beyondslavery/karl-widerquist/my-own-private-basic-income) — аргументированно о том, почему базовый доход полезен:

> One person’s experience becoming a business owner shows how our economy is based on luck rather than merit and how it rewards people who own stuff rather than people who do stuff.

В Финляндии сейчас идет эксперимент, в котором [некоторые жители получают по €550 вне зависимости от занятости](http://www.rbc.ru/rbcfreenews/56fc06249a79472d647aa541). Медуза публикует [интервью с одним из идеологов базового дохода](https://meduza.io/feature/2017/09/05/v-finlyandii-dve-tysyachi-chelovek-uzhe-polgoda-poluchayut-bezuslovnyy-dohod-560-evro-v-mesyats-prosto-tak-bez-vsyakih-obyazatelstv) в Финляндии.

### 002

Фильм «[На товарных поездах через Америку](https://www.youtube.com/watch?v=sWKuCBZBPIw)». Лучшая часть путешествия Ильи вокруг света. Показывает (кроме, собственно, товарных поездов), что автостоп в Америке возможен, полицейские чаще всего дружелюбны, ну и, если не думать долго, а просто ехать, то все получается.

<figure class="small-image">
  <img src="/media/2017/09/HyylixUib.jpg">
</figure>

### 003

Маша о том, [зачем путешествовать в сложные страны](http://masha-kvashonka.livejournal.com/358480.html):
    
> В большинстве развитых стран процессы, связанные с поддержанием комфорта, доведены до автоматизма. Автоматизм — убийца эмоций и одна из причин, по которой в развитых странах так много людей страдают от депрессий. Прерывание автоматических процессов — один из способов снова научиться ежедневно радоваться малому и ценить то, что дарит нам жизнь каждый день — удобную кровать, вкусную пищу, здоровье, проточную воду, возможность общаться с близкими, когда они в другой стране. Можете дополнить этот список сами.

Сергей Родионов: «[Я путешествую много и недорого](http://www.the-village.ru/village/business/opyt/239689-travel)».

### 004

С интересом наблюдаю за развитием распределенного интернета — это когда файлы и сайты хранятся не на одном сервере, а сразу у многих пользователей-читателей. Как торренты.

Одну из таких систем разрабатывает Доминик Тарр, живущий на лодке с солнечной батареей в Новой Зеландии. [An off-grid social network](https://staltz.com/an-off-grid-social-network.html):

> Scuttlebutt is slang for gossip, particularly among sailors. It is also the name of a peer-to-peer system ideal for social graphs, identity and messaging. Scuttlebutt was created by Dominic Tarr, a Node.js developer with more than 600 modules published on npm, who lives on a self-steering sailboat in New Zealand.

<figure class="small-image">
  <img src="/media/2017/08/r1ZgcMod-.jpg">
</figure>

Интервью, [The History of Scuttlebot with Dominic Tarr](http://www.gwenbell.com/dt-interview/):

> Scuttlebot is an open source peer-to-peer log store used as a database, identity provider, and messaging system. It has: Global replication, File-synchronization, End-to-end encryption. Scuttlebot behaves just like a Kappa Architecture DB. In the background, it syncs with known peers. Peers do not have to be trusted, and can share logs and files on behalf of other peers, as each log is an unforgeable append-only message feed. This means Scuttlebots comprise a global gossip-protocol mesh without any host dependencies.
>
> @gb Dominic, hey. You live on a boat. In New Zealand. How long have you been living on a boat? Did your parents live on a boat (and if not, where'd you grow up)?

Два других проекта в этой области: [Dat](http://datproject.org), для которого уже даже есть [браузер](https://beakerbrowser.com/), и [IPFS](https://ipfs.io/).

Про Dat Desktop [очень интересное интервью в блоге Electron](https://electron.atom.io/blog/2017/02/21/dat) (в этом блоге вообще много хорошего).

### 005

Про то, [как и почему Prince of Persia, игра детства, делалась 4 года](http://levik.livejournal.com/315745.html). Очень интересно, со скетчами и фотографиями.

> Джордан решил сделать своего главного героя максимально реалистичным. Поэтому первым делом он купил видеокамеру и заснял своего младшего брата, как тот бегает и прыгает по парковке недалеко от родительского дома. «Дэвид был не особо хорошим акробатом, но зато он согласился работать за бесплатно», — вспоминал потом Джордан.
> 
> Видеокамера стоила $2,500 — бешеные деньги по тем временам, поэтому молодой программист воспользовался возможностью вернуть её в течении 30 дней, отсняв всё, что ему было нужно для начала.
> 
> В середине 1980х не существовало хороших технологий для перевода видео с плёнки в компьютер, поэтому тут Джордану пришлось поработать. Он приобрёл видео-магнитофон, и стал проигрывать свою плёнку по несколько кадров вперёд, при этом снимая экран телевизора обычным фотоаппаратом. На получившихся фотографиях он обвёл силуэт чёрным фломастером, и выделил нужные части светлым цветом, затем отсканировал результат, и нарезал в памяти компьютера.

### 006

Сабстак (джаваскрипт-программист, активный в опенсорс-сообществе, автор [Browserify](http://browserify.org/)) [строит хижину на Гавайях](https://substack.neocities.org/writeups/spiderfarm-house/), ну и живет в ней:

<figure class="small-image"> 
  <img src="/media/2017/09/S1lZRMEjZ.jpg">
</figure>

> Marina and I haven't had a reason to leave aside from a few errands. It's very nice here and we are well-provisioned. I can wake up, fiddle around with the computer, make some food, make some coffee, drink a beer, fiddle some more with the computer, walk around a bit, fix something, paint, pet kitty, plant some seeds, check on the garden, and go back to sleep. Hobonesis achieved.
>
> [...]
> 
> Retirement is also something that gets dangled in front of us as a reward for our toils, like a late-stage capitalism version of heaven. There is a similar morality that goes along with these ideas, where those who didn't save (usually by exploiting others through owning rental property, gambling on the stock market, or otherwise benefiting from a generational transfer of wealth by plundering the future) are not "worthy" enough to enjoy the ability to rest in old age. That we work so hard for the chance to escape the indignities of the working world right before we die says a lot about our priorities as a global civilization.

### 007

Про «[язык юристов](https://ilyabirman.ru/meanwhile/all/lawyers-language/)», Илья Бирман:

> Если мне непонятен договор, который я подписываю, то как же я могу его подписать? Выходит, своему юристу нужно просто верить? Почему же тогда нельзя «просто верить» другой стороне?
> 
> Когда устраивают доверительные отношения, не нужен юрист. Он нужен, когда ты хочешь чётко зафиксировать договорённости. Плохой юрист говорит: «вот стандартный договор». Хороший юрист помогает учесть все тонкости, о которых ты бы никогда не подумал, и написать о них ясно и однозначно. Именно этим он и его опыт ценны, а вовсе не способностью копировать нечитаемое «стандартное» говно из ворда в ворд.

### 008

Как заработать, путешествуя? [Фриланс и работа на месте: 9 реальных историй](https://chopacho.ru/travel-and-work/).

### 009

У компании Basecamp (в связи с новым руководством страны и не только) появился специальный гид для сотрудников: «[International Travel Guide](https://github.com/basecamp/handbook/blob/master/international-travel-guide.md#company-security-at-the-border)». Особенно интересен и полезен пункт «Checklist: Before You Travel» ;-)

### 010

[Why Learning Is A New Procrastination](https://thecoffeelicious.com/why-learning-is-a-new-procrastination-104b53107e8b):

> You learn how to write and publish a new book. You learn how to launch a successful blog. You learn how to hit your goal on Kickstarter. You learn how to build the next “unicorn”. You learn how to land a job of your dream. You learn how to successfully sell thousands of items on Amazon. You learn how to make millions of dollars in passive income.
> 
> However, the problem is that you do everything except taking action.

И [твит в тему](https://twitter.com/r00k/status/902918202077970432):

> You can passively consume hundreds of articles and podcasts and learn far less than shipping one side project a year *— Ben Orenstein‏*

### 011

Немного [советов основателя](https://habrahabr.ru/post/300414/) от Никиты Обухова, создателя Tilda.

### 012

[Has TJ Holowaychuk been as prolific in the Golang community as he was in the Node.js community?](https://www.quora.com/Has-TJ-Holowaychuk-been-as-prolific-in-the-Golang-community-as-he-was-in-the-Node-js-community/answer/TJ-Holowaychuk)

> Nope, I have zero intention to be, my new goal is to live a better life. In the end open-source doesn’t pay the bills so it’s best to focus on other things if you can, or if you just enjoy the project then that’s cool.
> 
> As far as platforms are concerned, Go is in my opinion far more robust than Node.js, with a rich stdlib and thriving community, so much less is needed on that front. It is roughly the same age as Node, so it doesn’t really need much! There are tons of Go developers with way more skill than me, chances are they already wrote a great implementation of whatever you need haha.
> 
> I spend most of my time enjoying other things now, I write code 2–3 hours a day unless I’m really into something. Time is your real currency! Money is nice and all but don’t waste your time. If you really enjoy the project(s) you’re working on then go for it but don’t neglect other areas of your life (or people).

### 013

Я тут не выдержал и завел [Nintendo Switch](https://www.nintendo.com/switch/) с двумя играми (а больше толком и нет пока): [Mario Kart 8](https://youtu.be/GHz6s5iVpYU?t=5m12s) и новой [The Legend of Zelda: Breath of the Wild](https://youtu.be/r2JtSXXY_WY?t=17m42s). 

![](/media/2017/09/SknFieUsb.jpg)

Два совсем небольших артхаус-обзора:

- [An eight-year-old reviews the Nintendo Switch](https://www.theverge.com/2017/7/26/16029612/nintendo-switch-review-kid-zelda-mario-kart-8)
- [The Nintendo Switch of My Dreams](https://www.nytimes.com/2017/05/13/opinion/the-nintendo-switch-of-my-dreams.html):

    > There is a history to my Nintendo fanaticism. When I was 12, my family lived in Australia. We’d emigrated from Yugoslavia (now Serbia) when the wars began. I was in charge of looking after my 7-year-old sister, and all we did was play games in the dining room we had renamed the “Nintendo Room.”
    >
    > I was an awkward adolescent who didn’t fit in at school — embarrassed by my English-as-a-second-language, arriving at parties in T-shirts my mom got from Target while other girls showed up in tight tops and lipstick. My sister was equally weird, sporting a bowl cut and struggling to make friends. Our dad was dying from cancer in the next room, while my mom worked a full-time job and tended to him.
    >
    > Rather than deal with real life, my sister and I escaped into Zelda. There, our actual circumstances became minor details. There were no immigrants in the world of Nintendo. Being different wouldn’t get us teased, our social status was irrelevant, and anyway we had a more important task to focus on: rescuing a princess from a dungeon.

В общем, мне нравится. Игры увлекательные, отлично сделаны, но их пока немного. В Зелду играю все лето дома и в поездке по Европе. Удобно, что можно играть без дополнительного экрана — телевизора или монитора, которого у меня дома все еще нет. Но экран 6" маловат, конечно, особенно для игры в Марио карт вдвоем. Беспокоит, что до сих пор нельзя никак бэкапить сохранения: потерял Свитч — и все, часы поисков Hylian-щита и 320 клыков моблина — дивайн-бисту под хвост?

### 014

Парень [установил на Стриду аккумулятор и мотор](https://vk.com/al_feed.php?w=wall-52445200_12899). Шикарно, я бы купил. Посмотрите вот [еще фотографии](https://yadi.sk/a/lIL7bKd0reHBt).

### 015

После прочтения этой [статьи про RimWorld](https://habrahabr.ru/company/mosigra/blog/315188/), я тут же [купил](http://store.steampowered.com/app/294100/RimWorld/) ее и стал играть.

> **Парадокс Rimworld: захватывающая сюжетом «песочница»**
>
> Rimworld — симулятор строительства и выживания колонии на не очень дружелюбной планете. Вы строите базу, и время от времени происходят случайные события — то нападут космические пираты, то вспышка звезды выведет из строя все электроприборы, то аккумулятор взорвётся и ваша гидропонная ферма сгорит. 
>
> Ключевая фишка игрового процесса — AI-рассказчик подбирает наиболее интересное «случайное» событие, которое прямо здесь и сейчас окажет наиболее драматический эффект. Хороший или плохой — но эмоционально-сильный.

Вот моя скромная база из четырех колонистов, нескольких котов, крыс, собаки и муффало:

<figure>
  <img src="/media/2017/09/rkzzhTSoW.png">
</figure>

<figure>
  <img src="/media/2017/09/rkgMGhpBib.png">
</figure>

Внимание, опасно: тяжело оторваться.

### 016

Опенсорс-разработчик Sindre Sorhus [очень увлекательно отвечает на вопросы](https://blog.sindresorhus.com/answering-anything-678ce5623798): от любимой футболки до дохода и распорядка дня.

> In the past year, I have been answering absolutely anything people have been curious about. This is the result. It’s a long but good read.

И еще одно [интервью с ним](https://betweenthewires.org/2017/09/04/sindre-sorhus/).

### 017

Про деньги в программировании и опенсорсе.

1. Хороший [доход фултайм-программиста](https://twitter.com/modernserf/status/859054889904857091) в Нью-Йорке.
2. “[How much money do you have?](https://github.com/sindresorhus/ama/issues/89#issuecomment-119942784)” — отвечает Sindre Sorhus.
3. Программист Джеймс (Сабстак): for example! [Here's all the money I've made each year working in tech in USD](https://twitter.com/substack/status/829802572508639232).
4. На доход от Patreon, когда поклонники поддерживают автора чего-либо — блога, подкаста, книг, комиксов, программ — людям удается иногда жить фуллтайм. [Evan You](https://www.patreon.com/evanyou), создатель Vue JS, на Patreon. Подкаст [Hello Internet](https://www.patreon.com/hellointernet). Архивариус и сотрудник Internet Archive, [Jason Scott](https://www.patreon.com/textfiles) на Patreon.

### 018

[Как стать популярным на западе?](https://ilyabirman.ru/meanwhile/all/kak-stat-populyarnym-na-zapade/) — Илья Бирман задает актуальный для многих авторов вопрос и собирает советы в комментариях.

> Дело в том, что большая часть вещей, которыми я занимаюсь, актуальны и в Швеции, и в Португалии, и в Америке. Да что уж, и в Японии, и в Израиле они тоже актуальны. Я бы хотел, чтобы читатели со всего мира читали то, что я пишу.

### 019

Серия на редкость [приятных заметок о Нью-Йорке](http://being-malkovich.livejournal.com/tag/%D0%BD%D1%8C%D1%8E-%D0%B9%D0%BE%D1%80%D0%BA).

И еще в тему, другого автора, [про парк Гарри Поттера](https://www.facebook.com/rabeyka/posts/10206025522815396:1) и [магазин American Doll](https://www.facebook.com/rabeyka/posts/10206067204937423).

### 020

[Хаски — об общежитии МГУ и одиночестве](http://www.the-village.ru/village/weekend/favourite/254041-lyubimoe-mesto-haski). В этом интервью прекрасно ВСЕ.

### 021

Михаил Иванов [про школьное задание дочери](https://www.facebook.com/mikhail.ivanov78/posts/10153789685753812):

> Соне в школе задали очень полезное задание – сделать бюджет жизни выпускника школы. Найти работу, найти стоимость аренды места в комнате, посчитать, сколько уходит на продукты, развлечения, одежду и разобраться, есть ли возможность что-то откладывать. Все цифры должны быть реальными со ссылками на источник.

Всем бы такие школьные задания.

### 022

Джон Сиракьюза делает [обозры тостерных печей](https://www.caseyliss.com/2015/9/10/siracusa-on-toasters). Это очень интересно и смешно, внизу таблица со ссылками на конкретное время в подкастах с обзорами Джона и моделями печей.

### 023

Про [gap year](https://www.facebook.com/DAVoloshin/posts/1379822495412378):

> Есть такая хорошая штука на Западе как gap year. Идея в том, чтобы после школы поискать то, что тебе нравится. Поработать в придорожном кафе, или пройти стажировку в банке, или продавать мороженное на пляже. Очень одобрительно относятся к волонтерам, для поступления в некоторые вузы прямо требуют волонтерский опыт. В общем, разумная модель. Не тратить 4-6 лет жизни на то, чем не будешь заниматься, а (прошу прощения) осознанно выбрать себе дело в жизни. Пусть на 10-15 лет, но зряче. Как же нам этого не хватает!

### 024

[Снять и не умереть в Сан-Франциско](https://knowrealty.ru/snyat-i-ne-umeret-v-san-frantsisko-2/) — Коля Сулима про аренду в городе.

### 025

О том, что [приложения будущего должны работать на веб-платформе](https://ilyabirman.ru/meanwhile/all/web-or-native-future/) — пишет Илья Бирман.

Очень правильный ход мыслей, частично к этому все идет, особенно *не* у Эппл. Многие веб-разработчики сейчас делают прогрессивные веб-приложения PWA — есть у Твиттера, Старбакса, такси Лифт — и считают, что за ними будущее (я тоже на это надеюсь).

Например, о PWA много говорит Henrik Joreteg: [Betting on the web](https://joreteg.com/blog/betting-on-the-web), который перешел на Андроид специально из-за того, что на нем лучше поддержка новых веб-технологий и полноценных веб-приложений (на iOS — только Сафари, остальные браузеры — оболочки, а движок всегда Эппл, такие крутые правила).

### 026

Recycle — Эксперимент: [как жить с компостером для мусора на кухне](http://recyclemag.ru/article/eksperiment-ustanovit-komposter-v-moskovskoj-kvartire):

> Москвичи Юля и Никита два года прожили в Калифорнии, где привыкли раздельно собирать мусор. Вернувшись в Москву, они решили продолжить свой образ жизни, добавив к нему сбор органических отходов. Recycle записал результаты их эксперимента.

Мы тоже уже несколько лет собираем органический мусор. Складываем в морозилку, а раз в неделю относим в ближайший к дому community garden или на фермерский рынок — там его добавляют к общему компосту.

Это в Нью-Йорке. А в Москве у меня на балконе стоял черный ящик, в него складывалась органика (очистки картошки, кофе, яичная скорлупа), к ним добавлялись листья и немного земли, все это встряхивалось. Через месяц-другой превращалось в землю/удобрения. Правда, один раз листьев-земли было мало, и там что-то завелось в компосте, что мне стало страшно, и я его вместе с ящиком выкинул. Но это один раз было, потом, вроде, нормально.

Сплошные плюсы: нет запаха дома, мусор можно выносить реже, полезно для окружающей среды. Советую.

### 027

[Мне грустно, если вы не знаете английского](http://www.lookatme.ru/mag/blogs/anton-mukhataev/212777-learn-at-least-english) — Антон Мухатаев.

> LOOK AT ME — САЙТ О БУДУЩЕМ. К сожалению, русская культура на время увлеклась прошлым и о будущем забыла — уверен, что через пару лет это пройдёт. Но пока дела обстоят так, мы зависимы от тех культур, которые живут будущим. Так получилось, что эти культуры связаны с английским языком — либо он для них родной, либо он на них сильно влияет. А потому, когда мы готовим материалы для сайта, мы предполагаем, что наши посетители знают английский хотя бы на уровне «читать со словарём». По-хорошему говоря, в 2015 году надо обсуждать более насущный вопрос: должен ли образованный человек уметь программировать?
> 
> На деле мы постоянно встречаем комментарии вроде «Почему все книги из спиcка на английском?» — и искреннее непонимание, когда мы делимся видеороликами и статьями не на русском языке. Ладно бы только это: есть и люди, которые гордятся тем, что не знают ни одного языка, кроме родного. Наш редактор Гриша Пророков уже писал о них в своём блоге, не буду повторяться. Скажу лишь, что хотя учить или не учить иностранные языки — личный выбор каждого, знание только родного языка ограничивает ваше понимание мира, каким бы этот язык ни был. О многих вещах вы просто не можете судить, потому что любой перевод — это потеря информации, а двойной или даже тройной перевод часто извращает первоначальный смысл. Можно ли говорить о том, что происходит в науке, креативных индустриях — да что там, почти в любой области, — зная только родной язык? Конечно, нет. Даже носители английского языка вроде Билла Гейтса жалеют, что знают только его. Конечно, если ваш выбор — полная изоляция, то можно остановиться на одном языке. Но я не думаю, что люди, идущие на это, читают Look At Me.

### 028

Сергей Рассказов о том, как он делал [вывеску для Рубинштейна](http://rasskazov.pro/blog/all/rubinshteyn/):

> Ровно год назад я сделал один из самых главных проектов в жизни — вывеску для ставшего культовым «Кафе Рубинштейн» на Рубинштейна 20. Никто до меня, и я сам, не делал раньше ничего подобного в городской среде Петербурга последние несколько десятков лет.


