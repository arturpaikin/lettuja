---
title: "Дайджест #16"
datePublished: 2018-04-25 19:10
description: "В этом эпизоде: Super Mario Odyssey и снова немного про Nintendo Switch, дизайн-студия на лодке, Иммигранткаст, VPN (или как пользоваться интернетом относительно свободно), нехороший перфекционизм и альтернативы Твиттеру: Mastodon и Micro.blog, душевный ютьюб-канал и дроны."
cover: "http://arturpaikin.com/media/2018/04/BySwhDAnG.jpg"
published: true
---

![](/media/2018/04/BySwhDAnG.jpg)

В этом эпизоде: Super Mario Odyssey и снова немного про Nintendo Switch, дизайн-студия на лодке, Иммигранткаст, VPN (или как пользоваться интернетом относительно свободно), нехороший перфекционизм и альтернативы Твиттеру: Mastodon и Micro.blog, душевный ютьюб-канал и дроны.

<!--more-->

### 001

Я тут прошел [Super Mario Odyssey](https://www.nintendo.com/games/detail/super-mario-odyssey-switch) на Nintendo Switch. Понравилось: столько разных королевств со своими особенностями (в Нью-Йорке можно ездить на скутере, ходить по крышам небоскребов и прыгать на светофорах, на Луне меньше гравитация), костюмов для самого Марио (клоун, водолаз, строитель), шуток, хорошей музыки, атмосферы, переходов из нового 3Д-мира в ретро-2Д, говорящая шляпа-друг (мне бы не помешала) — текстом не передать.

![](/media/2018/04/HyhemFR2z.jpg)

![](/media/2018/04/B1RJQYAhf.jpg)

*Чтобы вставить сюда эти скриншоты, мне пришлось достать из Raspberry Pi MicroSD карту и перенести с помощью нее — такой вот Свитч.*

Если хотите быстрого и яркого погружения в игру (а покупать в ближайшее время не планируете), посмотрите обзор [Super Mario Odyssey (dunkview)](https://www.youtube.com/watch?v=e7Z2I9x4Pd0), он прекрасен. И [вот еще Зелды](https://www.youtube.com/watch?v=9EvbqxBUG_c), тоже класс.

И бонусом эмоциальное про Зелду: [I was depressed, anxious, and on the verge of suicide… then Zelda: Breath of the Wild saved me](https://www.gamesradar.com/i-was-depressed-anxious-and-on-the-verge-of-suicide-then-zelda-breath-of-the-wild-saved-me/).

![](/media/2018/04/rJBczt0hf.jpg)

![](/media/2018/04/SJQAfYChz.jpg)

Сама приставка Switch (о покупке я [писал в прошлом выпуске](http://arturpaikin.com/ru/digest-15/#013)) как устройство вызвала у меня немного смешанные чувства: экран смотрится невыгодно в сравнении с современными смартфонами и планшетами, во многих местах ощущение «пластиковой» сборки, беспроводные наушники не поддерживаются, довольно громоздкий и непортативный док, который к тому же иногда царапает экран и перегревает консоль ([в этом обзоре](https://www.hanselman.com/blog/ThePerfectNintendoSwitchTravelSetUpAndRecommendedAccessories.aspx) предлагаются решения этих проблем, путем покупки дополнительных аксессуаров, конечно).

Но зато: легкий интерфейс, джойстик, превращающийся в два (их еще можно держать отдельно, расположив руки свободно друг от друга, и использовать в движении — оттягивать тетиву лука, выше прыгать, ну или доить корову), возможность легко переключаться с игры в дороге на домашний экран, и наоборот. И главное — игры, пусть пока их не слишком много, но они так хорошо сделаны, что о недостатках забываешь.

### 002

[Hundred Rabbits](http://100r.co/) — студия дизайнеров и мейкеров, пары Devine и Rekka. Они уже два года путешествуют на своей лодке Pino по миру, живу и работают на ней же.

<img src="/media/2018/03/ry9SP8uYG.jpg">

Одно из самых любимых открытий за прошлый год. Посмотрел уже половину эпизодов, жутко увлекательно, попробуйте начать с этого: [Nov 2017 — Our Salty Sail to New Zealand](https://www.youtube.com/watch?v=hI_W5fgZUQU) (в конце будет небольшой сюрприз). И [March 2017: Crossing the Pacific Ocean from La Paz to Marquesas](https://www.youtube.com/watch?v=oycB4h4Kvwo).

Их [история на Пейтреоне](https://www.patreon.com/100), там же можно поддержать (я так и поступил):

> We are minimalists, as well as fervent believers in content that is open-source. All in all, we strive for our projects to reflect this. 
> 
> We make games, Oquonie, Verreciel, Hiversaire, Donsol & Markl (in production, AI tactics). We publish books, Thousand Rooms & Wiktopher (in production, desert tales told w/o gender). Making tools is also, for us, a priority, they were made for us but are available for all to use: Marabu (music), Rotonde (decentralized social network), Ronin (a programmable visual editor),  Left (writing app) & Dotgrid (vector line app). Hundredrabbits also makes music, art and plant-based recipes; all while traveling.

Девин еще между делом втянулся в P2P-движуху и сделал [экспериментальную децентрализованную соцсеть Rotonde](https://louis.center/p2p-social-networking/).

![](/media/2018/05/rkLxqRO0G.jpg)

![](/media/2018/05/r1ixcR_Az.jpg)

Почитайте [интервью с Девином](https://monochromatic.co/writing/interview-with-devine) и недавний хороший пост с ответами на вопросы о том, [каково жить на лодке, и как начать путешествовать](https://www.patreon.com/posts/17935401).

### 003

Меня тут неожиданно позвали подкаст записывать, уже писал об этом. Называется «[Иммигранткаст](https://www.spreaker.com/show/immigrantcast)», потому что ведущие и гости в основном живут в разных странах. Но обсуждаем любые интересные темы, от запуска Теслы к Марсу до проявки пленки. Вот три последних выпуска со мной в гостях:

* [11: Дикий, мамонтовый интернет](https://www.spreaker.com/user/immigrantcast/whild-mastodon-indie-net-ep-011) — свободные альтернативы твиттеру Micro.blog и Mastodon (о них снова в этом дайджесте ниже), нелегальное летание дроном в Лондоне, впечатления от Indie Web Camp в Балтиморе и умный дом на Ардуино.
* [12: На Бромптоне по Снегокалипсису](https://www.spreaker.com/user/immigrantcast/brompton-snowpocalypse-ep-012) — про складные велосипеды Стриду и Бромптон, налоги в России и Америке, и экзамены на вождение автомобиля в Нью-Йорке.
* [13: Невидимая внутренняя качка](https://www.spreaker.com/user/immigrantcast/burnout-in-the-ocean-ep-013) — про путешествия на яхте, выгорание на работе, опенсорс, невечность контента в интернете и кофе.

![](/media/2018/04/ByDOn5A2M.jpg)

Этот подкаст — неслучившаяся версия моего собственного, вместо которого случился этот дайджест (который я и считаю своим подкастом :-) Я предпочитаю текст как универсальный формат: по нему можно искать, легко цитировать, он занимает мало места (дешево хостить), доступен людям с особенностями слуха и зрения, его можно отредактировать, обдумать, дать ссылки. Но звук — это отдельный интересный опыт, а поговорить я люблю, поэтому рад.

![](/media/2018/04/ryN4hc03G.jpg)

### 004

[Stardew Valley](https://stardewvalley.net/) — популярная игра о том, как вы сидели в кьюбикле скучного офиса, вам все надоело, а дедушка как раз оставил в наследство небольшой клочок земли на ферме за городом. И вы приезжаете туда, знакомитесь с жителями деревни, садите репу — и понеслось. Сам поиграл немного и оставил, слишком много времени нужно уделять, и конца не видно :-) Может быть, вернусь.

![](/media/2018/04/SyPP-g33M.jpg)

Очень интересно о создателе игры, он сделал ее в одиночку: [Valley Forged: How One Man Made the Indie Video Game Sensation Stardew Valley](https://www.gq.com/story/stardew-valley-eric-barone-profile).

### 005

Про VPN (или как пользоваться интернетом относительно свободно):

<img src="/media/2018/04/S1F365C2z.jpg">

- [Хочу свой VPN за 5 минут. Что делать?](https://medium.com/meduza-how-it-works/%D1%85%D0%BE%D1%87%D1%83-%D1%81%D0%B2%D0%BE%D0%B9-vpn-%D0%B7%D0%B0-5-%D0%BC%D0%B8%D0%BD%D1%83%D1%82-%D1%87%D1%82%D0%BE-%D0%B4%D0%B5%D0%BB%D0%B0%D1%82%D1%8C-5581e536650a)
- [Private Internet Access | Anonymous VPN Service Provider](https://www.privateinternetaccess.com/) — хороший платный сервис, доступно много стран.
- [Свой собственный VPN за 3 минуты](https://p.umputun.com/p/2014/08/12/svoi-sobstviennyi-vpn-za-3-minuty/) — довольно понятный гид (внутри там выбор хостинга на Digital Ocean и установка в него OpenVPN через Docker, но если вы не поняли, что это значит, вы все равно, скорее всего, разберетесь).
- [openvpn-install](https://github.com/Nyr/openvpn-install) — cкрипт для запуска на своем VPS: “This script will let you setup your own VPN server in no more than a minute, even if you haven't used OpenVPN before. It has been designed to be as unobtrusive and universal as possible”.

### 006

Неожиданно увлекся дронами. Оказалось, что есть складной, очень компактный и умный дрон [DJI Mavic Pro](https://www.dji.com/mavic) (и новый Air), который умеет далеко летать, следить за объектом, облетать и снимать панорамы по кругу, и много чего еще.

![](/media/2018/04/S1BdwtCnz.jpg)

Вместо Mavic вполне подойдет модель попроще (и еще меньше), [DJI Spark](http://a.co/6mBqZnJ):

- [Dji Mavic Pro Review: A New Dimension](https://www.theverge.com/2016/11/4/13508560/dji-mavic-pro-drone-review-price)
- Крутой [влог Кейси Найстата про Mavic]( https://www.youtube.com/watch?v=iPG1Xa5Uqwo)

<figure>
<img src="/media/2018/04/H1eBOvt03M.jpg">
<figcaption>“It literally fits in my pocket! Can you see that?!”</figcaption>
</figure>

### 007

Если вы еще не смотрите [канал Лео Баланева](https://www.youtube.com/channel/UCx9afEzgDnqOElC9TnOEsNQ/playlists), очень рекомендую (заодно и на панаромы с дрона посмотреть можно). Душево, интересно, по-настоящему и по-хорошему патриотично (Лео живет в Петербурге и часто путешествует по России).

<img src="/media/2018/04/Hy0bOFC2M.jpg">

- [Про Омск. Без пиханины и постановы](https://www.youtube.com/watch?v=ky4kitHRGc8)
- [Путешествие на Байкал](https://www.youtube.com/watch?v=1G2SVMVAfCY)
- [Та самая деревня из детства](https://www.youtube.com/watch?v=QFWDa6qt7Wg)
- [Ответы на вопросы: как я зарабатываю, монтирую видео и путешествую](https://www.youtube.com/watch?v=dQPZsgG1xrk)

### 008

Посоветую пару сериалов и фильмов:

![](/media/2018/04/SkGYdF0nf.jpg)

* [The Marvelous Mrs. Maisel](https://en.wikipedia.org/wiki/The_Marvelous_Mrs._Maisel) прекрасный сериал про женщину, случайно решившую стать стендап-комиком и Нью-Йорк 50-х годов. Ярко, остро, красиво, смешно.
* [Stranger Things](https://en.wikipedia.org/wiki/Stranger_Things) — город Хоукингс, штат Индиана, мистические события, но главное — прекрасные герои, школьники играют в Dungeons and Dragons, ходят трик-о-тритить на Хэллоуин и все такое.
* [High Maintenance](https://en.wikipedia.org/wiki/High_Maintenance) — сериал про парня, который живет в Бруклине и зарабатывает доставкой марихуанны. Идея сериала в том, что этот парень как бы объединяет совершенно разных людей и коммьюнити Нью-Йорка, от китайцев в возрасте до молодых семей.

### 009

Space X [запустила в космос автомобиль Tesla](https://meduza.io/feature/2018/02/07/space-x-zapustila-raketu-falcon-heavy-avtomobil-tesla-budet-letat-vokrug-solntsa-pod-muzyku-devida-boui), и теперь он летит к Марсу. Я хочу отметить это событие в своем дайджесте :-)

<img src="/media/2018/04/By1AE12nf.jpg">

- [Видео первых часов полета](https://www.youtube.com/watch?v=aBr2kKAHN6M)
- [Подборка лучших фото](https://www.theverge.com/tldr/2018/2/7/16982052/spacex-falcon-heavy-photos-videos-watch-replay)
- [А где же сейчас Тесла?](http://www.whereisroadster.com/)

### 010

Друзья из Кооператива Черный [собрали 4 миллиона рублей краудфандингом](https://incrussia.ru/fly/idealnyj-fandrajzing-ili-kak-sobrat-na-perezapusk-biznesa-bolshe-4-mln-rublej-za-poltora-mesyatsa-kejs-kooperativa-chyornyj/) на открытие новой кофейни, продавая небольшую долю в проекте не крупным инвесторам, а самим посетителям:

> Летом 2017 года сооснователь кофейного кооператива «Чёрный» Артем Темиров понял: нужно либо закрыть дело, в которое вложено пять лет жизни и масса идей, либо искать деньги на его развитие. Прийти к соглашению с банками не удалось (владельцам нечего было дать в залог банкам — кроме бренда и честного имени). Тогда кофейные энтузиасты разработали идеальную схему фандрайзинга: сделать инвесторами своих клиентов и просто неравнодушных людей. Несколько идей предприниматели тестировали на постоянных гостях и знакомых и выбрали лучшую: инвестор получает скидку и процент от прибыли. Одного поста в Facebook о продаже паёв кооператива — об этом написали многие СМИ — оказалось достаточно: команда смогла собрать 4 млн рублей. Темиров рассказал Inc., как бизнес хотели купить в первый же день кампании, почему он рассчитывал только на социальную сеть, а кооператив не будет открывать новые кофейни.

<img src="/media/2018/04/r1jHeuCnG.jpg">

> Когда мы затеяли кампанию с паями, Саша стал нашим первым пайщиком. Он пришёл и сказал: «Я скоро в отпуск уезжаю, копил к отпуску, но на что я там потрачу, на тарелочки да всякие глупости. Лучше до отъезда внесу свой пай, хочу чтобы у вас всё получилось». И у нас всё получилось благодаря Саше и ещё десяткам других поверивших в нас людей. Придя в новую кофейню он не удержался и захотел посидеть за каждым столиком, на каждом стуле. Ведь каждый стол и стул появились благодаря его участию. 

### 011

[Россия и Америка — где дешевле жить](https://snob.ru/selected/entry/130301)? Андрей Мовчан хорошо объясняет об американских доходах и расходах против российских:

> Спасибо Алине Ананьевой, Исааку Беккеру и Financial Samurai: благодаря им мне на глаза попалась табличка «как едва выжить на 200 тыс. долларов в год в США». Табличка представляет собой бюджет семьи с одним ребенком до 13 лет, домом в ипотеке за 700 тыс. долларов, двумя машинами. Семья откладывает 18 тыс. в год на пенсию. С учетом размера налога на недвижимость (более 1% от стоимости дома) и самой стоимости дома, можно предположить, что семья живет в благополучном ближнем пригороде большого развитого города типа Нью-Джерси (Нью-Йорк) или на худой конец Нью-Роксбери (Бостон). В расчете грамотно учтены налоги, которые составят сумму payroll taxes, Medicare tax и income taxes, берущихся в сумме и с работодателя, и с работника.

### 012

![](/media/2018/04/rJpn-en3f.jpg)

Вспомним, что RSS остается [лучшим независимым средством подписки на сайты](https://davidyat.es/2017/05/18/rss-nothing-better/):

> “An RSS feed? That’s so 2007!”
> “Well then what do you suggest instead?”
> “Everyone gets their updates through their Facebook feeds these days! Just make a Facebook page!”
> “That sounds great! Then half of my subscribers will see half of my updates, during weeks in which Zuckerberg chooses to weigh the algorithms more towards news and less towards friends’ photos. Also everyone has to have a Facebook account.”
> “Yeah, okay, point made. Twitter is better anyway: it’s totally public, doesn’t force people to sign in to see feeds, and its feeds aren’t subject to algorithmic manipulation.”
> “Wonderful! With Twitter, people can keep up with updates interspersed with cat photos, stolen jokes, retweeted jokes and celebrities’ political opinions! Subscribers to multiple sites can enjoy the user-friendliness of having them all mushed up together, or having to laboriously visit each outlet’s page to see new updates they missed in the firehose of minutiae!”

Я даже [телеграм-каналы стал в RSS переводить](https://notifier.in/) (дополнительный бонус — кхм-кхм — когда вдруг Телеграм напрямую недоступен):

> Hi, My name is Kirill and I love RSS. I think it's the perfect format for consuming new content at your own pace without being pushed. It also doesn't decide for you what you should see and what shouldn't (I'm looking at you, Facebook).
> 
> Unfortunately, a lot of interesting content isn't distributed that way anymore. So I decided to solve that issue myself and RSSify some content sources:
>
> * Email: get your favorite newsletters or any other types of emails straight to your RSS reader.
> * Telegram channel: free your Telegram chat list from channels and read them via RSS instead.

А читаю в [Feedly](https://feedly.com/) (в него добавляете сайты, на которые хотите подписаться) и [Reeder](http://reederapp.com/) (это сама программа для чтения, но можно и прямо в Фидли читать).

### 013

Mastodon и Micro.blog — это сейчас две самые яркие распределенные альтернативы Твиттеру, Фэйсбуку и прочему Телеграму. Ну, если не считать старые надежные блоги + подписка на них по РСС.

[Mastodon](https://joinmastodon.org/) — это федеративный подход к соцсети, ближайшим аналогом будет электронная почта. У вас адрес на Gmail, а у друга на Mail.ru — но вы прекрасно можете общаться. То есть, вместо одного Твиттера, есть много разных инстансов Мастодона, каждый со своими правилами и небольшими отличиями. При этом вы можете подписываться на людей из любого инстанса и переписываться с ними, отправлять сообщения, читать ленту. При таком подходе все не зависит от одной компании, потому что ее нет — это много разных отдельных сущностей, связанных общим протоколом.

<img class="small-image" src="/media/2018/04/H1k0AvChz.jpg">

Нолан Лоусон рассказывает, [что такое Мастодон, и чем он лучше Твиттера](https://nolanlawson.com/2017/10/23/what-is-mastodon-and-why-is-it-better-than-twitter/). Недавно он перешел на Мастодон окончательно, удалив свой аккаунт в твиттере, [и интересно объяснил, почему](https://nolanlawson.com/2017/11/15/why-im-deleting-my-twitter-account/). А создатель Мастодона получает деньги на его разработку (опенсорс) [от пользователей, через Патреон](https://www.patreon.com/mastodon).

[Micro.blog](http://micro.blog) — это площадка, где можно завести себе свой микроблог (как Твиттер или Фэйсбук, ну или Мастодон ;-), со своим доменным именем (john.blog, например, или roasting.coffee), или добавить РСС своего существующего блога (откуда угодно, на Вордпрессе, Jekyll, SquareSpace или Тильде). В таком случае ваш блог не будет привязан к Микро.блогу, последний просто выступает агрегатором, то есть как фэйсбук и Твиттер, только блоги принадлежат самим пользователям, а не площадке. Микро.блог может перестать существовать, а все блоги останутся. Проще говоря, это как бы РСС-ридер, лента подписок, но с возможностью отвечать на посты, как в Твиттере.

<img class="small-image" src="/media/2018/04/Hy80Cw02f.jpg">

Предполагается, что в микро-блог вы будете постить чаще, чем в традиционный блог: быстрые записи (чаще без заголовков), фотографии как в инстаграме. Кто-то постит микро и большие посты вместе в один блог (как [Мэнтон](http://manton.org/) и [Брент](http://inessential.com/), например), а кто-то разделяет. Можно разделять и через «категории» ([пример](https://aaronparecki.com/articles)).

Мэнтон Рис [о том, почему он запустил Micro.blog](http://help.micro.blog/2015/why-i-created-this/):

> Micro.blog encourages publishing at your own domain name, where you can control your own content, but it still integrates posts into a familiar timeline user interface, with centralized replies, favorites, and an open API based on JSON Feed and IndieWeb standards.
> 
> You can register for free, bring your own weblog, or let Micro.blog host a microblog for you with a simple paid subscription. Map a custom domain to your site and get your content out whenever you want. I hope you like it.

Брэнт Симмонс о том, [почему Micro.blog — это не очередной App.net](http://inessential.com/2018/02/01/why_micro_blog_is_not_another_app_net) (потому что были уже попытки сделать альтернативный Твиттер).

Я зарегистрировался в Микро.блоге (так как у меня свой блог, это бесплатно), подписался на нескольких человек, осваиваюсь. Уже нашел через него пару новых интересных людей. Микро.блог не замена «традиционным» соцсетям, но там собралось свое приятное камерное коммьюнити. В нем и Мастодоне есть что-то такое от форумов и фидо начала интернета, когда еще не так много пользователей, и люди вроде как добрее друг другу, словно маленький сад (комьюнити гарден) в моем любимом Ист Вилладже. Кооператив Черный и «Кофе на кухне» вместо Старбакса и Кофемании.

Доступны приложения для мобильных, а вся коммуникация осуществляется по открытому протоколу Micro Pub.

Вот примеры (микро)-блогов (длинные и короткие записи, фото, лайки из интернета):

- http://mike.fm
- http://manton.org
- http://aaronparecki.com
- https://eli.li/
- http://inessential.com

Все они ведут свои блоги на разных площадках, а общаются в Micro.blog. Так победим ;-)

### 014

[Owning It](https://colinwalker.blog/owning-it/), пишет Colin Walker:

> Rebooting the blog was one of the best things I've ever done, and microblogging alongside longer posts has given me the new approach I sought.
> 
> Social blogging may have given me a larger audience (at least on Google+) and I may have convinced myself I didn't care about ownership, but nothing could actually have been further from the truth.
> 
> I was throwing hundreds of thousands of words away in search of validation. It's just lucky I wrote a lot of them offline so still have a copy.
> 
> You're never sure what's going to happen with words you don't control. Medium has flip-flopped on different business models. Google+ radically changed its approach after failing to take on Facebook. Facebook itself makes constant changes to the news feed and its priorities.

### 015

Оказалось, проблема, которая доставляет мне порой немалый дискомфорт — известная: [Perfectionism, anxiety and learning to be kind to yourself](https://medium.com/samsung-internet-dev/perfectionism-anxiety-and-learning-to-be-kind-to-yourself-e3c23710704):

> I was ‘diagnosed’ as a perfectionist with anxiety last year. It was actually a relief to finally have a name to put to the stress I was feeling. Every time I made a mistake or had a mental block on a piece of work I spiraled into depression and paralysis, making the mental block worse and my disgust with myself greater [...]
> 
> The average person is, well, average, but because of social media and this idea of the ‘tech celebrity’ we hear a lot about others’ accomplishments and not much about their hardships. We see people succeeding and creating and being excellent, but they don’t broadcast their mediocrity, or the times when they made mistakes.

Крайне рекомендуется к прочтению, особенно для тех, кто часто бывает недоволен собой и своей работой.

### 017

![](/media/2018/04/rkliYtRhf.png)

На новогодних каникулах прошел [Thimbleweed Park](https://thimbleweedpark.com/) — новый квест от создателей моих любимых шедевров Secret of the Monkey Island и Day of the Tentacle.

![](/media/2018/04/HJRVFKC2M.jpg)

Пиксельная графика, современные детали, тонкий юмор, с легким налетом Twin Peaks (так говорят) — все хорошо, хочется проходить снова. А Secret of the Monkey Island в моем списке игр по-прежнему занимает первые строчки.

### 018

Нолан Лоусон перешел на опен-сорс смартфон — поставил LineageOS, не стал ставить сервисы Гугла. Рассказывает, какого это: [Living with an open-source phone](https://nolanlawson.com/2017/11/27/living-with-an-open-source-phone/) — очень заманчиво, хочу попробовать. Поглядываю на One Plus и Redmi Note 5 Pro.

### 019

Приложение [PhotoScan](https://www.google.com/photos/scan/) от Гугла очень [хорошо справляется с задачей оцифровки бумажных фотографий](https://www.youtube.com/watch?v=MEyDt0DNjWU), рекомендую. Попробуйте, скорее всего, превзойдет ваши ожидания.
