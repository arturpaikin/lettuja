---
title: "Дайджест #17"
datePublished: 2019-01-12 13:10
description: "Водительские права, свежий Иммигранткаст, личные финансы, закрытость iOS, соларпанк будущего, бернаунт и завод."
cover: "http://arturpaikin.com/media/2019/01/B1KUJrLf4.jpeg"
published: true
---

<figure>
  <img src="/media/2019/01/B1KUJrLf4.jpeg">
  <figcaption>Это narrow boat в Лондоне. На ней, кстати, вполне <a href="https://www.inyourarea.co.uk/news/what-living-on-a-boat-in-london-is-really-like/">неплохо живут</a></figcaption>
</figure>

В этом эпизоде: водительские права, свежий Иммигранткаст, личные финансы, закрытость iOS, соларпанк будущего, бернаунт и завод.

<!--more-->

---

### 000

Из личных новостей. В августе получил права в Нью-Йорке, так что осень прошла под знаменем дорожных путешествий. Напишу про это отдельно. Дорожные путешествия — топчик (так теперь говорят в Москве). И да, кстати, ВкусВилл — тоже топчик.

![](/media/2019/01/rJg6p4LGV.jpeg)

<figure>
  <img src="/media/2019/01/HJbZZH8MN.jpg">
  <figcaption>Bears over there</figcaption>
</figure>

«Права» после экзамена вот такие выдали, следующим утром уже машину взял в прокате:

<img class="small-image" src="/media/2019/01/HyacoLLf4.jpg">

## Иммигранткаст

### 001

В нашем с Сашей и Сережей [подкасте](https://www.patreon.com/immigrantcast), вышло много новых клевых эпизодов. Как внимательные читатели могли заметить, подкаст выходит чаще, чем Дайджест (это потому что Саша заставляет и ругается). Послушайте вот:

* [Андрей Ситник про лингвопанк — выпуск 33](https://www.spreaker.com/user/immigrantcast/andrey-sitnik-linguopunk-ep-033): Нью-Йорк, опен-сорс, космическая еда.
* [Гаянэ Багдасарян про создание шрифтов — выпуск 33](https://www.spreaker.com/user/immigrantcast/gayaneh-bagdasaryan-ep-031): все об инди-бизнесе по продаже шрифов и гуманистических гротесках.
* [Картонные медведи штата Нью-Йорк — выпуск 27](https://www.spreaker.com/user/immigrantcast/cardboard-bear-immigrantcast-ep027): про американские дорожные путешествия и ночевку в машине как раз.
* [Радио Гринч из Спрингфилд, штат Огайо — выпуск 26](https://www.spreaker.com/user/immigrantcast/radio-grinch-ep-026): про настоящую Америку, которой я в своем Нью-Йорке и не видал.

Да вообще все выпуски слушайте, конечно.

## Про финансы

### 002

Подкаст на Медузе «[Семь кучек с деньгами. Как правильно копить?](https://meduza.io/episodes/2018/11/05/sem-kuchek-s-dengami-kak-pravilno-kopit)». Девушка на своем опыте рассказывает, как она откладывает часть зарплаты на приятные траты, путешествия, пенсию, «fuck off фонд» и так далее. Тоже стараюсь разделять деньги на разные кучи — не важно, какое количество.

И еще подкаст: «[Все, что вам нужно знать про планирование пенсии в 20 и 30 лет](https://www.ohmydollar.com/podcast/everything-need-retirement-planning-30s/)».

### 003

Лилиан Карабайк (Lillian Karabaic) фрилансер, умеющая жить очень бюджетно. При этом у нее получается путешествовать, откладывать деньги, покупать технику и все необходимое. Увлекательно читать про цели и бюджет, Лилиан публикует [красивый отчет каждый месяц](http://anomalily.net/may-2018-life-money-report-spend-all-the-money/):

<img class="small-image" src="/media/2019/01/Bk0JDFVM4.jpg">

*Лайфхак, как прикинуть американские доходы-расходы на российские: сумма годового дохода в США в долларах равна месячному в России в рублях. То есть, $80,000 в год в Нью-Йорке примерно равно 80,000 ₽ в месяц в Москве.*

> Save $10,000 — I got really, really close ($7,200), but I ended up deciding to spend a bit of money before the end of the year on business expenses (new laptop, annual software, etc) for tax reasons. I feel pretty solid about my savings rate considering that I earned less than ~$19,000 tax-home this year, though, putting me at a 39% savings rate (not calculating before-tax savings rate since my taxes are complicated as a sole proprietor with inventory). Obviously; this isn’t possible without the monthly federal health care subsidy I get, and my co-pay assistance program (since the cost of my drugs exceeds my annual income). I’ll dive more into this goal on my 2019 goals post and my annual report.

А вот [бюджет и планы на новый 2019 год](http://anomalily.net/2019-budget/), в которым Лилиан даже меня удивила ситуацией с ее американской страховкой. Прочитайте, это 🔥 Или вот [как прошел  2018 год](http://anomalily.net/2018-goals-review/).

О [нестабильном фрилансе](https://www.instagram.com/p/BfeKAqyl4hK/):

> I have reluctantly accepted the fact that I am a “self-employed creative”, a job title that sounds like it comes from a bystander quote in a NY Times article about Portland’s artisanal popcorn scene.
But that is exactly what I am; I mainly work for myself and my livelihood comes from a podcast, an online course, and an illustrated book filled with cartoon cats explain Roth IRAs. I dress up in glittery spandex costumes to teach about budgeting.
> 
> And 3 days a week, I get up at 4:30AM to vacuum and clean toilets at a gym for 50 cents above minimum wage. Sometimes other gigs come my way. Between these 2-5 jobs in a given month, my bills are more than covered, and I put aside a few hundred to a few thousand in savings each month.
> 
> I’m doing fine financially; I funded a 13-country trip in cash a few months back, I have 8 months of expenses in an emergency fund, and a healthy retirement portfolio. I buy nice coffee whenever I want it. I don’t have any debt.

### 004

Речь пошла о страховке, чтобы ответить себе на большинство вопросов о том, что с ней в Америке не так: «[Welcome to Our Modern Hospital Where If You Want to Know a Price You Can Go Fuck Yourself](https://www.mcsweeneys.net/articles/welcome-to-our-modern-hospital-where-if-you-want-to-know-a-price-you-can-go-fuck-yourself)»:

> Welcome to America General Hospital! Seems you have an oozing head injury there. Let’s check your insurance. Okay, quick “heads up” — ha! — that your plan may not cover everything today. What’s that? You want a reasonable price quote, upfront, for our services? Sorry, let me explain a hospital to you: we give you medical care, then we charge whatever the hell we want for it.
> 
> If you don’t like that, go fuck yourself and die.
> 
> Honestly, there’s no telling what you’ll pay today. Maybe $700. Maybe $70,000. It’s a fun surprise! Maybe you’ll go to the ER for five minutes, get no treatment, then we’ll charge you $5,000 for an ice pack and a bandage. Then your insurance company will be like, “This is nuts. We’re not paying this.” Who knows how hard you’ll get screwed? You will, in three months.

## О погоде

![](/media/2019/01/B17kerLM4.jpeg)

### 005

[Три причины (и ни одного спойлера), почему мы обожаем «Очень странные дела» ](https://meduza.io/feature/2017/10/28/tri-prichiny-i-ni-odnogo-spoylera-po-kotorym-my-obozhaem-ochen-strannye-dela):

> Герои «Очень странных дел», кажется, живут в разных вселенных не только с нами, но и с братьями Даффер: те родились после 1983 года (когда происходят события в сериале), а мечтали и взрослели в 1990-е. Но то, как режиссеры описывают свое детство, наверняка покажется вам знакомым. Братья росли в глубинке, часто убегали в лес, бродили с друзьями по железнодорожным путям, а самым большим богатством считали VHS-кассеты со старыми фильмами. В результате — приключения вымышленных героев для подростков становятся куда большим источником вдохновения, чем поступки старших братьев и сестер, не говоря уже об их папах и мамах.
> 
> Когда в первой серии «Очень странных дел» маленький Майк Уиллер пытается уговорить свою семью отправиться на поиски его пропавшего друга Уилла Байерса, отец пропускает его слова мимо ушей, а мать устраивает скандал. Поэтому Майк тайком берет велосипед и сбегает из дома — а увидев, что в это же время к его старшей сестре в окно лезет одноклассник, лишь разочарованно вздыхает. Семьи для большинства героев «Очень странных дел» — всего лишь коробки, в которых хранятся любимые игрушки. Как и в романе Стивена Кинга «Оно», взрослые просто не замечают творящегося вокруг зла.
> 
> Поэтому весь сериал герои руководствуются уроками вымышленных героев. «Как бы поступил на нашем месте Фродо? Что скажет на это учитель Йода? А не может ли этот взрослый быть таким же предателем, как генерал Ландо?» — все время спрашивают персонажи, а зрители понимают, что и сами до сих пор равняются на тех же героев, ориентируются на те же поведенческие модели, живут в той же системе координат. Массовая культура последних десятилетий, со всем своим чудовищным перепроизводством, устроена так, что жизненный опыт давно уже легче получать из книг, фильмов и сериалов, чем из взаимодействия с реальными людьми. Особенно из сериалов. Чтобы принадлежать к «поколению Stranger Things», вовсе не обязательно провести детство в Америке 1980-х, — именно поэтому шоу пользуется таким успехом во всем мире.

### 006

Смешной хороший ролик на две минуты «[Как голосовать](https://www.youtube.com/watch?v=8uMZL3jkJV4)» (на американских выборах):

<figure>
<a href="https://www.youtube.com/watch?v=8uMZL3jkJV4" target="_blank">
  <div class="video-overlay">
    <img src="/media/2019/01/SJjZAFVMN.jpg">
  </div>
</a>
</figure>

> "How to Vote," a public service message from @electrolemon, @adamlisagor, and the team at @sandwich — via @waxpancake

### 007

На «Зиме» пишут: «[“Невроз иммигранта”, кризис идентичности, “социальная смерть”: как их пережить после переезда](https://zimamagazine.com/2018/11/psihologicheskie-problemy-immigracii/)». Да, во многом узнал себя.

### 008

Владимир Шрейдер [о дизайн-образовании](https://medium.com/russian/%D0%BE-%D0%B4%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD-%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B8-b05d150910f6):

> Я считаю, что наступила эра пост-дизайна. Главной целью сегодня является донесение информации. «Айфон» с основным шрифтом Comic Sans продолжит блестяще работать, и это будет заслугой дизайнера. Чтобы угодить капризной моде, дизайнер может потребовать помощи маляра, который перекрасит иконки, а не наоборот.
> 
> Зачеткой студента должен стать его аккаунт в Google Analytics. Дизайнер ищет мнение обычного человека, а не мнение профи с многолетним стажем работы. Цель клиента — это цель дизайнера, а не наоборот.

### 009

[О сроках выполнения задач и прокрастинации](https://lost.report/blog/ru/o-srokah-vypolnenija-zadach-i-prokrastinacii/):

> О прокрастинации обычно пишут лишь с негативной стороны — все хотят избавиться от вредной привычки делать все в последний момент. И заодно ищут способ отучиться от привычки по кругу проверять Telegram, Facebook, Slack и почту. Но есть в этой теме один нюанс и он кроется в типе задачи и результате.

### 010

> “You have too much time on your hands.”
“There are more important things for you to worry about.”
“Don’t you have anything better to do?” 
> 
> Fuck anyone who judges how you spend your time on this planet.

*— [Louie Mantia](https://twitter.com/mantia/status/930803612451610624)*

### 011

Про модную тему бернаута (выгорания), [большой анализ](https://www.buzzfeednews.com/article/annehelenpetersen/millennials-burnout-generation-debt-work):

> Those two years as a nanny were hard — I was stultifyingly bored and commuted an hour in each direction — but it was the last time I remember not feeling burned out. I had a cellphone, but couldn’t even send texts; I checked my email once a day on a desktop computer in my friend’s room. Because I’d been placed through a nanny agency, my contract included health care, sick days, and paid time off. I made $32,000 a year and paid $500 a month in rent. I had no student debt from undergrad, and my car was paid off. I didn’t save much, but had money for movies and dinners out. I was intellectually unstimulated, but I was good at my job — caring for two infants — and had clear demarcations between when I was on and off the clock.

О бернауте в айти [в одной картинке](/media/2019/01/SyXRBrIGN.jpg). На самом деле, выгорание это правда и серьезно, во всех сферах жизни. Автор интересно все разбирает — про разницу эпох и ожиданий, хотя в основном про Америку.

<img class="small-image" src="/media/2019/01/S1la5L8ME.jpg">

### 012

[Сабстак](https://substack.net/#cv-experience) о том, что будущее за жизнью вне городов, кооперативами и солнечной энергией. Так как пост опубликован в распределенной P2P соцсети [Scuttlebutt](https://www.scuttlebutt.nz/), и для чтения нужно установить программу-клиент, приведу-ка его здесь целиком (😂):

> The future is rural, cooperative, and solar powered
> 
> Life in cities is difficult. Speculators, airbnb, and urbanization are pushing rents ever higher while wages remain stagnant with the possibility of complete and permanent collapse from automation. In a city, most people rent, which is a social exchange where you pay a substantial portion of your wages to a landlord who does nothing. Landlords might do something when you don't pay on time, like calling a sheriff to perform an eviction. Life in the city under these circumstances isn't really yours. You can't modify your surroundings to your liking if you're on a month-to-month lease and have to worry about getting your deposit back. You can't amass the raw materials to do things for yourself. You're not invested in the long term when you might have to move on short notice. You're probably not doing to be planting fruit trees if your time horizon is only a year or two, but we need long-term planning if we're going to survive metastasized late-stage capitalism. Our deindustrialized cities don't need people to work, they need people to keep up the pyramid scheme of property development and land values which increase every year.
> 
> What better time to swear off cities and live off the land? Solar power is cheap. Cell phone coverage and speeds are good, and you can always buy an antenna. There's no longer much of a reason to show up in an office to pretend to be working. You can do bullshit office work from the forest just as easily as a suburban hellscape, but much more cheaply. Cities and rural areas alike are full of rules, but rural areas are much less likely to actually enforce them. When enough people flagrantly violate the rules, you have the beginnings of a political constituency. If punk is about bottomfeeding on an empire flippantly, then solarpunk is about moving to where the future is going to be and quietly but conscientiously refuting the prohibitions and prescriptions of the legacy world that got us into such a mess in the first place. What legitimacy can the legacy world claim, when so much of how they would have us act in accordance with their laws makes us complicit in their crimes of living beyond what this planet can reasonably bear?
> 
> Solarpunk is not some architectural rendering of curved glass buildings surrounded by forest, it's about saying "fuck it". If you know where the future should be, why wait for permission from the people who are more interested in preserving the wealth and power of the already powerful rather than solving our real human problems.
> 
> Rural areas offer certain space for new kinds of political organization that challenge dominant modes. We can re-localize food production. We can re-localize manufacturing based on what people around us need. We can help each other to meet our basic needs and desires, from food to mesh networks to beer brewing and story telling. We can organize our affairs among ourselves equally. Most importantly, you can get away with things. Low population density makes you less legible. You can build things for yourself and nobody except for perhaps some of your neighbors will notice or care. You can also build things with your neighbors that are greater than the sum of their parts.

Я [писал](http://arturpaikin.com/ru/digest-15/#006), что сам Джеймс с Мариной пару лет назад купили кусок земли, [построили на нем хижину](https://substack.net/writeups/spiderfarm-house/) и живут-поживают.

Есть моя любимая отечественная версия этой идеи — [land.umonkey.net](https://land.umonkey.net/blog/).

### 013

«[Американское хвалить](https://www.facebook.com/sobolevskiy/posts/10213443487589078)»:

> Всем известна песня про то, что американская улыбка фальшива. Типа вот встретили тебя, улыбаются во весь рот, а на деле думают: нелепый парень! Вероятно, это так. Но это вообще не важно. Мне не нужно знать — искренне это или нет, зачем? Я больше никогда не увижу этого человека.
> 
> Еще американцы, как мне показалось, очень настроены на поддержку и не ставят себе целью сформулировать, почему ты что-то сделал херово. В этот раз я с разным успехом читал доклады в разных компаниях и юзер-группах: было очень легко расслабиться, люди позитивно реагируют почти на все, даже на лажу. Примерно то же самое было с линди-хопом. Чëрт его знает, но танцевать там будучи супер-начинающим, намного легче, чем у нас, в России.

Я очень рад, что [начал выступать](http://arturpaikin.com/en/home-automation-talks/) на конференциях и митапах именно в Штатах — более приятной и располагающей аудитории сложно пожелать.

### 014

[#отстаньотсебя](https://www.facebook.com/zhuravskaya/posts/1931580340213191):

> флешмоб, которого не хватает, должен называться #отстаньотсебя
> в предисловии к флешмобу я пояснил бы, что мне повезло с химией организма, я очень оптимистичный зайка, но самое мое грандиозное преимущество перед большинством мне известных людей — у меня с детства к себе не только мало претензий и много любви, но и заниженные стандарты. [...]
> 
>  я все время себя хвалю.
за все.
> 
> даже если я кричу, что «НИЧЕГО НЕ УСПЕЛА», то внутри лениво напоминаю себе, что раз успела хоть что-то, уже молодец.

Действительно, учусь себя хвалить.

## Технологии

### 015

Бобук [пишет](https://t.me/addmeto/2414):

> RescueTime, популярное приложение для мониторинга вашего времени, было удалено из appstore для iOS. Основная функция приложения — записывать сколько и каким приложением вы пользуетесь, чтобы вы потом могли в деталях узнать на что уходит ваше время. Однако совершенно аналогичную функцию встроили в iOS (она называется Screen Time), а значит приложение нарушает полиси о неповторении системных функций.
> 
> Вообще одна из главных проблем iOS для меня — невозможность установить приложение из интернета напрямую, минуя стор. Да, на андроидах это теперь тоже требует усилий, но хотябы можно, а на iOS по сути никак. Особенно я страдаю по удаленному из стора приложению ds_gеt для моей synology. https://blog.rescuetime.com/rescuetime-for-ios-removed/

У меня такая же моральная проблема с iOS. Да, большинство приложений в аппсторе доступны, все работает, потребитель счастлив. Но торренты скачать нельзя (они бывают вполне легальные, на web.archive.org, например), Телеграм могут убрать по требованию властей, настоящий браузер, отличный от Сафари, не поставить (Chrome и Firefox на iOS — это обертка одного движка), и так далее.

Подробнее я, Саша и Сергей обсуждали в [Иммигранткасте #32 — «Немецкая альтернатива Санта Клаусу»](https://www.spreaker.com/user/immigrantcast/german-santa-claus).

### 016

Вастрик [выложил крутые итоги года](https://vas3k.ru/blog/2018/), о переезде в Берлин и почему именно Берлин, сравнения Москвы с Европой (ну, с Вильнюсом), электросамокатах, причина продажи машины, про AirPods — там вообще все, короче:

> Когда английский становится твоим рабочим языком, он даже немного меняет отношение к ней. Например, вместо «почему твоё говно не собирается на моей машине» англоговорящие коллеги скажут «как у тебя будет время, не мог бы ты помочь мне со сборкой этого проекта». А книгу Ильяхова о деловой переписке можно без потери содержания уложить в один совет — пиши письма так, как их пишут англоговорящие люди. [...]

Или про Москву:

> Чемпионат мира сильно помог с обратной адаптацией. Количество весёлых иностранцев на улицах примерно приближалось к среднеевропейскому, что было привычно мне, но вызывало культурный шок почти у всех москвичей. Оказалось, по улицам можно не только грустно бежать на работу, но еще и весело гулять. Менты были особенно в шоке — столько весёлых людей, а дубинкой их отпиздить нельзя. У всех на лице был один вопрос: а чо так можно было?
> 
> Помимо ЧМ, интереснее всего было наблюдать последствия кризиса. Всё побито санкциями как метеоритным дождём, но никто не растерялся. Некоторые даже не заметили. Айфоны теперь не по карману, ну и хер с ними, ведь абсолютно случайно совпало, что на каждом углу теперь Xiaomi Store. Айтишники, выбиравшие вчера между Audi S5 и BMW M5, открыли для себя мир Kia и Ford — автопарковщик в машине за полтора ляма и другие небывалые корейские опции. Вино и сыр из Франции больше не возят и не надо, все полки теперь заставлены Кинзмараули и Сулугуни. Главные городские модники вместо Нью-Йорков и Сан-Франциско теперь летают в Сеул и Гонконг, ведь там «нормальные цены» и виз нет. [...]
> 
> Тот момент, когда у меня случился культурный обмен с моей же родной страной. Вернувшись в Европу поначалу даже попадал в неловкие моменты, когда забывал, что тут не знают ни о каких Xiaomi и электросамокатах.
> 
> А так всё было весело и круто. Москва всё так же шикарна, айтишники повысили себе зарплаты в три раза и всё так же зажрались, люди всё так же полны идей. В России Москве вообще классно, не знаю что за идиоты отсюда уезжают!

Топчик, только автор местами (как и многие в российском интернете, я заметил) стесняется показаться слишком искренним, поэтому вставляет шутки и подписи в стиле «Ути божечки. Ору.». Но это мелочи.

### 017

[Stumbling blocks on the way to web performance](https://macwright.org/2019/01/05/performance.html) — Том МакРайт  отмечает, что некоторые форматы и инструменты для ускорения загрузки страниц еще сырые, и, возможно, стоит подождать с их использованием. Чувствую то же самое, вроде хочется, но рано и сложно еще WebP, ленивая загрузка картинок во многих случаях больше бесит, чем помогает.

> I care about web performance — really I do. But we need to talk about something: a lot of the things we recommend for web performance are bad. The technology is unfinished, the implementations are rickety, and overall it’s just not easy to make something performant by default.

Но это не значит, что давайте теперь ничего не делать. Я за `srcset`, опциональные шрифты [`font-display`](https://developer.mozilla.org/en-US/docs/Web/CSS/@font-face/font-display), уменьшение размера скриптов, и все хорошее.

### 018

Алексей Черенкевич, как и многие другие, попробовал [перейти на с настоящей камеры и сменных объективов на айфон](https://www.facebook.com/cherenkevich/posts/10214812826246640) для фотографий в поездках:

> Короче, я эволюционировал (читай, постарел) и забил на фотоаппарат в отпуске. В пользу айфона XS. Один день ломки (ааа, мои фотографии больше не будут такими, как прежде), и наступила эйфория от полученных плюсов.

Все так. Но меня все равно немного парит, что я пожалею, что снимал только на смартфон столько лет. Но пока простота и удобства перевешивают.

### 019

Из заметки «[Пустое пространство в жизни](https://marinintim.com/2018/negative-space/)»:

> Одна из проблем в программировании, и, в частности, во фронтенде, — нельзя перестать учиться. Постоянно появляется что-то новое, инструменты, подходы, техники. Предполагается, что вы постоянно изучаете эти новые штуки, причём желательно не на рабочем проекте, а в своё свободное время. Сайд-проекты. Опенсорс, в него же тоже лучше контрибьютить. Дальше больше — стоит ходить на митапы, пообщаться с коллегами по отрасли и послушать доклады, слушать подкасты, смотреть доклады с конференций, на которые не получилось попасть вживую. [...]
> 
> Я не говорю, что всё это не нужно или не важно. Я говорю, что важно и другое.
> 
> Поездка в метро без подкаста в наушниках, просто разглядывая двери. Вечер без митапов, утро без статей, и дни без экспериментов с CSS. Обед без телефона в руке с открытым лонгридом или твиттером.
>
> Пространство, которое целенаправленно не занято чем-то.

## P. S.

### 020

Письма читателей:

<img class="small-image" src="/media/2019/01/SJge0VLzV.jpg">

Хороших выходных!

<figure>
  <img src="/media/2019/01/ryeiuUUf4.jpg">
  <figcaption>Это я в Бруклине одежду стирать пошел</figcaption>
</figure>
