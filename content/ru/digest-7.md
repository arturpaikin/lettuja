---
title: 'Дайджест #7'
datePublished: 2014-03-29 19:12
cover: http://arturpaikin.com/media/digest-7/syrom-apt-2.jpg
published: true
---

[![](/media/digest-7/syrom-apt-2.jpg)](/digest-7)

Содержание:

* Об аренде жилья в Москве и Нью-Йорке. Ремонт своими силами. Наша квартира в Сыромятниках
* Джейсон Джоунс в Daily Show о России
* VPS и облачный хостинг. Статичные сайты, Destroy Today и Oak
* Стайлгайды для веб-разработчиков. Поддержка ретины на сайтах
* Papers, Please
* О Popcorn Time и пиратских сериалах

<!--more-->

***

1. Cергей Король и Анна Шишлякова решили переехать в Москву из Череповца. Нарисовали картинку и разместили [объявление о поиске жилья](https://www.facebook.com/photo.php?fbid=10203254045451326&set=a.2751936156980.2156896.1214931795&type=1&stream_ref=10). Комментарии к нему можно уже печатать и издавать триллер:
    > ты романтику то выключи) какое кайфово? ты что думаешь чувак приедет такой и будет охуенно гулять и фоткать кирпичики и заборы на ломо и вся такая хуйня? Работа-дом, вот что ждет тут почти каждого, нужно жить рядом с работой, рядом с метро, выбираться куда то можно на выходные в лучшем случае

    Ещё:
    > есть охуенный способ гайз, апруф на личном опыте! ищите новостройку в районе где вам нравится, где живут ваши кенты, идете к консъержке (она как правило еще знает других консъержек и другие подъезды- бесплатный опцион), спрашиваете кто сдает однушку в доме, DONE! гуд квартира без посредников и риэлторов

    Не могу остановиться:
    > — ребят, все возможно. мы снимали на смоленской прекрасную однушку за 35) четыре почти года)

    > — Самое распространенное заблуждение провинциалов - в Москве надо жить в центре!))

    > — да в центре нехуй делать, кроме как сходить на кремль посмотреть разок )

    Найти не очень дорогую квартиру (25-40 тысяч) в Москве и Питере около центра можно. Но нужно хорошо поискать и сделать несложный ремонт. А это, как правило, многих и пугает:

    <figure><img src="/media/digest-7/apartment-pugaet.png" class="small-image"><figcaption>— Тут только за 60 в жопе. — Вот же, 30 у Парка Горького. — От работы далеко и еще что-то делать надо.</figcaption></figure>

    *В итоге Сергей и Аня квартиру нашли, переехали и [делятся впечатлениями](http://sergeykorol.ru/blog/to-moscow/).*

    Убежден, что наша миссия, как современных думающих людей, живущих на постсоветском пространстве, снимать унылые советские квартиры и делать в них ремонт. Не обязательно тратиться на дорогой: поклеить обои или покрасить стены, пол, снять ковер и победить финального босса — выкинуть стенку-горку. Это наша битва за будущие поколения. Если мы сделаем ремонт сегодня, им не придется снимать «чистую обычную квартиру».

    Анна Пономарева снимала сначала старый разваливающийся дом на Бали, а затем в Нью-Йорке. Оба сделала прекрасными. Посмотрите её посты: [про ванную](http://anna-ponomareva.livejournal.com/173771.html), «[Тихо, не бренди](http://anna-ponomareva.livejournal.com/176968.html)», [про Нью-Йорк](http://anna-ponomareva.livejournal.com/162551.html) (фотки пропали).

2. Отличная [колонка Алисы По](http://www.the-village.ru/village/home/home/130679-kvartira) о поисках квартиры в Москве:
    > Вы же знаете это неловкое чувство, когда тебя разглядывают оценивающе… Мещанская улица, агент — строгая женщина в возрасте: «Какую мебель хотите завезти? Кровати? Вы заметили тамбур на этаже — соседи будут видеть, кто к вам на эти кровати приходить будет!»

    Ну и еще раз напомню про материал Воса «[СУС в плитке до потолка](http://w-o-s.ru/article/4512)»:
    > 2-комнатная квартира, м. Новогиреево, 29 000 р. ЭТО трехкомнатная квартира, хозяин будет приходить на два часа и использовать как кабинет третью комнату. Хозяин профессор-энтилегентный.

3. Сегодня получается квартирный дайджест. Продолжаем. О нашей квартире в Сыромятниках [написал Кварблог](http://kvartblog.ru/blog/peremeny-k-luchshemu).

    <img src="/media/digest-7/syrom-apt-2.jpg" class="small-image">

    И Лена еще писала о том, [как мы делали ремонт](http://blog.elenazaharova.com/post/78984445528).

4. Дмитрий Мироненко [интересно пишет об аренде жилья в Нью-Йорке](http://www.the-village.ru/village/home/home/133057-amerikanskaya-istoriya-uzhasov-dmitriy-mironenko-o-tom-kak-snyat-kvartiru-v-nyu-yorke)»:
    > Первый и главный компромисс молодого ньюйоркца: или ты идёшь до своего дома по улице, на которой тебя рано или поздно сфотографирует «Сарториалист», но дом этот делишь с тремя-четырьмя художниками-дизайнерами-барменами-моделями, или селишься в скромной эмигрантской глуши и слушаешь по выходным буррито-музыку, играющую на неприличной громкости у соседей-испанцев. Я склонялся ко второму, потому что людей люблю меньше, чем дурацкую музыку.

    Отправил её почитать моему другу Андрею Чайковскому, который сейчас тоже живет в Нью-Йорке, вот что он ответил:
    > Чувак невезучий какой-то, у меня все агенты, с которыми я виделся, вились вокруг меня и не брали свой процент. То, что он предлагал за полгода вперед проплатить лиз, и думал что что-то изобрел, так на это почти сто процентов агентов пойдут, это последнее на что стоит соглашаться, можно выторговать 3 месяца. А вообще для всех нас идеал — сублиз, ничего не подписываешь, но живешь с кем-то. А с его бюджетом в 1300! можно охерительную студию в джерси снять хоть с семью умывальниками, 20 мин от манхеттена, а не в ссаном бушвике, где не хипстеры, а мексы на заброшенных заводах живут (многие восточный уильямсбург, котрый еще терпимый, за бушвик принимают). Но вот про виды квартир и унитаз посреди комнаты — это в точку. И на креглисте я тож жил одно время, и посморел порядочно. Кстати, я забил на все эти ганкса варианты и перебираюсь в самое сердце уильямсбурга к французам, потому что людей я люблю больше чем мексиканскую музыку))

5. Оля Малышева пишет [о детоксе дома](http://salatshop.ru/post/76307798085), и почему важно держать окно открытым.

6. Яркое [интервью с Антоном Шнайдером](http://mtrpl.ru/schnaider).

7. Джейсон Джоунс из американского «Шоу с Джоном Стюартом» побывал в России во время олимпиады. Три небольших, смешных и местами объективных видео: [Behind the Iron Closet](http://www.thedailyshow.com/watch/wed-february-12-2014/jason-jones-live-from-sochi-ish---behind-the-iron-closet), [Commie Dearest](http://www.thedailyshow.com/watch/mon-february-10-2014/jason-jones-live-from-sochi-ish---commie-dearest) и [No Safety Nyet](http://thedailyshow.cc.com/videos/30gza6/jason-jones-live-from-sochi-ish---no-safety-nyet). Про геев, Навального, шпионов, Шейк Шак и другое. Понравилось «maybe that’s just crazy old Russians with dead animals on their head».

8. Марко пишет о том, как включить в Гугл аналитике статистику по ретина-экранам, и [что все-таки делать с фотографиями для ретины](http://www.marco.org/2014/03/16/and). Краткий ответ: для блогов и небольших сайтов стоит просто вставлять фотографии в 1,5-2 раза больше обычного и не париться:
    > ...assuming everyone is high-DPI and designing accordingly is not only a safe bet — it’s a requirement. And serving 1X images isn’t something that small sites (for which bandwidth costs aren’t significant) need to worry much about.

    Я так и делаю. А для логотипов и графики по возможности SVG, иконочный шрифт, либо PNG в двух версиях (чтобы не мылилось на не-ретине).

    *Еще о поддержке ретины был хороший доклад Вадима Макеева «[Чётко и резко](http://pepelsbey.net/pres/clear-and-sharp/)».*

8. Брент Симмонс и Марко обсуждают в своих блогах облачные хостинги и VPS. Началось с поста Марко «[Web Hosting For App Developers](http://www.marco.org/2014/03/27/web-hosting-for-app-developers)». В нем он рассуждает о том, почему VPS лучше и дешевле облачных решений вроде AWS и Heroku.

    Ему ответил Брент: «[On Running Your Own Servers, and Why We’re Not](http://inessential.com/2014/03/27/on_running_your_own_servers_and_why_we)» о том, почему он предпочитает спать спокойно и пользоваться готовыми решениями и shared-хостингом.

    После этого, по иронии, его блог упал :-) Он написал «[What Did I Just Learn?](http://inessential.com/2014/03/27/what_did_i_just_learn_)», и задался вопросом, а не пора ли ему на VPS. И Марко снова ответил: «[What Did Brent Learn](http://www.marco.org/2014/03/27/what-did-brent-learn)».

    Если вы разработчик или вам просто интересна тема облачных хостингов, серверов, VPS и вот этого всего, обязательно прочитайте. Для себя я завел [VPS на Диджиталоушене](https://www.digitalocean.com/?refcode=054bb617bf5e) (реферальная ссылка) за 5$ в месяц, мне пока хватает, всем очень доволен. Научился немного администрировать линукс. Клиентам же советую [Докер](http://docker.ru) для shared-хостинга, либо [DreamHost](http://www.dreamhost.com/).

9. Играю в [Papers, Please](http://papersplea.se/). Эта игра — выполненный в ретро-стиле симулятор жизни таможенника на границе. Нужно проверять документы и решать, выдать человеку визу или нет. Есть заговоры, подозрительные личности, взятки. Увлекательно, советую.

    <img src="/media/digest-7/papers-please.png" class="small-image">

10. Несколько отличных гайдов по веб-разработке:
    * [Простой и понятный стайлгайд](http://mdo.github.io/code-guide/) по HTML, CSS и JS.
    *Полный [есть у Гугла](https://google-styleguide.googlecode.com/svn/trunk/htmlcssguide.xml), зато этот простой.*
    * Объяснения сложных моментов в верстке «[WTF, HTML and CSS?](http://wtfhtmlcss.com/)»: про флоаты, позиционирование и другое.
    * Удобная и понятная [адаптивная сетка](http://www.adamkaplan.me/grid/).
    * Еще раз упомяну [Motherfucking Website](http://motherfuckingwebsite.com/) — о том, что сайты нужно делать проще.
    * [Наглядные отличия](http://liquidapsive.com/) адаптивной, резиновой, статичной и респонсив верстки.
    * [Рекомендации по оформлению текстов](https://www.facebook.com/photo.php?fbid=10152746403130200&set=a.10152746361520200&type=3&theater) (на основе всех возможных справочников и устоявшихся традиций) от РИА Новостей. *[Прямая ссылка, PDF](https://www.dropbox.com/s/c73xdw85jg7dl63/ria-rules.pdf)*

11. Сергей Король пишет про [прекрасный фильм «Она»](http://sergeykorol.ru/blog/her/). Очень красивый и эмоциональный.

    > По сюжету фильма Теодор, сочиняющий на заказ красивые письма, тестирует операционную систему с искусственным интеллектом и влюбляется в неё, преображаясь через отношения с одухотворённой машиной.

    <img src="/media/digest-7/her.jpg" class="small-image">

    > Спайк Джонс рисует мир убер-хипстеров, которые живут в интерьерах из «Икеи», получившей пластическую подтяжку в стиле семидесятых, Дитера Рамса и вот этого всего. Фильм фонтанирует красотой минимализма и насыщенными цветами, визуальная эклектика сочетается в нём с тягой к натуральным материалам и вещам надолго. Всё это смотрится очень вкусно. Не зря к визуальной составляющей фильма приложили руку крутые ребята вроде Загмайстера.

13. Не теряющее актуальностью интервью [с мэром Боготы Энрике Пеньялоса](http://www.the-village.ru/village/city/infrastructure/112681-lektsiya-enrike-penyalosa).

    > Есть большая опасность для многих городов мира — торговые центры. Когда они замещают общественные пространства как места встречи людей — это симптом. Он говорит нам о том, что город болен и страдает.
    > ...
    > Хороший город — тот, в котором людям хочется бывать на улице.
    > ...
    > Для кого существует город: для машин или для людей? Чем более город дружелюбен к автомобилям, тем менее он дружелюбен к людям. Большие города — это заборы, ограничивающие вашу свободу. Чем больше в вашем городе автомобильных дорог, тем меньше места остается для вас.

14. Недавно запустился сервис [Popcorn Time](http://getpopcornti.me/) (проект [на гитхабе](https://github.com/popcorn-team/popcorn-app/releases/)). Задумка гениальная и простая: с помощью открытых библиотек формируется каталог, пользователь выбирает фильм, программа запускает загрузку его торрента и сразу стримит вам на экран. Таким образом получилось самое удобное приложение для просмотра фильмов (к сожалению, пиратских). Людям оно очень понравилось, не потому что жалко платить, а потому что альтернатив нормальных нет:

    > Piracy is not a people problem. It’s a service problem. A problem created by an industry that portrays innovation as a threat to their antique recipe to collect value. It seems to everyone that they just don’t care.

    Сегодня в России вообще нельзя легально посмотреть через интернет Теорию большого взрыва или Гравитацию в оригинале. Самый прогрессивный и легальный сервис в России — [Аййо](http://ayyo.ru), а в США — [Netflix](http://www.netflix.com/). Но им пока далеко по удобству и библиотеке до пиратских конкурентов вроде [Турбофильма](http://turbik.tv). Последний, кстати, доказывает своим примером, что люди готовы платить деньги даже за то, что можно скачать бесплатно, лишь бы это было удобно и доступно.

15. [Work From Home? Do it Better](https://medium.com/startup-lesson-learned/f33dd0e150d1) — правильные советы о том, как лучше организовать работу из дома.

16. Look at Me [опубликовал презентацию](http://dpl.lookatmedia.ru/) своего редактора материалов, а на List Apart пишут об [использовании плэйсхолдеров](http://alistapart.com/article/battle-for-the-body-field) для виджетов в тексте:
    > Instead of relying on rule-based templates to position them, however, use placeholders like ```<gallery id="1" />``` and ```<teaser article="82" rel="rebuttal" />``` right inside the narrative fields.

    Еще обратите внимание на тему для Вордпресса [Zero](http://cherenkevich.com/zero/) от [Алексея Черенкевича](cherenkevich.com). Она приятно выглядит, поддерживает врезки, обложки, лидовые абзацы и другое.

17. Как вовремя [понять, что ты счастлив](http://theoryandpractice.ru/posts/8673-happy1):
    > Что нужно делать для повышения уровня счастья? Один из советов лектора очень прост: попробуйте перед сном записывать на бумажку три события, за которые вы благодарны ушедшему дню, или имена трех людей. Таким образом вы настроите внутренний фокус на восприятие хорошего, а не плохого. Исследования подтверждают, что уже через пару недель люди, подводящие таким образом итоги дня, чувствуют себя немного счастливее.

18. Приятный блог [Destroy Today](http://destroytoday.com/blog/) Джонни Холлмэна. Он фриланс-дизайнер и разработчик из Нью-Йорка, раньше работал в компании [Oak](http://oak.is/) (в коворкинге Тины Айсенберг «Studio Mates» в Dumbo).

    На Creative Mornings (которые сейчас [проходят и в Москве](http://creativemornings.com/cities/mow), рекомендую сходить) Джонни [рассказал](http://www.youtube.com/watch?v=yV__6IFutwU) о детстве и хобби своего папы — моделях самолетов.

    <figure><img src="/media/digest-7/oak.jpg" class="small-image"><figcaption>Студия Oak. Фото Julia Robbs.</figcaption></figure>

    Oak сделали [Siteleaf](http://www.siteleaf.com/) — красивую и удобную CMS, генерирующую статичные сайты (посмотрите ролик на сайте). Статичные сайты — это как органическая еда, печенье без глютена и слоу-кофе. Мой сайт сейчас генерируется подобным образом, только движок написал я сам на Node.js.
