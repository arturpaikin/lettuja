---
title: "Евротрип фильм: Часть I"
description: "Мой фильм про два месяца путешествий по Европе автостопом, на поездах и машинах. Часть I: Париж, каучсерфинг, сырный французский разговор, автостопом до Люксембурга, еще каучсерфинг, и автостоп до Берлина."
cover: "http://arturpaikin.com/media/2017/10/BJCeyrg6b.jpg"
datePublished: 2017-10-14 9:53
published: true
---

Смонтировал фильм про два месяца путешествий по Европе. 

Часть I: Париж, каучсерфинг, сырный французский разговор, автостопом до Люксембурга, еще каучсерфинг и автостоп до Берлина.

<figure>
<a href="https://www.youtube.com/watch?v=7CfdUFgLQog">
  <img src="/media/2017/10/BJCeyrg6b.jpg">
</a>
<figcaption><a href="https://youtu.be/7CfdUFgLQog">https://youtu.be/7CfdUFgLQog</a></figcaption>
</figure>

Субтитры доступны на английском. Вторая часть в процессе монтажа.

**Часть 1**
[Часть 2](/ru/eurotrip-movie-part-two/)