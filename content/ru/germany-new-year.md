---
title: "Новый год в Германии"
description: "Этот Новый год мы встретили в Германии, по традиции в гостях у местного жителя (в прошлом году мы тоже встречали Новый год не дома, а в Нидерландах, в Гааге, у хорошего парня Мауритса, про это тоже бы надо отдельно написать). В этот раз местным жителем оказался классный немец Клаус, приятный во всех отношениях."
cover: "http://arturpaikin.com/media/germany-new-year/0_6e7cd_f00b1a8e_XXXL.jpg"
datePublished: 2011-07-07 17:29
tags: ["travel"]
published: true
---

На двери квартиры Клауса во Франкфурте нас ждала табличка «Уважаемые Елена и Артур из Москвы, добро пожаловать во Франкфурт» :-)

<img src="/media/germany-new-year/0_6e7c6_312b82ad_XXXL.jpg">

<!--more-->

Клаус водил нас завтракать в свои любимые кафешки, где очень мило подают традиционный завтрак: яйца, масло, хлеб, колбасу и бекон, тосты, фрукты.

<img src="/media/germany-new-year/0_6e7c8_2a01f6ac_XXXL.jpg">

За завтраком Клаус рассказывал о своей жизни. Он очень любит путешествовать на велосипеде и проехал на нем почти весь мир вокруг. В прошлом году он проехал за три месяца Россию — от Смоленска до Иркутска. Потом поехал в Монголию и Китай, на велосипеде. Принципы Клауса таковы, что в Китае он доехал до самого моря, а потом взял самолет до США. Проехав по США он взял самолет в Париж, но специально на поезде вернулся к океану, чтобы проехать всю Францию на велосипеде. У него есть смонтированная фотка, где он выходит из дома направо летом, а возвращается слева зимой.

Еще Клаус большой фанат Apple. Короче, я влюбился :-)

Вход в метро.

<img src="/media/germany-new-year/0_6e7c9_a7fdea63_XXXL.jpg">

Большой театр напоминает.

<img src="/media/germany-new-year/0_6e7cb_ef3e8d36_XXXL.jpg">

<img src="/media/germany-new-year/0_6e7cd_f00b1a8e_XXXL.jpg">

Трехэтажный завтрак.

<img src="/media/germany-new-year/0_6e7d2_2c9fe459_XXXL.jpg">

<img src="/media/germany-new-year/0_6e7d6_5416396a_XXXL.jpg">

<img src="/media/germany-new-year/0_6e7d5_1ff7c246_XXXL.jpg">

На Новый год немцы устроили традиционную для Европы войну феерверками, а под утро все выглядело так:

<img src="/media/germany-new-year/0_6e7d8_2358f81_XXXL.jpg">

<img src="/media/germany-new-year/0_6e7d7_5ec3bef4_XXXL.jpg">

Официант в традиционном старом немецком ресторане по традиции старается быть немного грубым.

<img src="/media/germany-new-year/0_6e7db_f1072a4a_XXXL.jpg">

<img src="/media/germany-new-year/0_6e7d9_495dbc2e_XXXL.jpg">

Пьем какой-то забавный сидр-пиво.

<img src="/media/germany-new-year/0_6e7dc_db1f7ea7_XXXL.jpg">

Из Франкфурта мы поехали на поезде в Мюнхен, где нас ждала Лиза (на фото справа), которая когда-то останавливалась у нас в Москве.

<img src="/media/germany-new-year/0_6e7c2_bd536_XXXL.jpg">

Первым делом в Мюнхене надо что? Выпить пива и закусить колбасками.

<img src="/media/germany-new-year/0_6e7dd_8bba2704_XXXL.jpg">

Потом сходить в Эппл стор.

<img src="/media/germany-new-year/0_6e7e1_c34a8b5b_XXXL.jpg">

Посмотреть на афиши в театрах.

<img src="/media/germany-new-year/0_6e7de_6f1135cc_XXXL.jpg">

Узнать, что тут думаю про русских.

<img src="/media/germany-new-year/0_6e7e3_df5bf6b0_XXXL.jpg">

<img src="/media/germany-new-year/0_6e7e6_c31a92f1_XXXL.jpg">

Забавный у девочки символ в волосах ;-)

<img src="/media/germany-new-year/0_6e7e7_afcb93d2_XXXL.jpg">

Из Германии в Ригу.

<img src="/media/germany-new-year/0_6e7e9_5c21a2c8_XXXL.jpg">

А из Риги домой.
