---
title: "Автостопом по Исландии: шторм вулканического яйца"
description: "Смонтировал фильм про неделю путешествий автостопом по Исландии с палаткой. Рейкьявик, кемпинг, водопады, тайный бассейн. Знакомлюсь с паффинами и варю яйцо в вулкане. В конце мы попадаем в шторм и спасаемся бегством."
cover: "http://arturpaikin.com/media/2019/12/Sk94rowar.jpg"
datePublished: 2019-12-06 13:25
published: true
---

Смонтировал фильм про неделю путешествий автостопом по Исландии с палаткой. Рейкьявик, кемпинг, водопады, тайный бассейн. Знакомлюсь с паффинами и варю яйцо в вулкане. В конце мы попадаем в шторм и спасаемся бегством.

<figure>
<a href="https://youtu.be/zbXk5NHe1HM">
  <div class="video-overlay">
    <img src="/media/2019/12/Sk94rowar.jpg">
  </div>
</a>
<figcaption><a href="https://youtu.be/zbXk5NHe1HM">https://youtu.be/zbXk5NHe1HM</a></figcaption>
</figure>

Приятного просмотра!

★ Дополнение: [выпуск Иммигранткаста](https://www.spreaker.com/user/immigrantcast/vulcanic-egg-iceland-ep-025), где я подробно рассказываю о путешествии.