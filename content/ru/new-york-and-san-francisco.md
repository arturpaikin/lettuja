---
title: Нью-Йорк и Сан-Франциско
cover: http://arturpaikin.com/media/new-york-and-san-francisco/IMG_6119.jpg
datePublished: 2013-12-09 10:12
published: true
---

![](/media/new-york-and-san-francisco/IMG_6119.jpg)

В прошлую поездку мне очень понравился Нью-Йорк.

Сан-Франциско — город мечты, раз туда уже переехали почти все, кого я фоловлю в интернете, а городской туман [зовут Карлом](https://www.facebook.com/pages/Karl-the-Fog/145697385477802).

Американская виза открыта на три года.

Три слагаемых в сумме дают билеты Москва—Нью-Йорк—Сан-Франциско—Нью-Йорк—Москва.

<!--more-->

## Нью-Йорк

Летели Аэрофлотом, мне давно хотелось посмотреть, за что их хвалят. Все неплохо, но Трансаэро не хуже. А лучше всех пока Этихад.

Нью-Йорк встретил танцевальным шоу в метро.

![](/media/new-york-and-san-francisco/IMG_3291.jpg)

С каучсерфингом в этот раз не везело — на сорок запросов в оба города пришлось четыре отказа, остальные письма вообще остались без ответа. Зато друзья (привет Наташа и Брэд) как раз сняли квартиру в Вильямсбурге — самом хипстерском районе — и позвали в гости.

В Вильямсбурге отлично. Вот знаменитая кофейня [Blue Bottle](http://www.bluebottlecoffee.com/).

![](/media/new-york-and-san-francisco/IMG_3295.jpg)

[ROA](http://www.unurth.com/filter/ROA) нарисовал белку.

![](/media/new-york-and-san-francisco/IMG_6066.jpg)

Вывески на магазинах принято делать минималистичными. Нет хитрого смысла или желания что-то выдумывать — просто название белыми буквами на стекле. Толстый желто-зеленый баннер не требуется, чудеса.

![](/media/new-york-and-san-francisco/IMG_3299.jpg)

Прогулялись пешком по Вильямсбургскому мосту. Настоятельно советую пройтись хотя бы по одному из мостов, соединяющих Бруклин и Манхэттен (Williamsburg Bridge, Brooklyn Bridge, Manhattan Bridge), со всех по-своему приятный вид.

![](/media/new-york-and-san-francisco/IMG_3301.jpg)

Есть отдельная сторона для велосипедов. В Нью-Йорке уже работает автоматический велопрокат, правда для туристов дороговат — 10$ в день или около 100$ в год.

![](/media/new-york-and-san-francisco/IMG_6119.jpg)

На фермерском рынке продают чудесные тыквы к Хэллоуину.

![](/media/new-york-and-san-francisco/pumpkins.jpg)

![](/media/new-york-and-san-francisco/IMG_6108.jpg)

Пополнил свой стратегический запас кленового сиропа.

![](/media/new-york-and-san-francisco/farmer-market.jpg)

Собак принято выгуливать оптом.

![](/media/new-york-and-san-francisco/IMG_3302.jpg)

В Юнион сквере турнир по шахматам.

![](/media/new-york-and-san-francisco/IMG_6099.jpg)

Город полон небольшими парками и скверами, в которых можно просто посидеть — взять зеленый стул и стол, пообедать тэйкаут-едой из ближайшего магазина, поймать вай-фай, подзарядить телефон.

![](/media/new-york-and-san-francisco/square-park.jpg)

![](/media/new-york-and-san-francisco/IMG_6136.jpg)

В Централ парке бросают фрисби.

![](/media/new-york-and-san-francisco/IMG_3317.jpg)

Sheep Meadow.

![](/media/new-york-and-san-francisco/IMG_3365.jpg)

![](/media/new-york-and-san-francisco/IMG_3330.jpg)

![](/media/new-york-and-san-francisco/IMG_6227.jpg)

Пожарная машина.

![](/media/new-york-and-san-francisco/IMG_3360.jpg)

Книжный магазин (внимание на вывеску).

![](/media/new-york-and-san-francisco/IMG_6208.jpg)

Чайный магазин.

![](/media/new-york-and-san-francisco/IMG_6206.jpg)

Дом Кэрри Брэдшоу из Секса в большом городе.

![](/media/new-york-and-san-francisco/IMG_3361.jpg)

<aside class="side-note">Если вам нравится Нью-Йорк, очень советую сам сериал и фильмы <a href="http://en.wikipedia.org/wiki/Sex_and_the_City">Sex and the City</a>. Они не только для девочек, как многие ошибочно думают. Кроме секса в сериале рассказывается про жизнь в Америке, аренду жилья, работу колумнистом в газете, <a href="http://en.wikipedia.org/wiki/Jury_duty">Jury duty</a> и отношения между людьми.</aside>

Попробовал знаменитый фрозен йогурт. Вкусно.

![](/media/new-york-and-san-francisco/IMG_3332.jpg)

Одно из самых интересных мест Нью-Йорка — [Highline Park](http://en.wikipedia.org/wiki/High_Line_(New_York_City)). На месте заброшенной железной дороги построили полуторакилометровый парк, с растениями, водой, тележками с едой и живописным видом на город.

![](/media/new-york-and-san-francisco/IMG_3333.jpg)

![](/media/new-york-and-san-francisco/IMG_3342.jpg)

![](/media/new-york-and-san-francisco/IMG_3338.jpg)

![](/media/new-york-and-san-francisco/IMG_3340.jpg)

![](/media/new-york-and-san-francisco/IMG_3364.jpg)

Голубая бутылка угощает кофе — это промо-акция Амазона. По очереди ходит мужчина с новым киндлом и дает всем его пощупать.

![](/media/new-york-and-san-francisco/IMG_3334.jpg)

![](/media/new-york-and-san-francisco/IMG_6213.jpg)

Почта.

![](/media/new-york-and-san-francisco/IMG_3353.jpg)

![](/media/new-york-and-san-francisco/IMG_3362.jpg)

И посылка с Амазона.

![](/media/new-york-and-san-francisco/IMG_3366.jpg)

Прямо моя мечта: все свое всегда с собой. Правда у меня вещей поменьше.

![](/media/new-york-and-san-francisco/IMG_6226.jpg)

Завтрак в Вильямсбурге: гранола (мюсли) с фруктами. В Штатах мюсли очень популярны, можно найти много органических, изготовленных на ферме в Портлэнде. Прямо как в сериале Портландия, [про «местного цыплёнка»](http://www.youtube.com/watch?v=ErRHJlE4PGI).

![](/media/new-york-and-san-francisco/IMG_3367.jpg)

<aside class="side-note">Портлэнд — город-рай для хипстеров и людей, живущих по принципу «Independently make a living doing what you love». Номер один в моем плане на следующую поездку в США. Недавно там прошел <a href="http://xoxofest.com">фестиваль XOXO</a>, с которого уже выложили очень интересные <a href="http://www.youtube.com/channel/UCqMG_BBwxrhLG80Y3yuEu-Q">видео выступлений</a>. Посмотрите еще первый сезон сериала <a href="http://en.wikipedia.org/wiki/Portlandia_(TV_series&#41;">Портландия</a>, начать можно с <a href="http://www.youtube.com/watch?v=FE_9CzLCbkY">этого короткого ролика</a>.</aside>

[Машина](http://en.wikipedia.org/wiki/Ice_cream_van) с мороженым.

![](/media/new-york-and-san-francisco/IMG_3371.jpg)

Погулял по району Дамбо — это такой Красный октябрь в Нью-Йорке, с отличным видом на Манхэттен, только без пафоса. Здесь находятся офисы интересных компаний вроде [Studio Mates](http://studiomates.com/) (коворкинга [Тины Айсенберг](http://www.swiss-miss.com/)) и [FiftyThree](http://fiftythree.com/).

![](/media/new-york-and-san-francisco/IMG_3385.jpg)

И мой любимый West Elm.

![](/media/new-york-and-san-francisco/IMG_6302.jpg)

![](/media/new-york-and-san-francisco/IMG_6303.jpg)

Суровый вид на Манхэттен и Бруклин бридж.

![](/media/new-york-and-san-francisco/IMG_3387.jpg)

И менее суровый.

![](/media/new-york-and-san-francisco/IMG_6346.jpg)

Чайна-таун.

![](/media/new-york-and-san-francisco/IMG_6370.jpg)

Большой магазин комиксов [на 42 улице](http://www.midtowncomics.com/info.asp?tour=times-square).

![](/media/new-york-and-san-francisco/IMG_3391.jpg)

Previously — on AMS's — The Walking Dead. Auaghhh.

![](/media/new-york-and-san-francisco/IMG_3554.jpg)

Прокатились на [канатном трамвае](http://en.wikipedia.org/wiki/Roosevelt_Island_Tramway) на Рузвельт айленд. Классный вид, немного страшновато, но стоит того.

![](/media/new-york-and-san-francisco/IMG_3395.jpg)

В середине недели ответила пара с каучсерфинга, позвали на два дня к себе в Парк Слоуп. Парня зовут Дуглас, он американец, а девушку Махо, она японка. Познакомились в Японии, когда Дуглас путешествовал по миру, потом переехали в Нью-Йорк учиться.

Парк Слоуп к Хэллоуину готов.

![](/media/new-york-and-san-francisco/IMG_6299.jpg)

Утро. Пора лететь в Сан-Франциско.

![](/media/new-york-and-san-francisco/IMG_3404.jpg)


## Калифорния

В аэропорту неожиданно встретился <a href="http://en.wikipedia.org/wiki/Walter_White_(Breaking_Bad)">Уолтер Уайт</a>, улетающий в Калифорнию с чемоданом денег.

![](/media/new-york-and-san-francisco/walter.jpg)

Рейс «Американ эирлайнс», которым мы летели, оказался ужасным: стоит 300$, кресла очень неудобные, стюардессы без формы, за 6 часов не кормят.

![](/media/new-york-and-san-francisco/IMG_3414.jpg)

В Сан-Франциско с каучсерфингом еще хуже, чем в Нью-Йорке. Сняли [пол дома на Эирбнб](https://www.airbnb.com/rooms/1351783). Получилось примерно 2100 рублей в сутки.

![](/media/new-york-and-san-francisco/IMG_6416.jpg)

Домики по соседству. Стоит примерно 1500$ в месяц за этаж: пара комнат, ванная, кухня.

![](/media/new-york-and-san-francisco/sunset-houses.jpg)

Рядом прекрасный Golden Gate Park, где [желающие найдут](http://instagram.com/p/fQfW-aqIKW/) множество живописных деревьев, растений и шишек.

![](/media/new-york-and-san-francisco/IMG_6423.jpg)

Я устроил себе рабочее место у окна и сразу основал стартап.

![](/media/new-york-and-san-francisco/IMG_3429.jpg)

Вид с холма.

![](/media/new-york-and-san-francisco/IMG_3639.jpg)

Велопарковка внутри станции, на пересадочном узле «метро-электрички». Очень разумно.

![](/media/new-york-and-san-francisco/IMG_3430.jpg)

Знаменитые трамваи ([cabel car](http://en.wikipedia.org/wiki/San_Francisco_cable_car_system)), по большей части туристические.

![](/media/new-york-and-san-francisco/IMG_6461.jpg)

В Голубой бутылке двери и окна открыты настолько, что кажется, что их нет. Помещения распространяются на улицу, барьеры стираются — красота.

![](/media/new-york-and-san-francisco/IMG_3434.jpg)

В Сан-Франциско полно бомжей. С собаками, большими тележками, на инвалидных колясках, у которых сломалась спинка, поэтому для опоры к ней привязана скотчем труба от унитаза. Они заполоняют собой город, особенно внизу, потому что, как говорят местные, на  холм им сложно и лениво забраться.

![](/media/new-york-and-san-francisco/homeless.jpg)

Парк рядом с Yerba Buena Center, где проходят конференции Эппл WWDC.

![](/media/new-york-and-san-francisco/IMG_3433.jpg)

Район [Хэйc Вэлли](https://www.airbnb.com/locations/san-francisco/hayes-valley).

![](/media/new-york-and-san-francisco/IMG_6473.jpg)

С необычным мороженным,

![](/media/new-york-and-san-francisco/IMG_3442.jpg)

кофе в гараже,

![](/media/new-york-and-san-francisco/IMG_3445.jpg)

кафе в контейнерах,

![](/media/new-york-and-san-francisco/IMG_3443.jpg)

и местным жителем, который был очень горд своим районом. Сказал, что живет в нем всю жизнь, и его окружает прекрасная еда и отличные люди.

![](/media/new-york-and-san-francisco/IMG_3440.jpg)

Тем временем две собаки перетягивали красную резинку.

![](/media/new-york-and-san-francisco/IMG_3693.jpg)

Дома в Сан-Франциско, которые все фотографируют.

![](/media/new-york-and-san-francisco/IMG_6506.jpg)

На холме.

![](/media/new-york-and-san-francisco/IMG_6510.jpg)

![](/media/new-york-and-san-francisco/IMG_6523.jpg)

Питьевой фонтан для собак.

![](/media/new-york-and-san-francisco/IMG_3600.jpg)

![](/media/new-york-and-san-francisco/IMG_3450.jpg)

Распределение велосипедов по районам.

![](/media/new-york-and-san-francisco/IMG_3452.jpg)

Трамвай идет по Маркет стрит до Fisherman's Wharf.

![](/media/new-york-and-san-francisco/IMG_6586.jpg)

Читаем комикс по дороге.

![](/media/new-york-and-san-francisco/IMG_3421.jpg)

Парк Долорес, в молодежном хипстерском районе Мишн.

![](/media/new-york-and-san-francisco/IMG_3584.jpg)

Все отдыхают и устраивают пикники с барбекю и пивом. Иногда подходят сомнительно вида парни и ненавязчиво предлагают дунуть.

![](/media/new-york-and-san-francisco/IMG_3574.jpg)

![](/media/new-york-and-san-francisco/IMG_3575.jpg)

![](/media/new-york-and-san-francisco/IMG_3580.jpg)

Реклама в небе.

![](/media/new-york-and-san-francisco/IMG_3577.jpg)

Очередь в «местный Чоп-Чоп» на Валенсии.

![](/media/new-york-and-san-francisco/IMG_3463.jpg)

![](/media/new-york-and-san-francisco/IMG_6591.jpg)

Заходили на шоколадную фабрику [Dandelion Chocolate](https://medium.com/beautiful-stories/3eddd92ca6d7) (по ссылке красивый пост о фабрике и о том, как делается шоколад).

В офисе Airbnb мастеркласс про meteor.js. Кто-то привел своего пса.

![](/media/new-york-and-san-francisco/airbnb-meetup.jpg)

Сам офис забавно устроен: каждая комната [оформлена в интерьере отдельной квартиры](http://www.businessinsider.com/airbnb-office-tour).

Кофе-ростер в одной из лучших кофеен Сан-Франциско — [Sightglass](https://sightglasscoffee.com). В Штатах принято совмещать производство с магазинами, всегда интересно понаблюдать, как делают то, что ты потом покупаешь.

![](/media/new-york-and-san-francisco/IMG_3601.jpg)

![](/media/new-york-and-san-francisco/IMG_3608.jpg)

Хипстеры-программисты делают стартап (не такое часто явление в Сан-Франциско, как я ожидал).

![](/media/new-york-and-san-francisco/IMG_3603.jpg)

Набор кофемана: чайники, кофе всех сортов, фильтры, дрипперы, весы с секундомером.

![](/media/new-york-and-san-francisco/IMG_3605.jpg)

Подруга Катя позвала нас на Burning Man Decompression — лайт-версию фестиваля [Burning Man](http://en.wikipedia.org/wiki/Burning_Man). Там я встретил человека, которому Стриды показалось мало (правда вторая фотография раскрывает истинное предназначение его велосипеда).

![](/media/new-york-and-san-francisco/bike-woman.jpg)

Бернинг мэн, судя только по той малой его части, что я увидел, — это свобода и яркие костюмы.

![](/media/new-york-and-san-francisco/bm-decompression.jpg)

![](/media/new-york-and-san-francisco/IMG_3624.jpg)

Кафе-коворкинг.

![](/media/new-york-and-san-francisco/IMG_3632.jpg)

Неплохой бар, который все советовали на Yelp.

![](/media/new-york-and-san-francisco/IMG_3634.jpg)

<aside class="side-note">В поездках и дома я всегда ищу интересные места в <a href="https://foursquare.com/">Foursquare</a> — это отличный справочник и гид по городам. В Штатах все пользуются <a href="http://www.yelp.com/">Yelp</a> и в нем действительно можно найти много полезных рекомендаций, особенно на тему еды и кофе.</aside>

А здесь отличные сэндвичи и салаты.

![](/media/new-york-and-san-francisco/IMG_6645.jpg)

Этот мост вы знаете.

![](/media/new-york-and-san-francisco/IMG_6617.jpg)

Пирс с морскими котиками. Они очень милые, зайдите [в гости](http://en.wikipedia.org/wiki/Pier_39) при случае.

![](/media/new-york-and-san-francisco/IMG_3475.jpg)

В Сан-Франциско активно продают VHS-кассеты и считают, что это нормально. А, ну и проезд в трамвае по городу стоит два доллара купюрами без сдачи, картой оплатить нельзя (то есть можно где-то достать проездной, но никто не знает, где и зачем). Ходишь каждый раз побираешься по трамваю: разменяйте пожалуйста кто-нибудь 5$, а то я за проезд заплатить не могу.

![](/media/new-york-and-san-francisco/IMG_3482.jpg)

Еще в Сан-Франциско большой и интересный Чайна-таун.

![](/media/new-york-and-san-francisco/IMG_6732.jpg)

C мини-фабрикой по приготовлению fortune cookies, куда [желающие могут заглянуть](http://www.yelp.com/biz/golden-gate-fortune-cookies-san-francisco).

![](/media/new-york-and-san-francisco/IMG_6721.jpg)

И да, ростеры на каждом шагу.

![](/media/new-york-and-san-francisco/IMG_3483.jpg)

## Visiting the Mothership: Купертино, кампус Apple, Пало-Альто и Стэнфорд

Разумеется, находясь в паре десятков километров от самой интересной айти-компании в мире, я не мог туда не съездить. Поезд Caltrain довезет до Sunnyvale за час, а оттуда, а оттуда все. Автобус ходит так редко, что его никто не видел, поэтому шесть километров пешком.

По пути встретилась гаражная распродажа.

![](/media/new-york-and-san-francisco/IMG_3486.jpg)

Под мост, через мост. Еврейский вопрос мужчине на машине «извините, а Купертино — это туда?» не прокатил. И вот я на месте.

![](/media/new-york-and-san-francisco/IMG_3492.jpg)

1, Infinite Loop. Apple Campus.

![](/media/new-york-and-san-francisco/IMG_3490.jpg)

Вход.

![](/media/new-york-and-san-francisco/IMG_3493.jpg)

Велопарковка.

![](/media/new-york-and-san-francisco/IMG_3501.jpg)

![](/media/new-york-and-san-francisco/IMG_3512.jpg)

На территории кампуса есть уникальный Эппл стор: в нем продаются футболки, кружки и прочая сувенирная продукция с логотипом компании. Больше нигде официально футболку «Cupertino, home of the Mothership» купить нельзя.

![](/media/new-york-and-san-francisco/IMG_3500.jpg)

![](/media/new-york-and-san-francisco/IMG_3498.jpg)

![](/media/new-york-and-san-francisco/IMG_3495.jpg)

![](/media/new-york-and-san-francisco/IMG_3496.jpg)

Территория кампуса.

![](/media/new-york-and-san-francisco/IMG_3509.jpg)

Сотрудники играют в Воллейбол.

![](/media/new-york-and-san-francisco/IMG_3507.jpg)

Перед каждым корпусом его пиксельный номер.

![](/media/new-york-and-san-francisco/apple-campus-numbers.jpg)

Туалет для посетителей.

![](/media/new-york-and-san-francisco/IMG_3514.jpg)

Сотрудники ждут шаттл-автобуса домой.

![](/media/new-york-and-san-francisco/IMG_3510.jpg)

А за нами автобус не приехал. На вопрос «а как добраться вот до этой улицы» сотрудник Эппл-стора ответил: «ну как — [садишься за руль и едешь](http://www.youtube.com/watch?v=72068l8usCI)». Пошли пешком еще три километра.

![](/media/new-york-and-san-francisco/IMG_3523.jpg)

![](/media/new-york-and-san-francisco/IMG_3519.jpg)

Следующая остановка — дом с гаражом, где Стив Джобс и Стив Возняк основали Apple.

На некоторых домах наблюдаются солнечные панели.

![](/media/new-york-and-san-francisco/IMG_3526.jpg)

Калифорнийские ретро-пейзажи.

![](/media/new-york-and-san-francisco/IMG_3527.jpg)

![](/media/new-york-and-san-francisco/IMG_3550.jpg)

![](/media/new-york-and-san-francisco/IMG_6776.jpg)

Типичный дом в Купертино.

![](/media/new-york-and-san-francisco/IMG_3531.jpg)

А вот и место назначения — тот самый гараж. Перед ним табличка: «Private property, please do not disturb the tenant».

![](/media/new-york-and-san-francisco/IMG_3551.jpg)

Дальше должен был быть автобус. Но время было — о ужас — 6 часов, а ведь всем известно, что в 6 вечера все порядочные калифорнийцы прячутся по машинам, барам и домам, а на улицы выходят бомжи-зомби.

<img src="/media/new-york-and-san-francisco/IMG_3520.jpg" class="small-image">

Подошли к автобусной остановке, откуда, судя по картам Гугл, мы могли бы уехать до ближайшей станции Caltrain. Женщина, которая почему-то сидела на этой остановке, на мексиканском английском заявила нам, что «bus no mo» и изобразила руками крест.

Пошли на заправку. Подхожу к американцу, говорю, вот такое дело, как нам на станцию бы попасть и до Сан-Франциско. Хм, задумался он. Даже и не знаю. А такси дорого стоит, спрашиваю я. А я не знаю, дорого, но могу выяснить.

Достает айфон, говорит: «Siri, call me a cab». Да, я вот тут на заправке, сколько будет стоить до станции. 20 долларов, ага. Я киваю, да, вызывайте. Он вызывает, говорит им, парень будет вас ждать здесь на заправке. Будут через 20 минут.

Проходит 20 минут, потом 30 — никого. Я подхожу к следующему заправляющемуся, объясняю все снова, такси не приехало. Он смеется. Такси, говорит. Конечно, не приехало. Потом к нему приезжает друг: у них дочки дружат и ездят к друг другу в гости на выходные иногда.

«Guess what!» — говорит один другому. — «They called a cab and it never showed up! Surprise-surprise!» Оба смеются и объясняют, что в Калифорнии таксист не считает своим долгом приехать, если его вызвали, а ему не очень нравится маршрут. Второй мужчина согласился подвезти нас до Сан-Франциско.

![](/media/new-york-and-san-francisco/IMG_3536.jpg)

Потом они вспомнили, что вообще-то сейчас пятница вечер и в Стэнфорде, куда мы все равно собирались, явно будут вечеринки. Решено — едем в Стэнфорд.

На самом деле вечеринки оказались в закрытых для нас общежитиях, а кампус было не очень интересно смотреть в темноте. Так что мы вернулись через день со знакомым на машине (Арам, спасибо!).

![](/media/new-york-and-san-francisco/IMG_3641.jpg)

По кампусу все перемещаются на велосипедах.

![](/media/new-york-and-san-francisco/stanford-bikes.jpg)

А вокруг все похоже на Хогвартс, который занесло в Марокко.

![](/media/new-york-and-san-francisco/IMG_3640.jpg)

![](/media/new-york-and-san-francisco/IMG_3646.jpg)

Еще немного и из-за угла выйдет [Минерва МакГонаггал](http://ru.harrypotter.wikia.com/wiki/%D0%9C%D0%B8%D0%BD%D0%B5%D1%80%D0%B2%D0%B0_%D0%9C%D0%B0%D0%BA%D0%B3%D0%BE%D0%BD%D0%B0%D0%B3%D0%B0%D0%BB%D0%BB). Ну или Абдул.

![](/media/new-york-and-san-francisco/IMG_3645.jpg)

![](/media/new-york-and-san-francisco/IMG_3642.jpg)

Дропбокс завлекает студентов футболками и бесплатной едой.

![](/media/new-york-and-san-francisco/IMG_3540.jpg)

По кампусу, кстати, можно свободно ходить и заходить в большинство зданий.

Дальше пошли в Пало-Альто. Там обнаружился классный Эппл стор (ну а что вы от меня ожидали).

![](/media/new-york-and-san-francisco/apple-store-palo-alto.jpg)

И Cheesecake Factory, известная всем [по одному сериалу](http://en.wikipedia.org/wiki/The_Big_Bang_Theory).

![](/media/new-york-and-san-francisco/IMG_3652.jpg)

![](/media/new-york-and-san-francisco/IMG_3653.jpg)

Уберите ваш брошенный велосипед а то мы его ата-та.

![](/media/new-york-and-san-francisco/IMG_3654.jpg)

Вообще в Пало-Альто спокойно, хорошо, красиво и тихо. ~~Как в Финляндии на пенсии.~~

![](/media/new-york-and-san-francisco/IMG_3659.jpg)

Красивые дома.

![](/media/new-york-and-san-francisco/IMG_7021.jpg)

![](/media/new-york-and-san-francisco/IMG_3666.jpg)

А это — последний дом Стива Джобса.

![](/media/new-york-and-san-francisco/IMG_3662.jpg)

![](/media/new-york-and-san-francisco/IMG_3671.jpg)

![](/media/new-york-and-san-francisco/IMG_3670.jpg)

The Creamery — приятное кафе в Сан-Франциско возле станции с электричками в Долину. Говорят, здесь часто заключают большие сделки и продают-покупают компании.

![](/media/new-york-and-san-francisco/IMG_3667.jpg)

Кофейня [DRIP'D Coffee](http://www.dripdcoffee.com/) снимает помещение напополам с магазином косметики. Парень очень обрадовался, когда мы рассказали ему про [Типи кофе](https://www.facebook.com/thetipicoffee), и подарил упаковку кофе из Sightglass :-) Передаем привет из Москвы.

![](/media/new-york-and-san-francisco/IMG_3568.jpg)

Посмотрел в живую на Теслу.

![](/media/new-york-and-san-francisco/IMG_3674.jpg)

Старая вывеска Пепси.

![](/media/new-york-and-san-francisco/IMG_3675.jpg)

![](/media/new-york-and-san-francisco/IMG_7082.jpg)

Серфер спешит на волну.

![](/media/new-york-and-san-francisco/IMG_3682.jpg)

Купил [рюкзак Everlane](https://www.everlane.com/collections/canvas-leather-packs/products/snap-pack-grey), очень нравится, рекомендую.

![](/media/new-york-and-san-francisco/IMG_3692.jpg)

Национальный парк «[Заброшенный бассейн](http://en.wikipedia.org/wiki/Sutro_Baths)».

![](/media/new-york-and-san-francisco/IMG_3685.jpg)

Это первый раз за поездку, когда я заметил, что в США вообще-то [government shutdown](http://en.wikipedia.org/wiki/United_States_federal_government_shutdown_of_2013): туалет в заброшенном бассейне оказался закрыт.

![](/media/new-york-and-san-francisco/IMG_3686.jpg)

Водолаз чистит аквариум в [California Academy of Sciences](http://www.calacademy.org/).

![](/media/new-york-and-san-francisco/IMG_3687.jpg)

Там же рассказывается, что пить воду из-под крана полезнее, чем бутилированную. Думаю, они про какую-то определенную страну говорят.

![](/media/new-york-and-san-francisco/IMG_3691.jpg)

Живая крыша.

![](/media/new-york-and-san-francisco/IMG_3690.jpg)

Пора обратно в Нью-Йорк.

Пока мы были в Сан-Франциско, он мне не нравился из-за завышенных ожиданий: я представлял себе какие-то толпы стартаперов на летающих самокатах и велосипедах, технологии и жизнь ключем. А нашел тихий спокойный рай, как в Таиланде, только дорогой и не с таким уж большим количеством хипстеров.

Сан-Франциско как Москва — в нем нужно знать, что ты от города хочешь получить, и составлять план, чтобы тебя случайно не съели (шутка).

Недавно наткнулся на [пост](http://destroytoday.com/blog/2012-a-year-in-review) в блоге одного дизайнера и веб-разработчика, и следующая цитата отлично передает мои ощущения от города:

>After a year and a half in San Francisco, both Jen and I heard the calling of the east coast loud and clear. Even with the beautiful weather, close proximity to the mountains and beaches, and unbelievably accessible fresh food, overall, it wasn’t for us. In February, we moved to New York where we feel at home more than ever. This is where I’m meant to be.

В Нью-Йорке осталось два дня. Завтрак в лучшем кафе Вильямсбурга — [Five Leaves](http://fiveleavesny.com/).

![](/media/new-york-and-san-francisco/IMG_3700.jpg)

<aside class="side-note">Рекомендую «Big Brekkie» с грибами и картошкой или блины с кленовым сиропом и ягодами (одно из двух, все сразу не съесть — порции огромные). На завтрак бывает очередь.</aside>

Потом хипстерский бородатый кофе в [Grumpy](http://cafegrumpy.com/).

![](/media/new-york-and-san-francisco/IMG_3698.jpg)

Погулял по [району Tribeca](https://www.airbnb.com/locations/new-york/tribeca), в котором можно увидеть настоящий Нью-Йорк, без толп туристов.

![](/media/new-york-and-san-francisco/IMG_3701.jpg)

![](/media/new-york-and-san-francisco/IMG_7101.jpg)

Последние покупки перед возвращением в голодный СССР (чай, бэйглы, мюсли, кленовый сироп, [почтовый ящик](http://instagram.com/p/gArk47SSaO/)). Упаковка покупок в челночные сумки.

![](/media/new-york-and-san-francisco/chelnok.jpg)

И на метро в аэропорт JFK в компании зеленого человека.

![](/media/new-york-and-san-francisco/IMG_7146.jpg)

---

### Про деньги

Билеты стоили больше, чем в прошлый раз — 22 тысячи первые и 11 вторые. Средняя цена, если без распродаж. В середине сентября началась распродажа до Нью-Йорка за 12 и даже до Лос Анжелеса, почти в два раза больше по расстоянию, тоже за 12. Но нужные даты и маршрут бы не получились, так что сдавать не стал.

##### Полезные ссылки

* Отличный [фотогид по Нью-Йорку](https://readymag.com/u11904265/NewYork-PhotoGuide/) от Антона Реппонена
* Колонка «[Нерезиновая столица Калифорнии](http://mtrpl.ru/san-francisco/)», после которой захотелось сразу переехать в Сан-Франциско
* [10 Things I Love About You](https://medium.com/@monteiro/10-things-i-love-about-you-2b57e97e2941) (San Francisco edition)
* [Твит](https://twitter.com/pepelsbey/status/403014750931386368) Вадима Макеева про СФ
* [Гид по Сан-Франциско](https://www.airbnb.com/locations/san-francisco) от Airbnb
* Пост Елены Захаровой [про поездку в Нью-Йорк и Сан-Франциско](http://blog.elenazaharova.com/post/65807736691/new-york-and-san-francisco)
* [San Francisco On The Cheap: Do This, Skip That In Hipsterland](http://www.huffingtonpost.com/2013/08/24/san-francisco-cheap_n_3780993.html)
