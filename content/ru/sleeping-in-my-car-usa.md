---
title: "Можно ли ночевать в машине в Штатах?"
description: "Мне всегда интересно эксперементировать с бюджетными способами жить и путешествовать. Про Америку принято думать, что там все очень дорого, а спать в машине запрещено. Я решил проверить, можно ли в штате Нью-Йорк спокойно пожить в обычной машине как нормальный человек? Чтобы ответить на этот вопрос, я по очереди ночую в лодочном порту, на стоянке грузовиков и на парковке знаменитого гипермаркета Волмарт."
cover: "http://arturpaikin.com/media/2019/02/S1lJB87VN.jpg"
datePublished: 2019-02-02 20:35
published: true
---

Мне всегда интересно эксперементировать с бюджетными способами жить и путешествовать. Про Америку принято думать, что там все очень дорого, а спать в машине запрещено. Я решил проверить, можно ли в штате Нью-Йорк спокойно пожить в обычной машине как нормальный человек?

Чтобы ответить на этот вопрос, я по очереди ночую в лодочном порту, на стоянке грузовиков и на парковке знаменитого гипермаркета Волмарт:

<figure>
<a href="https://youtu.be/MaGasAUm3Ms">
  <div class="video-overlay">
    <img src="/media/2019/02/S1lJB87VN.jpg">
  </div>
</a>
<figcaption><a href="https://youtu.be/MaGasAUm3Ms">https://youtu.be/MaGasAUm3Ms</a></figcaption>
</figure>

🎙 Подробнее тему раскрыли мы с Сашей в [Иммигранткасте #27: Картонные медведи штата Нью-Йорк](https://www.spreaker.com/user/immigrantcast/cardboard-bear-immigrantcast-ep027).

Смотрите также: [#vanlife](https://www.instagram.com/explore/tags/vanlife/), [@vanlife.magazine](https://www.instagram.com/vanlife.magazine/) и канал [Exploring Alternatives](https://www.youtube.com/user/explorealternatives).

А вообще, профессионалы [ночуют в машине так](https://www.youtube.com/watch?v=UyXzn9-Rcg0).
