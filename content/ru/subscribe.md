---
title: Подписаться
datePublished: 2019-02-24 13:57
published: true
type: page
---

Подписаться на мои посты и заметки:

* [RSS](/ru/blog-feed.xml) — лучший способ, он придуман специально для этого. Выберите любой удобный сервис, например, [Feedly](https://feedly.com) — в нем можно читать через веб или поставить приложения на компьютер и смартфон (например, [Reeder](http://reederapp.com/)). [Добавьте мой блог](http://feedly.com/i/subscription/feed/http://arturpaikin.com/ru/blog-feed.xml) — и все, ваша личная подборка интересных постов и статей, без рекламы, без алгоритмов фэйсбука и прочего мусора. Подробнее [про РСС и почему он лучше соцсетей](https://ilyabirman.ru/meanwhile/all/rss-subscription/).
* [Канал в Телеграме](https://t.me/arturpaikincom)
* [Твиттер](https://twitter.com/arturi_ru)
* [Инстаграм](https://instagram.com/arturi)
* [Фэйсбук](https://fb.me/arturpaikin)
