---
title: Спасибо
datePublished: 2015-03-06 13:57
type: page
---

Если вам понравились мои посты, фотографии, видео или что-то еще, вы можете поблагодарить меня, переведя небольшую сумму денег на кофе:

* [PayPal](https://paypal.me/iamarturpaykin);
* Яндекс.Деньги: 41001898195842. [Перевод с банковской карты или Яндекс.Денег](https://money.yandex.ru/embed/shop.xml?account=41001898195842&quickpay=shop&payment-type-choice=on&writer=seller&targets=%D0%91%D0%BB%D0%B0%D0%B3%D0%BE%D0%B4%D0%B0%D1%80%D0%BD%D0%BE%D1%81%D1%82%D1%8C&targets-hint=&default-sum=100&button-text=01&successURL=).

![image](/media/2015/05/img_66851432231289707.jpg)
