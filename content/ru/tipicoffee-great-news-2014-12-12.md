---
datePublished: 2014-12-12 09:12
published: true
excludeFromFeed: true
tags: ["note"]
---

![](/media/2018/01/rkt4S0UBM.jpg)

У нас отличные новости. Кофейня Типи кофе арендовала домик в самом центре Москвы, на площади у Большого театра (Театральная площадь, 1). Сегодня и каждый день, до самого Нового года, с 11 утра до 9 вечера, мы будем угощать вас ароматным кофе из Кении и Эфиопии, обжаренного нашими друзьями, Кооперативом Чёрный. А наши соседи — проект Bakersville — пекут вкусные бельгийские вафли.

![](/media/2018/01/ry5kIAUHf.jpg)

![](/media/2018/01/HkgtEBR8Hf.jpg)

Мы очень рады и ждем в гости всех друзей. Приходите!

А вот короткое [видео о том, как мы делаем кофе](https://www.youtube.com/watch?v=nfuX1rypN4Y).

На самом деле [настоящее видео здесь](https://vimeo.com/118402160), снял Кирилл Кулаков:

<iframe src="https://player.vimeo.com/video/118402160" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
