---
title: "Год в Нью-Йорке: лето"
datePublished: 2017-08-21 18:37
description: "Продолжаю рассказывать про Нью-Йорк, когда месяц превратился в год. Во второй части: Ист Вилладж, ферма на крыше, Брайтон, люди в метро, охрана стирки, Риджвуд, виза и покемоны."
cover: http://arturpaikin.com/media/2017/08/H1xxG4tHu-.jpg
published: true
---

<img src="/media/2017/08/H1xxG4tHu-.jpg">

Продолжаю рассказывать про Нью-Йорк, когда месяц превратился в год.

*Во второй части: Ист Вилладж, ферма на крыше, Брайтон, люди в метро, охрана стирки, Риджвуд, виза и покемоны.*

<!--more-->

Полюбил гулять по Ист Вилладжу. Самый уютный район на Манхэттене, я считаю.

![](/media/2017/06/r1eXZeUfZ.jpg)

![](/media/2017/06/SkQ2ZgIzb.jpg)

![](/media/2017/06/BkdxHgIGZ.jpg)

Зайдите в [Ray’s Candy Store](https://www.yelp.com/biz/rays-candy-store-new-york-2), где забавный мужчина сделает вам pistachio cone!

![](/media/2017/06/SJY3-eIMW.jpg)

Возле Tompkins Square парка раздают еду.

![](/media/2017/06/BkLbzxUzZ.jpg)

И продают тоже.

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2017/06/SJEYBeIf-.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2017/06/r15FHgUMW.jpg">
  </div>
</div>
</figure>

Рисует Манхэттен.

![](/media/2017/08/Hk4A5OOu-.jpg)

Кирилл приехал в гости. Я не общался открыто (по-русски, когда друг другу на проблемы жалуются, из серии «рыдать — только с русскими» *— Елена Захарова*) несколько месяцев, а проблемы накопились. Поэтому я говорил и говорил, даже пока Кирилл мылся в душе.

![](/media/2017/06/SySHzl8fb.jpg)

Водил его в Cobble Hill, Gowanus, Park Slope — по всем своим любимым местам. Как раз все чудесно цвело в начале мая. На следующий день Кирилл сбежал на Манхэттен.

![](/media/2017/06/HkiPMlIzZ.jpg)

![](/media/2017/06/SkGKflUzW.jpg)

![](/media/2017/06/ryOFMxIfW.jpg)

![](/media/2017/06/SJMcfeLMW.jpg)

Ездили на Брайтон и Кони Айленд, там атмосферно, как всегда.

![](/media/2017/06/Sku2GlLG-.jpg)

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2017/06/rJoTMlUzZ.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2017/06/rJeipzg8zZ.jpg">
  </div>
</div>
</figure>

![](/media/2017/08/S1eGsGzRvW.jpg)

![](/media/2017/08/BkGiMGCP-.jpg)

![](/media/2017/08/Syde2duuW.jpg)

В метро бывает хорошо.

![](/media/2017/06/BylLXeIMW.jpg)

Кирилл сводил на экскурсию в Гугл.

<figure>
<div class="row wide collage">
  <img src="/media/2017/06/B11dQgUGb.jpg">
</div>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2017/06/Hk-FmxIGZ.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2017/06/rJl-KQlLGW.jpg">
  </div>
</div>
</figure>

Станция «Сенека Авеню». Риджвуд (новый Бушвик) — дом родной.

![](/media/2017/06/HJQ0mgIM-.jpg)

![](/media/2017/06/rJn4VxUfb.jpg)

Собака ждет хозяина с бейглами у Norma’s.

![](/media/2017/06/Hk2zSlIfW.jpg)

Так в Америке приходят по почте важные документы.

![](/media/2017/06/B1ryEg8M-.jpg)

А вот так распределяются деньги, когда ты снимаешь их с карты Альфы, а банкомат выдает тысячу купюрами по двадцать.

![](/media/2017/06/HyDZVeLzb.jpg)

---

Jackson Heights — район в Квинсе, где говорят на 167 языках.

<figure>
<div class="row wide collage">
  <div class="column-full">
    <img src="/media/2017/06/S1HjEgUGW.jpg">
  </div>
</div>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2017/06/rkz3VgUzb.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2017/06/ryDhVx8GZ.jpg">
  </div>
</div>
</figure>

Тем временем, Егор улетел в Москву.

<figure class="small-image">
  <img src="/media/2017/06/HyOWSe8fW.jpg">
</figure>

Наша крыша в Риджвуде.

![](/media/2017/06/B1MESlUzW.jpg)

Экскурсия на крышу Brooklyn Grange фермы. Да, ферма на крыше.

<figure>
<div class="row wide collage">
  <div class="column-full">
    <img src="/media/2017/06/ByHwSe8f-.jpg">
  </div>
</div>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2017/06/B1hvSlIf-.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2017/08/S1xjBV74ub.jpg">
  </div>
</div>
</figure>

![](/media/2017/08/ByiSNm4_-.jpg)

У Verizon забастовка сотрудников.

![](/media/2017/06/ry1THl8zb.jpg)

В метро для рекламы доступны разные форматы, в том числе столбы.

![](/media/2017/06/rkc6SlUzZ.jpg)

Закаты в Вильямсбурге.

![](/media/2017/06/BJxYlIxIG-.jpg)

![](/media/2017/06/r1IWLl8MZ.jpg)

Пока я мылся в душе, за мной подглядывали.

![](/media/2017/06/ByrMIe8GW.jpg)

![](/media/2017/06/ryVEIx8MZ.jpg)

В Вашингтон сквере.

![](/media/2017/06/HJRI7lIzW.jpg)

![](/media/2017/06/HyFNLgIM-.jpg)

## Люди в метро

Девочка плакала, а незнакомый парень решил помочь родителям и подсел ее развлекать.

![](/media/2017/06/BklV58QwM-.jpg)

«Сфоткай меня, чтоб я как мачо».

![](/media/2017/06/HJZN987wMW.jpg)

Self explanatory.

![](/media/2017/06/S1EqU7PMb.jpg)

![](/media/2017/08/rkvDGfCvb.jpg)

![](/media/2017/08/B1Na3O_d-.jpg)

<figure>
<div class="row wide collage">
  <div class="column-1-2 height-xl">
    <img src="/media/2017/08/rJIQCg0Db.jpg">
  </div>

  <div class="column-1-2 height-xl">
    <img src="/media/2017/08/Hyr7Ag0v-.jpg">
  </div>
</div>

<figcaption>Компания Baguette LLC и Артур Пайкин умещаются в два рюкзака и папку. Компания Figura.co занимает немного больше.</figcaption>
</figure>

Indeed.

![](/media/2017/08/B1sb1bCPZ.jpg)

XOXO митап c [Andy McMillan](http://goodonpaper.com/) и [Lucy Bellwood](https://lucybellwood.com/).

![](/media/2017/08/HybQybAPW.jpg)

SoHo.

![](/media/2017/08/rk301ZAwW.jpg)

![](/media/2017/08/rJz92dO_Z.jpg)

Феерверки на 4 июля, День независимости.

![](/media/2017/08/SkJ_ebRwb.jpg)

О Нью-Йорке больше всего узнаю из рекламы Street Easy.

![](/media/2017/08/HJb5x-Cv-.jpg)

Мое рабочее место (по настроению) у Flatiron.

![](/media/2017/08/BJo2lZ0vZ.jpg)

![](/media/2017/08/BJKxZZRDb.jpg)

Мороженое для собак.

![](/media/2017/08/BkkP--0DW.jpg)

Баржа, вид с Brooklyn Heights.

![](/media/2017/08/By1VbZRPW.jpg)

![](/media/2017/08/Byyj4-RwW.jpg)

Жизнь в Нью-Йорке не сахар. Чтобы не украли твои штаны, которые стираются в двух кварталах от дома, нужно заводить собаку.

![](/media/2017/08/Syk_WZRPW.jpg)

Ответ Лены на вопрос «кем вы видите себя через пять лет».

![](/media/2017/08/S1jtW-CDb.jpg)

Велосипеды компактнее Стриды существуют (это A-bike).

![](/media/2017/08/SJGoZbAwZ.jpg)

Малоизвестный чайнатаун-2 в Бруклине, Sunset Park.

![](/media/2017/08/rJ8nrWAwb.jpg)

![](/media/2017/08/S1wGffCPb.jpg)

В нем парк с видом и танцами.

![](/media/2017/08/HybU2SbCDW.jpg)

![](/media/2017/08/S1xI3SW0Db.jpg)

Это лето Покемонов.

![](/media/2017/08/SkMOXKH_-.jpg)

<img class="small-image" src="/media/2017/08/B1TI4_S_-.jpg">

Мой любимый мост — Вильямсбургский. Обязательно по нему прогуляйтесь.

![](/media/2017/08/r1T_GKHdZ.jpg)

![](/media/2017/08/BkxzVYS_-.jpg)

![](/media/2017/08/r1o0fYudZ.jpg)

![](/media/2017/08/ByrRzYdO-.jpg)

Пляж Rockaway. Альтернатива Брайтону.

![](/media/2017/08/r1W0ftrd-.jpg)

![](/media/2017/08/Sy4QmYSuZ.jpg)

![](/media/2017/08/Hk7xQFruW.jpg)

![](/media/2017/08/HJdk9dOub.jpg)

Документы на визу выглядят так. Сбор и подготовка заняли полгода.

![](/media/2017/08/H1v8t__O-.jpg)

А сама виза выглядит так.

![](/media/2017/08/BkbFYOddZ.jpg)

Бушвик.

![](/media/2017/08/Sk4Bc__uZ.jpg)

Попугай из Бушвика едет домой.

![](/media/2017/08/HJD55O_OW.jpg)

![](/media/2017/08/HJWIqOd_b.jpg)

Вечеринка по поводу выхода Harry Potter and the Cursed Child.

![](/media/2017/08/S1SLh_OOb.jpg)

Знакомая поставила крутой спектакль о цикличности жизни.

![](/media/2017/08/ry_csOudb.jpg)

Продолжение следует.

---

[Год в Нью-Йорке: зима и весна](http://arturpaikin.com/ru/year-in-new-york-part-1/)
**Год в Нью-Йорке: лето**

---

##### В предыдущих сериях

* [Нью-Йорк, первая поездка](http://arturpaikin.com/ru/new-york/)
* [Нью-Йорк и Сан Франциско](http://arturpaikin.com/ru/new-york-and-san-francisco/)
* [Brooklyn: Chapter One](http://arturpaikin.com/en/brooklyn-chapter-one/)
* [Brooklyn: Chapter Two](http://arturpaikin.com/en/brooklyn-chapter-two/)
* [Путешествие по США: фильм](http://arturpaikin.com/ru/usa-movie/)